


-- ----------------------------
-- Table structure for absensi
-- ----------------------------
DROP TABLE IF EXISTS "public"."absensi";
CREATE TABLE "public"."absensi" (
"id" int2 DEFAULT nextval('absensi_id_seq'::regclass) NOT NULL,
"id_karyawan" int2,
"tanggal" date,
"periode" "public"."periode",
"jumlah_hadir" numeric(15),
"jumlah_harus_hadir" numeric(15),
"potongan" numeric(30)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of absensi
-- ----------------------------
INSERT INTO "public"."absensi" VALUES ('23', '29', '2015-08-30', 'Agustus', '20', '30', '1066000');
INSERT INTO "public"."absensi" VALUES ('25', '35', '2015-08-30', 'Agustus', '15', '20', '1000000');
INSERT INTO "public"."absensi" VALUES ('26', '27', '2015-08-30', 'Agustus', '25', '30', '583000');
INSERT INTO "public"."absensi" VALUES ('27', '34', '2015-08-30', 'Agustus', '19', '20', '160000');

-- ----------------------------
-- Table structure for app_user_role
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_user_role";
CREATE TABLE "public"."app_user_role" (
"uniq_kode" int4 DEFAULT nextval('app_user_role_aur_id_seq'::regclass) NOT NULL,
"user_name" varchar(255) COLLATE "default" NOT NULL,
"password" text COLLATE "default",
"role" "public"."role"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of app_user_role
-- ----------------------------
INSERT INTO "public"."app_user_role" VALUES ('2', 'karyawan', '$2a$08$/PMBB5iwXsN5MhFayYpHHeJSJBNZ5k3EuslBZKV7M8JJDmwBGQhZ6', 'karyawan');
INSERT INTO "public"."app_user_role" VALUES ('6', 'superadmin', '$2a$08$/PMBB5iwXsN5MhFayYpHHeJSJBNZ5k3EuslBZKV7M8JJDmwBGQhZ6', 'superadmin');
INSERT INTO "public"."app_user_role" VALUES ('145', '03.03.145', '$2a$08$/PMBB5iwXsN5MhFayYpHHeJSJBNZ5k3EuslBZKV7M8JJDmwBGQhZ6', 'karyawan');
INSERT INTO "public"."app_user_role" VALUES ('147', '08.10.147', '$2a$08$/PMBB5iwXsN5MhFayYpHHeJSJBNZ5k3EuslBZKV7M8JJDmwBGQhZ6', 'karyawan');
INSERT INTO "public"."app_user_role" VALUES ('156', 'admin', '$2a$08$/PMBB5iwXsN5MhFayYpHHeJSJBNZ5k3EuslBZKV7M8JJDmwBGQhZ6', 'admin');
INSERT INTO "public"."app_user_role" VALUES ('157', 'afita', '$2a$08$/PMBB5iwXsN5MhFayYpHHeJSJBNZ5k3EuslBZKV7M8JJDmwBGQhZ6', 'admin');
INSERT INTO "public"."app_user_role" VALUES ('158', 'admin2', '$2a$08$/PMBB5iwXsN5MhFayYpHHeJSJBNZ5k3EuslBZKV7M8JJDmwBGQhZ6', 'admin');
INSERT INTO "public"."app_user_role" VALUES ('159', '08.10.160', '$2a$08$/PMBB5iwXsN5MhFayYpHHeJSJBNZ5k3EuslBZKV7M8JJDmwBGQhZ6', 'karyawan');
INSERT INTO "public"."app_user_role" VALUES ('159', 'superadmin2', '$2a$08$/PMBB5iwXsN5MhFayYpHHeJSJBNZ5k3EuslBZKV7M8JJDmwBGQhZ6', 'superadmin');
INSERT INTO "public"."app_user_role" VALUES ('160', '01.03.160', '$2a$08$/PMBB5iwXsN5MhFayYpHHeJSJBNZ5k3EuslBZKV7M8JJDmwBGQhZ6', 'karyawan');
INSERT INTO "public"."app_user_role" VALUES ('161', '08.03.161', '$2a$08$/PMBB5iwXsN5MhFayYpHHeJSJBNZ5k3EuslBZKV7M8JJDmwBGQhZ6', 'karyawan');

-- ----------------------------
-- Table structure for bonus
-- ----------------------------
DROP TABLE IF EXISTS "public"."bonus";
CREATE TABLE "public"."bonus" (
"id" int4 DEFAULT nextval('bonus1_id_seq'::regclass) NOT NULL,
"id_karyawan" int4,
"nama_bonus" varchar(50) COLLATE "default",
"jumlah_bonus" numeric(30),
"tanggal" date,
"periode" "public"."periode"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of bonus
-- ----------------------------
INSERT INTO "public"."bonus" VALUES ('1', '29', 'thr', '2000000', '2015-08-30', 'Agustus');
INSERT INTO "public"."bonus" VALUES ('2', '27', 'THR', '2000000', '2015-08-30', 'Agustus');
INSERT INTO "public"."bonus" VALUES ('3', '34', 'THR', '2000000', '2015-08-30', 'Agustus');
INSERT INTO "public"."bonus" VALUES ('4', '35', 'THR', '2000000', '2015-08-30', 'Agustus');

-- ----------------------------
-- Table structure for ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS "public"."ci_sessions";
CREATE TABLE "public"."ci_sessions" (
"session_id" varchar(40) COLLATE "default" DEFAULT '0'::character varying NOT NULL,
"ip_address" varchar(16) COLLATE "default" DEFAULT '0'::character varying NOT NULL,
"user_agent" varchar(150) COLLATE "default" NOT NULL,
"last_activity" int4 DEFAULT 0 NOT NULL,
"user_data" text COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO "public"."ci_sessions" VALUES ('3a65b172389b4c0b25a0e2a076729de1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36', '1677641123', 'a:5:{s:9:"user_data";s:0:"";s:20:"app_config_uniq_kode";s:1:"3";s:9:"logged_in";b:1;s:8:"username";s:5:"admin";s:4:"role";s:5:"admin";}');
INSERT INTO "public"."ci_sessions" VALUES ('9ef87c6b5f396034cd03e0114eced329', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36', '1677640151', 'a:5:{s:9:"user_data";s:0:"";s:20:"app_config_uniq_kode";s:1:"3";s:9:"logged_in";b:1;s:8:"username";s:5:"admin";s:4:"role";s:5:"admin";}');
INSERT INTO "public"."ci_sessions" VALUES ('a6ac33420363bd1d302b04bb546fa591', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36', '1677637807', 'a:3:{s:9:"user_data";s:0:"";s:20:"app_config_uniq_kode";s:1:"3";s:13:"security_code";s:5:"6549c";}');
INSERT INTO "public"."ci_sessions" VALUES ('db2e0afb693b57446059010521527f10', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36', '1677641008', 'a:3:{s:9:"user_data";s:0:"";s:20:"app_config_uniq_kode";s:1:"3";s:13:"security_code";s:5:"zy3kk";}');

-- ----------------------------
-- Table structure for cobatambah
-- ----------------------------
DROP TABLE IF EXISTS "public"."cobatambah";
CREATE TABLE "public"."cobatambah" (
"id" int2 DEFAULT nextval('golongan_gaji_id_seq'::regclass) NOT NULL,
"id_jabatan" int2,
"gaji_pokok" numeric(30),
"id_divisi" int2,
"lembur_perjam" numeric(30),
"status_karyawan" "public"."status_karyawan"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of cobatambah
-- ----------------------------
INSERT INTO "public"."cobatambah" VALUES ('24', '3', '2000000', '1', '20000', 'Kontrak');
INSERT INTO "public"."cobatambah" VALUES ('25', '3', '2500000', '7', '25000', 'Kontrak');
INSERT INTO "public"."cobatambah" VALUES ('26', '3', '3000000', '1', '30000', 'Tetap');
INSERT INTO "public"."cobatambah" VALUES ('28', '2', '12345', '1', '67890', 'Kontrak');

-- ----------------------------
-- Table structure for divisi
-- ----------------------------
DROP TABLE IF EXISTS "public"."divisi";
CREATE TABLE "public"."divisi" (
"id" int2 DEFAULT nextval('divisi_id_seq'::regclass) NOT NULL,
"nama_divisi" varchar(50) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of divisi
-- ----------------------------
INSERT INTO "public"."divisi" VALUES ('1', 'Bio Engineering');
INSERT INTO "public"."divisi" VALUES ('3', 'Social Engineering');
INSERT INTO "public"."divisi" VALUES ('7', 'Energi');
INSERT INTO "public"."divisi" VALUES ('8', 'Teknologi Informasi');
INSERT INTO "public"."divisi" VALUES ('10', 'cek');

-- ----------------------------
-- Table structure for gaji
-- ----------------------------
DROP TABLE IF EXISTS "public"."gaji";
CREATE TABLE "public"."gaji" (
"id" int8 DEFAULT nextval('gaji_id_seq'::regclass) NOT NULL,
"id_karyawan" int4,
"tanggal_gaji" date,
"gaji_bersih" numeric(30),
"periode" "public"."periode"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of gaji
-- ----------------------------
INSERT INTO "public"."gaji" VALUES ('50', '35', '2015-08-30', '5500000', 'Agustus');

-- ----------------------------
-- Table structure for golongan_gaji
-- ----------------------------
DROP TABLE IF EXISTS "public"."golongan_gaji";
CREATE TABLE "public"."golongan_gaji" (
"id" int2 DEFAULT nextval('golongan_gaji_id_seq'::regclass) NOT NULL,
"id_jabatan" int2,
"gaji_pokok" numeric(30),
"id_divisi" int2,
"lembur_perjam" numeric(30),
"status_karyawan" "public"."status_karyawan"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of golongan_gaji
-- ----------------------------
INSERT INTO "public"."golongan_gaji" VALUES ('13', '1', '1000000', '4', '10000', null);
INSERT INTO "public"."golongan_gaji" VALUES ('14', '2', '900000', '1', '9000', 'Kontrak');
INSERT INTO "public"."golongan_gaji" VALUES ('15', '2', '900000', '7', '9000', 'Kontrak');
INSERT INTO "public"."golongan_gaji" VALUES ('16', '10', '1200000', '8', '12000', 'Tetap');
INSERT INTO "public"."golongan_gaji" VALUES ('17', '10', '1500000', '8', '15000', 'Kontrak');
INSERT INTO "public"."golongan_gaji" VALUES ('18', '2', '1500000', '8', '15000', 'Tetap');
INSERT INTO "public"."golongan_gaji" VALUES ('19', '3', '2000000', '8', '20000', 'Kontrak');
INSERT INTO "public"."golongan_gaji" VALUES ('20', '3', '1500000', '3', '15000', 'Tetap');
INSERT INTO "public"."golongan_gaji" VALUES ('21', '3', '2500000', '8', '25000', 'Tetap');
INSERT INTO "public"."golongan_gaji" VALUES ('22', '2', '1500000', '8', '15000', 'Kontrak');
INSERT INTO "public"."golongan_gaji" VALUES ('23', '3', '2000000', '3', '20000', 'Kontrak');
INSERT INTO "public"."golongan_gaji" VALUES ('24', '3', '2000000', '1', '20000', 'Kontrak');
INSERT INTO "public"."golongan_gaji" VALUES ('25', '3', '2500000', '7', '25000', 'Kontrak');
INSERT INTO "public"."golongan_gaji" VALUES ('26', '3', '3000000', '1', '30000', 'Tetap');
INSERT INTO "public"."golongan_gaji" VALUES ('29', '2', '9000000', '1', '1', 'Kontrak');

-- ----------------------------
-- Table structure for jabatan
-- ----------------------------
DROP TABLE IF EXISTS "public"."jabatan";
CREATE TABLE "public"."jabatan" (
"id" int2 DEFAULT nextval('jabatan_id_seq'::regclass) NOT NULL,
"nama_jabatan" varchar(50) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of jabatan
-- ----------------------------
INSERT INTO "public"."jabatan" VALUES ('2', 'Analis');
INSERT INTO "public"."jabatan" VALUES ('3', 'Manajer');
INSERT INTO "public"."jabatan" VALUES ('10', 'Programmer');

-- ----------------------------
-- Table structure for karyawan
-- ----------------------------
DROP TABLE IF EXISTS "public"."karyawan";
CREATE TABLE "public"."karyawan" (
"id" int8 DEFAULT nextval('karyawan_id_seq'::regclass) NOT NULL,
"nama" varchar(50) COLLATE "default",
"username" varchar(50) COLLATE "default",
"no_ktp" varchar(50) COLLATE "default",
"alamat_asal" varchar(50) COLLATE "default",
"alamat_tinggal" varchar(50) COLLATE "default",
"no_hp" varchar(50) COLLATE "default",
"email" varchar(50) COLLATE "default",
"id_divisi" int2,
"id_jabatan" int2,
"tanggal_masuk" date,
"tanggal_akhir_kontrak" date,
"id_proyek" int2,
"tanggal_lahir" date,
"tanggal" date,
"status_karyawan" "public"."status_karyawan",
"agama" "public"."agama",
"golongan_darah" "public"."golongan_darah",
"jenis_kelamin" "public"."jenis_kelamin"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of karyawan
-- ----------------------------
INSERT INTO "public"."karyawan" VALUES ('27', 'Cahya Yuli', '03.03.145', '3276728328623', null, null, null, null, '3', '3', '2015-09-19', '2016-02-19', '5', null, '2015-08-19', 'Tetap', null, null, null);
INSERT INTO "public"."karyawan" VALUES ('29', 'Afita Sofiana', '08.10.147', '324986328469234', null, null, null, null, '8', '10', '2016-02-19', '2017-04-19', '5', null, '2015-08-19', 'Tetap', null, null, null);
INSERT INTO "public"."karyawan" VALUES ('34', 'Dewi Anisa', '08.10.160', '1235645648945', null, null, null, null, '8', '10', '2015-08-30', '2015-10-30', '5', null, '2015-08-30', 'Kontrak', null, null, null);
INSERT INTO "public"."karyawan" VALUES ('35', 'Kiki Palmin', '01.03.160', '1543543545345', 'Lampung', 'Jogja', '0982862376', 'kiki@gmail.com', '1', '3', '2015-08-30', '2015-10-30', '6', '2015-08-30', '2015-08-30', 'Kontrak', 'Islam', 'B', 'Perempuan');
INSERT INTO "public"."karyawan" VALUES ('36', 'Rizki', '08.03.161', '8764287347537', null, null, null, null, '8', '3', '2015-08-31', '2015-10-30', '5', null, '2015-08-31', 'Tetap', null, null, null);

-- ----------------------------
-- Table structure for lembur
-- ----------------------------
DROP TABLE IF EXISTS "public"."lembur";
CREATE TABLE "public"."lembur" (
"id" int2 DEFAULT nextval('lembur_id_seq'::regclass) NOT NULL,
"id_karyawan" int2,
"jumlah_bonus" numeric(30),
"tanggal_lembur" date,
"jumlah_jam" numeric(16),
"tanggal" varchar COLLATE "default",
"periode" "public"."periode"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of lembur
-- ----------------------------
INSERT INTO "public"."lembur" VALUES ('21', '27', '1200000', '2015-07-30', '80', '2015-08-19', 'Agustus');
INSERT INTO "public"."lembur" VALUES ('22', '29', '240000', '2015-08-19', '20', '2015-08-19', 'Agustus');
INSERT INTO "public"."lembur" VALUES ('28', '29', '360000', '2015-08-23', '30', '2015-08-23', 'Januari');
INSERT INTO "public"."lembur" VALUES ('29', '35', '500000', '2015-08-04', '25', '2015-08-30', 'Agustus');
INSERT INTO "public"."lembur" VALUES ('30', '34', '450000', '2015-08-04', '30', '2015-08-30', 'Agustus');
INSERT INTO "public"."lembur" VALUES ('31', '34', '150000', '2015-08-31', '10', '2015-08-31', 'Agustus');
INSERT INTO "public"."lembur" VALUES ('32', '27', '75000', '2015-08-31', '5', '2015-08-31', 'Agustus');

-- ----------------------------
-- Table structure for pinjaman
-- ----------------------------
DROP TABLE IF EXISTS "public"."pinjaman";
CREATE TABLE "public"."pinjaman" (
"id" int2 DEFAULT nextval('pinjaman_id_seq'::regclass) NOT NULL,
"id_karyawan" int2,
"jumlah_pinjam" numeric(30),
"alasan_pinjam" varchar(50) COLLATE "default",
"tanggal_pinjam" date,
"angsuran_pinjam" numeric(30),
"sisa_pinjam" numeric(30),
"tanggal" varchar COLLATE "default",
"status_pinjam" "public"."status_pinjam"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of pinjaman
-- ----------------------------
INSERT INTO "public"."pinjaman" VALUES ('34', '29', '2000000', 'bayar spp', '2015-08-10', null, null, '2015-08-30', 'belum_lunas');
INSERT INTO "public"."pinjaman" VALUES ('35', '27', '2000000', 'bayar kos', '2015-07-27', null, null, '2015-08-30', 'belum_lunas');

-- ----------------------------
-- Table structure for tunjangan_proyek
-- ----------------------------
DROP TABLE IF EXISTS "public"."tunjangan_proyek";
CREATE TABLE "public"."tunjangan_proyek" (
"id" int4 DEFAULT nextval('proyek_id_seq'::regclass) NOT NULL,
"nama_proyek" varchar(50) COLLATE "default",
"tunjangan_proyek" numeric(30)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tunjangan_proyek
-- ----------------------------
INSERT INTO "public"."tunjangan_proyek" VALUES ('3', 'Pembuatan Aplikasi Pemilu', '1500000');
INSERT INTO "public"."tunjangan_proyek" VALUES ('4', 'Pembuatan Aplikasi Android', '2000000');
INSERT INTO "public"."tunjangan_proyek" VALUES ('5', 'GRMS', '2000000');
INSERT INTO "public"."tunjangan_proyek" VALUES ('6', 'Bandit', '2000000');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table absensi
-- ----------------------------
ALTER TABLE "public"."absensi" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table bonus
-- ----------------------------
ALTER TABLE "public"."bonus" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table ci_sessions
-- ----------------------------
ALTER TABLE "public"."ci_sessions" ADD PRIMARY KEY ("session_id");

-- ----------------------------
-- Primary Key structure for table cobatambah
-- ----------------------------
ALTER TABLE "public"."cobatambah" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table divisi
-- ----------------------------
ALTER TABLE "public"."divisi" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table gaji
-- ----------------------------
ALTER TABLE "public"."gaji" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table golongan_gaji
-- ----------------------------
ALTER TABLE "public"."golongan_gaji" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jabatan
-- ----------------------------
ALTER TABLE "public"."jabatan" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table karyawan
-- ----------------------------
ALTER TABLE "public"."karyawan" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table lembur
-- ----------------------------
ALTER TABLE "public"."lembur" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table pinjaman
-- ----------------------------
ALTER TABLE "public"."pinjaman" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tunjangan_proyek
-- ----------------------------
ALTER TABLE "public"."tunjangan_proyek" ADD PRIMARY KEY ("id");
