<?php

$config = array(
    'pengawas/add/tenaga_ahli' => array(
        array(
            'field' => 'nama',
            'label' => 'nama',
            'rules' => 'required'
        ),
        array(
            'field' => 'jenis_pengawas',
            'label' => 'jenis pengawas',
            'rules' => 'required'
        ),
        array(
            'field' => 'ktp',
            'label' => 'ktp',
            'rules' => 'required'
        ),
        array(
            'field' => 'no_surat_ska',
            'label' => 'no ska',
            'rules' => 'required'
        ),
        array(
            'field' => 'pendidikan',
            'label' => 'pendidikan',
            'rules' => 'required'
        ),
        array(
            'field' => 'keahlian',
            'label' => 'keahlian',
            'rules' => 'required'
        )
    ),
    'penyedia/pengadaan_langsung/addendumtri/add_addendum_volume_harga' => array(
        array(
            'field' => 'tanggal',
            'label' => 'tanggal',
            'rules' => 'required'
        ),
         array(
            'field' => 'nilai_addendum',
            'label' => 'Nilai Addendum',
            'rules' => 'required'
        )
    ),    
    'pengawas/add/pphp' => array(
        array(
            'field' => 'nama',
            'label' => 'nama',
            'rules' => 'required'
        ),
        array(
            'field' => 'jenis_pengawas',
            'label' => 'jenis pengawas',
            'rules' => 'required'
        ),
        array(
            'field' => 'username',
            'label' => 'username',
            'rules' => 'required'
        ),

        array(
            'field' => 'no_surat_penunjukan',
            'label' => 'no surat penunjukan',
            'rules' => 'required'
        ),
        array(
            'field' => 'tgl_surat_penunjukan',
            'label' => 'tgl surat penunjukan',
            'rules' => 'required'
        ),
        array(
            'field' => 'pangkat',
            'label' => 'pangkat',
            'rules' => 'required'
        ),
        array(
            'field' => 'jabatan',
            'label' => 'jabatan',
            'rules' => 'required'
        )
    ),
    'pengawas/edit/tenaga_ahli' => array(
        array(
            'field' => 'nama',
            'label' => 'Nama',
            'rules' => 'required'
        ),
        array(
            'field' => 'ktp',
            'label' => 'Ktp',
            'rules' => 'required'
        ),
        array(
            'field' => 'pendidikan',
            'label' => 'Pendidikan',
            'rules' => 'required'
        ),
        array(
            'field' => 'keahlian',
            'label' => 'Keahlian',
            'rules' => 'required'
        ),
        array(
            'field' => 'no_surat_ska',
            'label' => 'No SKA',
            'rules' => 'required'
        )
    ),
    'pengawas/edit/pphp' => array(
        array(
            'field' => 'no_surat_penunjukan',
            'label' => 'No Surat Penunjukan',
            'rules' => 'required'
        ),
        array(
            'field' => 'tgl_surat_penunjukan',
            'label' => 'Tgl Surat Penunjukan',
            'rules' => 'required'
        )
    ),
    'panitia/add' => array(
        array(
            'field' => 'nomor',
            'label' => 'Nomor',
            'rules' => 'required'
        )
    ),
);
