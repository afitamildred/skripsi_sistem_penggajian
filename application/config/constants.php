<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

//define('CONS_TAHAP', '2');
//define('CONS_TAHUN', '2015');
define('DEFAULT_PAGE', '20');
define('spk', 'spk');
define('def_fade', '.fadeIn(200).delay(3000).fadeOut(200);');
define('field_empty', ".data('error-empty')).fadeIn(200).delay(2000).fadeOut(200);");
define('DEFAULT_NUM_LINKS', '5');
define('APP_CONFIG_UNIQ_KODE', '3');
define('ROLE_UNIQ_KODE_KARYAWAN','1');
define('ROLE_UNIQ_KODE_SUPERADMIN','2');
define('ROLE_UNIQ_KODE_ADMIN','4');
define('enck', 'Jaskhjc347#@ka!kas');
define('enckiv', mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND));


/* End of file constants.php */
/* Location: ./application/config/constants.php */