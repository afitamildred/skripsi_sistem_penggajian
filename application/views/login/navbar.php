<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="LPTI Pelataran Mataram">
        <title>Let's Start GRMS</title>
        <!-- plugin CSS -->
        <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/font-awesome.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/hover.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/dataTables.bootstrap.css'); ?>" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,700,400,600' rel='stylesheet' type='text/css'>
        <!-- Custom CSS -->
        <link href="<?php echo base_url('assets/css/style2.css'); ?>" rel="stylesheet">
        <link rel="icon" type="image/ico" href="<?php echo base_url('assets/images/icon_url.jpg'); ?>" href="favicon.ico">
    </head>
    <body>
        <header id="top" class="header navbar navbar-fixed-top">
            <?php
            $logged_in = $this->session->userdata('logged_in');
            if ($logged_in == TRUE) {
                echo '<a class="logout btn ' . ($logged_in == TRUE ? 'class="display_none"' : '') . '" href="#"><span class="fa fa-sign-out"></span>Logout</a>';
            }
            ?>
            <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    <img class="img-responsive" src="<?php echo base_url('assets/images/logo.png'); ?>">
                    <h3><strong>edelive</strong>ry</h3>
                </a>
            </div>
            <nav aria-expanded="false" style="height: 1px;">
                <ul id="nav" class="nav navbar-right top-nav">
                    <li>
                        <a href="#">About</a>
                        <ul>
                            <li><a href="#">The product</a></li>

                            <li><a href="#">Meet the team</a></li>
                        </ul>
                    </li>
                    <li class="">
                        <a class="hvr-underline-reveal parent" href="#"><i></i>Menu</a>
                        <div class="area_child">
                            <ul class="child">
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class=""><a class="hvr-underline-reveal" href="#"><i></i>e-budgeting</a></li>
                    <li class=""><a class="hvr-underline-reveal" href="#"><i></i>e-project</a></li>
                    <li class=""><a class="hvr-underline-reveal" href="#"><i></i>e-procurement</a></li>
                    <li class=""><a class="hvr-underline-reveal" href="#"><i></i>e-hsb</a></li>
                    <li class=""><a class="hvr-underline-reveal" href="#"><i></i>e-controlling</a></li>
                </ul>
            </nav>
        </header>

        <div id="page">
            <div id="main-sidebar">                
                <ul class="sidebar-nav">
                    <li>
                        <a href="#">
                            <div class="show_hidden"><i class="fa fa-outdent"></i> Lelang</div>
                        </a>
                        <ul class="section">
                            <li><a href="<?php echo base_url('ppk/lelang/kontrak'); ?>">Kontrak</a></li>
                            <li><a href="<?php echo base_url('ppk/lelang/draft_kontrak'); ?>">Draft Kontrak</a></li>
                            <li><a href="<?php echo base_url('ppk/lelang/addendum/daftar_addendum'); ?>">Addendum</a></li>
                        </ul>  
                    </li>
                    <li>
                        <a href="#">
                            <div class="show_hidden"><i class="fa fa-share-square"></i> Pengadaan Langsung</div>
                        </a>
                        <ul class="section">
                            <li><a href="<?php echo base_url('ppk/pengadaan_langsung/spk'); ?>">SPK</a></li>
                            <li><a href="<?php echo base_url('ppk/pengadaan_langsung/draft_spk'); ?>">Draft SPK</a></li>
                            <li><a href="<?php echo base_url('ppk/pengadaan_langsung/addendum/daftar_addendum'); ?>">Addendum</a></li>
                        </ul>  
                    </li>
                    <li>
                        <a href="#">
                            <div class="show_hidden"><i class="fa fa-hand-o-right"></i> Pemberian Langsung</div>
                        </a>
                        <ul class="section">
                            <li><a href="<?php echo base_url('ppk/pemberian_langsung/pekerjaan'); ?>">Pekerjaan</a></li>
                            <li><a href="<?php echo base_url('ppk/pemberian_langsung/spk'); ?>">SPK</a></li>
                        </ul>  
                    </li>
                    <li>
                        <a href="#">
                            <div class="show_hidden"><i class="fa fa-money"></i> Pembelian Langsung</div>
                        </a>
                        <ul class="section">
                            <li><a href="<?php echo base_url('ppk/pembelian_langsung/pekerjaan'); ?>">Pekerjaan</a></li>
                            <li><a href="<?php echo base_url('ppk/pembelian_langsung/spl'); ?>">SPL</a></li>
                        </ul>  
                    </li>
                    <li>
                        <a href="#">
                            <div class="show_hidden"><i class="fa fa-credit-card"></i> Pemb. Secara Elektronik</div>
                        </a>
                        <ul class="section">
                            <li><a href="<?php echo base_url('ppk/pemb_secara_elektronik/pekerjaan'); ?>">Pekerjaan</a></li>
                            <li><a href="<?php echo base_url('ppk/pemb_secara_elektronik/spl'); ?>">SPL</a></li>
                            <li><a href="<?php echo base_url('ppk/pemb_secara_elektronik/draft_spl'); ?>">Draft SPK</a></li>
                        </ul>  
                    </li>
    
                </ul>
            </div>
        </div>

        <!-- JavaScript -->
        <?php
        if (isset($scripts)) {
            foreach ($scripts as $script) {
                echo $script;
            }
        }
        ?>
        <script type="text/javascript">
            // sidebar menu
            $(function () {
                $('.sidebar-nav').metisMenu({
                    toggle: false
                });
            });
        </script>
    </body>

</html>