<div id="fancybox_wrap" style="min-width:700px;">
    <div class="box-title">
        <h3><?php echo $page_title; ?></h3>
    </div>

    <?php
    echo '<div id="alert_success" class="alert alert-success" style="display:none"></div>';
    echo '<div id="alert_pass_tidak_sama" class="alert alert-error" style="display:none"></div>';
    ?>
    <div class="box-inner">

        <div role="tabpanel">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="myTab">
                <li role="presentation" class="active"><a href="#profil" aria-controls="profil" role="tab" data-toggle="tab">Edit Profil</a></li>
                <li role="presentation"><a href="#user_password" aria-controls="user_password" role="tab" data-toggle="tab">Edit Password</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content" id="myTabContent">
                <div role="tabpanel" class="tab-pane fade active in active form-horizontal" id="profil">
                    <?php
                    echo form_open_multipart($urlnya . '', array('id' => 'form_ubah_profil'));
                    echo form_input($username);
                    ?>
                    <input type="hidden" name="targetData" value="true">
                    <input type="hidden" name="url" value="<?php echo current_url() ?>">
                    <input type="hidden" name="targetForm" value="edit_profil">
                    <br>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Avatar</label>
                        <div class="col-md-9">  
                            <?php
                            if ($file_avatarDB != '') {
                                echo '<img src="' . base_url('assets/uploads/avatar/' . $file_avatarDB) . '" style="width:50px;" /> Ganti dengan, <br />';
                            }
                            echo form_upload($file_avatar);
                            ?>                  
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Email</label>
                        <div class="col-md-9">   
                            <?php
                            echo form_input($email);
                            ?>  
                            <label class="error alert-error" id="error_email" style="display:none"></label>                       
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <?php echo form_submit($submitin); ?>
                        </div>
                    </div>

                    <?php
                    echo form_close();
                    ?>
                </div>
                <div role="tabpanel" class="tab-pane fade in form-horizontal" id="user_password">
                    <?php
                    echo form_open($urlnya . '/edit_password', array('id' => 'form_ubah_password'));
                    echo form_input($username);
                    ?>
                    <input type="hidden" name="targetData" value="true">
                    <input type="hidden" name="url" value="<?php echo current_url() ?>">
                    <input type="hidden" name="targetForm" value="edit_password">

                    <br />
                    <div class="form-group">
                        <label class="col-md-3 control-label">Password Lama</label>
                        <div class="col-md-9">
                            <?php
                            echo form_password($password_lama);
                            ?>
                            <img src="<?php echo base_url('assets/images'); ?>/loading_big.gif" id="loading" style="display:none" />
                            <label class="error alert-error error_password_lama" id="message" style="display:none">Password lama salah.</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Password Baru</label>
                        <div class="col-md-9">   
                            <?php
                            echo form_password($password_baru);
                            ?>              
                            <label class="error alert-error" id="error_pass_baru" style="display:none"></label>           
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Konfirmasi</label>
                        <div class="col-md-9">   
                            <?php
                            echo form_password($password_lagi);
                            ?>
                            <label class="error alert-error" id="error_pass_lagi" style="display:none"></label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <?php
                            echo form_submit($submitin_password);
                            ?>
                        </div>
                    </div>

                    <?php
                    echo form_close();
                    ?>
                </div>
            </div>

        </div>

    </div>
</div>

<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js'); ?>"></script>
<script>
    $(document).ready(function () {
        $('#myTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
        //ajax alert ganti avatar
        var frm = $('#form_ubah_profil');
        frm.on('submit', function (ev) {
            console.log(frm.serializeArray());
            var regex_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            if (!$('#email').val()) {
                $('#submitin').val('Lengkapi Data').removeClass('btn-primary').addClass('btn-warning');
                $('#error_email').html($('#email').data('error-empty')).fadeIn(200).delay(2000).fadeOut(200);
            } else if (!regex_email.test($('#email').val())) {
                $('#submitin').val('Lengkapi Data').removeClass('btn-primary').addClass('btn-warning');
                $('#error_email').html("Email tidak valid!").fadeIn(200).delay(2000).fadeOut(200);
            } else {
                var datas = new FormData(frm.serializeArray());
                $.ajax({
                    type: 'POST',
                    url: frm.attr('action'),
                    data: datas,
                    success: function (data) {
                        $('#submitin').val('Password berhasil diperbarui').removeClass('btn-primary').addClass('btn-success');
                        $('#alert_success').html("Profil berhasil diperbarui").fadeIn(200).delay(2000).fadeOut(200);
                        window.setTimeout(function () {
                            $('#reload').load(frm.attr('data-reload'));
                            $.fancybox.close();
                        }, 500);
                    }
                });
            }
            ev.preventDefault();
        });

        // make loader hidden in start
        $('#loading').hide();
        $('#password_lama').on('blur', function () {
            var password_lama_val = $("#password_lama").val();
            // show loader
            $('#loading').show().delay(300).fadeOut();
            $.post("<?php echo base_url('login/password_lama_check') ?>", {
                password_lama: password_lama_val
            }, function (data) {
                if (data.message == false) {
                    $('#message').fadeIn(200).delay(2000).fadeOut(200);
                } else {
                    $('#message').hide();
                    $("#submitin_password").prop('disabled', false);
                }
            }, 'json');
            return false;
        });
        //ajax alert ganti password
        var frm = $('#form_ubah_password');
        frm.on('submit', function (ev) {
            console.log(frm.serializeArray());
            if (!$('#password_lama').val()) {//tidak mengisi password lama
                $('#submitin_password').val('Lengkapi Data').removeClass('btn-primary').addClass('btn-warning');
                $('#message').html($('#message').data('error-empty')).fadeIn(200).delay(2000).fadeOut(200);
            } else if (!$('#password_baru').val()) {//tidak mengisi password baru
                $('#submitin_password').val('Lengkapi Data').removeClass('btn-primary').addClass('btn-warning');
                $('#error_pass_baru').html($('#password_baru').data('error-empty')).fadeIn(200).delay(2000).fadeOut(200);
            } else if (!$('#password_lagi').val()) {//tidak mengisi password lagi
                $('#submitin_password').val('Lengkapi Data').removeClass('btn-primary').addClass('btn-warning');
                $('#error_pass_lagi').html($('#password_lagi').data('error-empty')).fadeIn(200).delay(2000).fadeOut(200);
            } else if ($('#password_baru').val() != $('#password_lagi').val()) {//password baru != lama
                $('#submitin_password').val('Lengkapi Data').removeClass('btn-primary').addClass('btn-warning');
                $('#alert_pass_tidak_sama').html("Password tidak sama!").fadeIn(200).delay(2000).fadeOut(200);
            }
            else {
//                var datas = new FormData(frm.serializeArray());
//                $.ajax({
//                    type: 'POST',
//                    url: frm.attr('action'),
//                    data: datas,
//                    success: function (data) {
//                        $('#submitin_password').val('Password berhasil diperbarui').removeClass('btn-primary').addClass('btn-success');
//                        $('#alert_success').html("Password berhasil disimpan!").fadeIn(200).delay(2000).fadeOut(200);
//                        window.setTimeout(function () {
//                            $('#reload').load(frm.attr('data-reload'));
//                            $.fancybox.close();
//                        }, 500);
//                    }
//                });
                
                var datas = new FormData(frm.serializeArray());
                $.ajax({
                    type: 'POST',
                    url: frm.attr('action'),
                    //data: frm.serializeArray(),
                    data: datas,
                    success: function (data) {
                        $('#submitin_password').val('Password berhasil diperbarui').removeClass('btn-primary').addClass('btn-success');
                        $('#alert_success').html("Password berhasil disimpan!").fadeIn(200).delay(2000).fadeOut(200);
                        window.setTimeout(function () {
                            $('#reload').load(frm.attr('data-reload'));
                            $.fancybox.close();
                        }, 500);
//                        window.setTimeout(function () {
//                            $('#reload').load(frm.attr('data-reload'));
//                            parent.jQuery.fancybox.close(true);
//                            $.fn.fancybox.close(true);
//                            $.fn.fancybox.close();
//                            parent.window.document.getElementsClassName('fancybox-opened').style.overflow = 'visible !important';
//                            parent.window.document.getElementById('fancybox_wrap').style.display = 'none';
//                            //
//                            parent.$.fancybox.close(true);
//                            parent.$.fancybox.close();
//                            console.log('tried to close');
//                        }, 500);
                    }
                    
                });
            }
            ev.preventDefault();
        });
    });
</script>