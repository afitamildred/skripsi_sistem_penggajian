<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Let's start GRMS</title>

        <!-- plugin CSS -->
        <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/font-awesome.css'); ?>" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,700,400,600' rel='stylesheet' type='text/css'>

        <!-- Custom CSS -->
        <link href="<?php echo base_url('assets/demo/style.css'); ?>" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <header class="header navbar navbar-fixed-top" role="banner">
            <div class="container-fluid">
                <div class="brand">
                    <!-- Logo -->
                    <a class="navbar-brand" href="index.html">
                        <img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="logo" />
                        <h3><strong>grms</strong>jateng</h3>
                    </a>

                </div>



                <ul class="nav navbar-nav navbar-right">
                    <li><a href="http://jgrms.jatengprov.go.id/ebgbaru">e-budgeting</a></li>
                    <li><a href="http://jgrms.jatengprov.go.id/eproject">e-project</a></li>
                    <li><a href="#">e-procurement</a></li>
                    <li><a href="#">e-delivery</a></li>
                    <li><a href="#">e-controlling</a></li>
                </ul>
            </div>
        </header>

        <div class="wrap">
            <div class="container">
                <div class="col-md-8 col-sm-8 col-xs-8">
                    <div class="login-box">
                        <div class="konten">
                            <h3>Apa Itu e-delivery?</h3>
                            <p>Sistem Penggajian</p>
                        </div>

                    </div>

                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="login">
                        <div class="login-screen">
                            <div class="app-title">
                                <h3>Login User</h3>
                            </div><br>
                            <?php
                            echo form_open($action, 'id="form_validate"');

                            $flash_failed_message = $this->session->flashdata('failed_message');
                            echo!empty($flash_failed_message) ? '<div class="alert alert-error"><i class="fa fa-warning"></i> ' . $flash_failed_message . '</div>' : '';
                            ?>
                            <label class="login-fail">
                                <?php
                                if ($this->session->userdata('error_message')) {
                                    echo '<br /><div class="alert alert-error">' . $this->session->userdata('error_message') . "</div>";
                                }
                                ?>
                            </label>

                            <div class="login-form">

                                <div class="control-group">
                                    <?php echo form_input('username', set_value('username', isset($username) ? $username : ''), 'class="form-control required" minlength="1" ' . ('placeholder="Masukkan username..."') . '') ?>            
                                    <br />
                                    <?php echo form_error('username'); ?>
                                    <br />
                                    <?php echo form_password('password', set_value('password', isset($password) ? $password : ''), 'class="form-control required" minlength="1" ' . ('placeholder="Masukkan password..."') . '') ?>
                                    <br>
                                    <?php echo form_error('password'); ?>
                                </div>

                                <div class="control-group">
                                </div>

                            </div>

                            <div class="control-group">
                                <div class="chap">
                                    <img class="img-responsive" src="<?php echo base_url('captcha/'); ?>" alt="captcha" /> <!--src="http://c22blog.files.wordpress.com/2010/10/input-black.gif">-->
                                </div>
                                <?php echo anchor(base_url($urlnya . '/'), 'Ganti Kode Lain'); ?>
                                <?php echo form_input($captcha); ?>
                                <br />
                                <?php echo form_error('captcha'); ?>
                                <?php
                                echo form_submit($submitin);
                                echo form_close();
                                ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="copyright">
            <span>&copy 2015 <a href="#">LPTI Pelataran Mataram Yogyakarta</a></span>
        </div>
    </div>
    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
    <script>
        // When the document is ready
        $(document).ready(function() {
            //validation rules                       
            $("#form_validate").validate({
                rules: {
                    "username": {
                        required: true
                    },
                    "password": {
                        required: true
                    },
                    "captcha": {
                        required: true
                    }
                }
            });
        });
    </script>
</body>

</html>
