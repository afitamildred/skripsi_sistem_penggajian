<div id="konten" class="container">
    <div class="col-md-8">
        <h1 class="judul_sistem_login">e-Delivery</h1>
        <ul class="nav nav-pills">
            <li><a href="<?php echo base_url(); ?>#"><i class="fa fa fa-warning"></i>&nbsp; Alur</a></li>
            <li><a href="<?php echo base_url(); ?>#"><i class="fa fa-book"></i>&nbsp; Manual</a></li>
            <li><a href="<?php echo base_url(); ?>#"><i class="fa fa-download"></i>&nbsp; Download SOAP</a></li>
        </ul>
        <h3>Apa Itu e-Delivery?</h3>
        <p>e-Delivery adalah Sistem Pendukung Administrasi Kegiatan yang didalamnya termasuk program komputer berbasis web untuk memfasilitasi kebutuhan pembuatan kontrak pengadaan barang/jasa dan penyediaan dokumen-dokumen kelengkapan.</p>
    </div>
    <div id="form_login" class="col-md-4">
        <h2>Login User</h2><br/>
        <?php
        echo form_open($action, 'id="form_validate"');

        $flash_failed_message = $this->session->flashdata('failed_message');
        echo!empty($flash_failed_message) ? '<div class="alert alert-error"><i class="fa fa-warning"></i> ' . $flash_failed_message . '</div>' : '';
        ?>

        <div class="form-group">
            <label for="exampleInputEmail1">Username</label>
            <?php echo form_input('username', set_value('username', isset($username) ? $username : ''), 'class="form-control required" minlength="1" ' . ('placeholder="Masukkan username..."') . '') ?>
            <?php echo form_error('username'); ?>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <?php echo form_password('password', set_value('password', isset($password) ? $password : ''), 'class="form-control required" minlength="1" ' . ('placeholder="Masukkan password..."') . '') ?>
            <?php echo form_error('password'); ?>
        </div>

        <div class="form-group">
            <div class="chap">
                <img class="img-responsive" src="<?php echo base_url('captcha/'); ?>" alt="captcha" />
            </div>

            <?php
            echo anchor(base_url($urlnya . '/'), '<br/>Ganti Kode Lain<br/>');
            echo form_input($captcha);
            ?>
            <br />
            <?php
            echo form_error('captcha');
            if ($this->session->userdata('error_message')) {
                echo '<br /><div class="alert alert-error">' . $this->session->userdata('error_message') . "</div>";
            }
            ?>

        </div>
        <?php
        echo form_submit($submitin);
        echo form_close();
        ?>
    </div>
</div>
<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
<script>
    // When the document is ready
    $(document).ready(function () {
        //validation rules                       
        $("#form_validate").validate({
            rules: {
                "username": {
                    required: true
                },
                "password": {
                    required: true
                },
                "captcha": {
                    required: true
                }
            }
        });
    });
</script>