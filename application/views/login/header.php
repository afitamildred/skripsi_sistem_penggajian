<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="LPTI Pelataran Mataram">
        <title>Let's Start GRMS</title>
        <!-- plugin CSS -->
        <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/font-awesome.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/hover.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/dataTables.bootstrap.css'); ?>" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,700,400,600' rel='stylesheet' type='text/css'>
        <!-- Custom CSS -->
        <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
        <link rel="icon" type="image/ico" href="<?php echo base_url('assets/images/icon_url.jpg'); ?>" href="favicon.ico">
    </head>
    <body>
        <header id="top" class="header navbar navbar-fixed-top">
            <?php
            $logged_in = $this->session->userdata('logged_in');
            if ($logged_in == TRUE) {
                echo '<a class="logout btn ' . ($logged_in == TRUE ? 'class="display_none"' : '') . '" href="#"><span class="fa fa-sign-out"></span>Logout</a>';
            }
            ?>
            <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    <img class="img-responsive" src="<?php echo base_url('assets/images/logo.png'); ?>">
                    <h3><strong>edelive</strong>ry</h3>
                </a>
            </div>
            <nav id="links_sistem" aria-expanded="false" style="height: 1px;">
                <ul class="nav navbar-nav navbar-right top-nav">
                    <li class="extra"><a class="hvr-underline-reveal" href="#"><i></i>e-budgeting</a></li>
                    <li class="extra"><a class="hvr-underline-reveal" href="#"><i></i>e-project</a></li>
                    <li class="extra"><a class="hvr-underline-reveal" href="#"><i></i>e-procurement</a></li>
                    <li class="extra"><a class="hvr-underline-reveal" href="#"><i></i>e-hsb</a></li>
                    <li class="extra"><a class="hvr-underline-reveal" href="#"><i></i>e-controlling</a></li>
                </ul>
            </nav>
        </header>