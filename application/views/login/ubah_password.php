<div class="box" style="min-width: 500px">
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4>Ubah Password </h4>
        </div>
    </div>
    <div id="alert_success" class="panel panel-success" style="display:none">
        <div class="panel-heading">
            <h4>Password berhasil disimpan!</h4>
        </div>
    </div>
    <div id="alert_pass_tidak_sama" class="panel panel-danger" style="display:none">
        <div class="panel-heading">
            <h4>Password tidak sama!</h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div id="main" class="col-lg-12">
                <?php
                echo form_open('login/proses_ubah_password', array('id' => 'form_ubah_password'));
                ?>
                <input type="hidden" name="targetData" value="true">
                <input type="hidden" name="url" value="<?php echo current_url() ?>">
                <input type="hidden" name="targetForm" value="proses_ubah_password">

                <table width="100%" border="0" cellspacing="1" cellpadding="3">
                    <tr>
                        <td class="col-md-5">Password Lama</td>
                        <td>
                            <input type="password" id="password_lama" data-error-empty="Password lama harus diisi" class="form-control col-md-7" name="password_lama" placeholder="Password Lama" />
                            <img src="<?php echo base_url('assets/images'); ?>/ezgif.com-gif-maker.gif" id="loading" style="display:none" />
                            <label class="error alert-error error_password_lama" id="message" style="display:none">Password lama salah.</label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-5">Password Baru</td>
                        <td>
                            <input type="password" id="password_baru" data-error-empty="Password baru harus diisi" class="form-control col-md-7" name="password_baru" placeholder="Password Baru" />
                            <label class="error alert-error" id="error_pass_baru" style="display:none"></label> 
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-5">Konfirmasi Password</td>
                        <td>
                            <input type="password" id="password_lagi" data-error-empty="Ketik ulang password baru" class="form-control col-md-7" name="password_lagi" placeholder="Password Baru Lagi" />
                            <label class="error alert-error" id="error_pass_lagi" style="display:none"></label>
                        </td>
                    </tr>
                </table>

                <input type="submit" name="submitin" value="Lengkapi Data" id="submitin" class="btn pull-right btn-warning" disabled="disabled" >

                <?php
                echo form_close();
                ?>
            </div>
        </div>
    </div>
</div>
<script src="//<?php echo base_url('assets/js/jquery-1.11.0.js'); ?>"></script>
<script>
    $(document).ready(function () {
        // make loader hidden in start
        $('#loading').hide();
        $('#password_lama').on('blur', function () {
            var password_lama_val = $("#password_lama").val();

            $('#loading').show().delay(300).fadeOut();
            $.post("<?php echo base_url('login/password_lama_check') ?>", {
                password_lama: password_lama_val
            }, function (data) {
                if (data.message == false) {
                    $('#message').fadeIn(200).delay(2000).fadeOut(200);
                } else {
                    $('#message').hide();
                    $("#submitin").prop('disabled', false);
                }
            }, 'json');
            return false;
        });
        //ajax alert ganti password
        var frm = $('#form_ubah_password');
        frm.on('submit', function (ev) {
            console.log(frm.serializeArray());
            if (!$('#password_lama').val()) {//tidak mengisi password lama
                $('#submitin').val('Lengkapi Data').removeClass('btn-primary').addClass('btn-warning');
                $('#message').html($('#message').data('error-empty')).fadeIn(200).delay(2000).fadeOut(200);
            } else if (!$('#password_baru').val()) {//tidak mengisi password baru
                $('#submitin').val('Lengkapi Data').removeClass('btn-primary').addClass('btn-warning');
                $('#error_pass_baru').html($('#password_baru').data('error-empty')).fadeIn(200).delay(2000).fadeOut(200);
            } else if (!$('#password_lagi').val()) {//tidak mengisi password lagi
                $('#submitin').val('Lengkapi Data').removeClass('btn-primary').addClass('btn-warning');
                $('#error_pass_lagi').html($('#password_lagi').data('error-empty')).fadeIn(200).delay(2000).fadeOut(200);
            } else if ($('#password_baru').val() != $('#password_lagi').val()) {//password baru != lama
                $('#submitin').val('Lengkapi Data').removeClass('btn-primary').addClass('btn-warning');
                $('#alert_pass_tidak_sama').fadeIn(200).delay(2000).fadeOut(200);
            }
            else {
                var datas = new FormData(frm.serializeArray());
                $.ajax({
                    type: 'POST',
                    url: frm.attr('action'),
                    //data: frm.serializeArray(),
                    data: datas,
                    success: function (data) {
                        $('#submitin').val('Password berhasil diperbarui').removeClass('btn-primary').addClass('btn-success');
                        $('#alert_success').fadeIn(200).delay(2000).fadeOut(200);
                        window.setTimeout(function () {
                            $('#reload').load(frm.attr('data-reload'));
                            $.fancybox.close();
                        }, 500);
                    }

                });
            }
            ev.preventDefault();
        });
    });
</script>