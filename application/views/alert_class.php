
<div id="tambah-alert" class="alert alert-success" style="display:none" role="alert">
    Data berhasil <strong>ditambahkan.</strong>
</div>
<div id="ubah-alert" class="alert alert-success" style="display:none" role="alert">
    Data berhasil <strong>diubah.</strong>
</div>
<div id="hapus-alert" class="alert alert-danger" style="display:none" role="alert">
    Data berhasil <strong>dihapus.</strong>
</div>