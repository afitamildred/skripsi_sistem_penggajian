<div class="col-lg-10">
    <div class="breadcrumb-wrapper">            
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('superadmin/dashboard'); ?>">Home</a></li>
            <li class="active">Data Lembur Karyawan</li>
        </ol>
    </div>         
    <div class="box">
        <div class="box-title">
            <h2><?php echo $page_title; ?></h2>
        </div>
        <div class="clear"></div>
        <div class="box-inner" id="reload">
            
            <fieldset>
                <div class="form-group">
                    <div class="col-md-10" style="padding: 0px !important;">
                        <input name="cari" type="text" class="form-control" placeholder="Cari data karyawan berdasarkan nama atau divisi . . .">
                    </div>
                    <div class="col-md-2 pull-right" style="padding: 0px !important; text-align: right">
                        <a class='btn btn-success conbtn fancybox.ajax' align="right" data-toggle='tooltip' data-placement='top' title='Cetak Data Lembur' href='<?php echo base_url($urlnya . '/cetak_data'); ?>'>Cetak Data Lembur</a>
                    </div>
                </div>
            </fieldset>
            <div class="table-responsive">
                <table class="display table table-striped table-bordered" id="table">
                    <thead>
                        <tr class="info">
                            <th class="text-center">No.</th>
                            <th class="text-center">Nama Karyawan</th>
                            <th class="text-center">Divisi</th>
                            <th class="text-center">Tanggal Lembur</th>
                            <th class="text-center">Jumlah Jam</th>
                            <th class="text-center">Jumlah/Jam</th>
                            <th class="text-center">Jumlah Bonus</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data_lembur as $row) {
                            $data_kary = $this->lembur_m->view('view_data_karyawan', array('username' => $row->username));
                            $divisi = $data_kary[0]->id_divisi;
                            $jabatan = $data_kary[0]->id_jabatan;
                            $status_kary = $data_kary[0]->status_karyawan;
                            $data['jml_bonus'] = $this->lembur_m->view('golongan_gaji', array(
                                'id_divisi' => $divisi,
                                'id_jabatan' => $jabatan,
                                'status_karyawan' => $status_kary
                                    )
                            );
                            ?> 
                            <tr>
                                <td class='text-center'><?php echo $no; ?></td>
                                <td><?php echo $row->nama_karyawan; ?></td>
                                <td><?php echo $row->nama_divisi; ?></td>
                                <td><?php echo $row->tanggal_lembur; ?></td>
                                <td><?php echo $row->jumlah_jam; ?></td>
                                <td align='right'><?php echo format_rupiah($data['jml_bonus'][0]->lembur_perjam); ?></td>
                                <td align='right'><?php echo format_rupiah($row->jumlah_bonus); ?></td>
                                <?php
                                $no++;
                            }
                            ?>
                    </tbody>
                </table>
                <div id="pages">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>