<div class="col-lg-10">
    <div class="breadcrumb-wrapper">            
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('superadmin/dashboard'); ?>">Home</a></li>
            <li class="active">Data Karyawan</li>
        </ol>
    </div>         
    <div class="box">
        <div class="box-title">
            <h2><?php echo $page_title; ?></h2>
        </div>
        <div class="clear"></div>
        <div class="box-inner" id="reload">
            <fieldset>
                <div class="form-group">
                    <div class="col-md-10" style="padding: 0px !important;">
                        <input name="cari" type="text" class="form-control" placeholder="Cari data karyawan berdasarkan nama atau divisi . . .">
                    </div>
                    <div class="col-md-2 pull-right" style="padding: 0px !important; text-align: right">
                        <a class='btn btn-success conbtn fancybox.ajax' align="right" data-toggle='tooltip' data-placement='top' title='Cetak Data Karyawan' href='<?php echo base_url($urlnya . '/cetak_data_karyawan'); ?>'>Cetak Data Karyawan</a>
                    </div>
                </div>
            </fieldset>
            <div class="table-responsive">
                <table class="display table table-striped table-bordered" id="table">
                    <thead>
                        <tr class="info">
                            <th class="text-center" width="3%">No.</th>
                            <th class="text-center" width="10%">Nama Karyawan</th>
                            <th class="text-center" width="10%">Username</th>
                            <th class="text-center" width="10%">Divisi</th>
                            <th class="text-center" width="10%">Jabatan</th>
                            <th class="text-center" width="10%">No. KTP</th>
                            <th class="text-center" width="10%">No. HP</th>
                            <th class="text-center" width="10%">Gaji Pokok</th>
                            <th class="text-center" width="10%">Tunjangan Proyek</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data_karyawan as $row) {
                            $data_gapok = $this->karyawan_m->view('golongan_gaji', array('id_divisi' => $row->id_divisi, 'id_jabatan' => $row->id_jabatan));
                            ?> 
                            <tr>
                                <td class='text-center'><?php echo $no; ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo $row->username; ?></td>
                                <td><?php echo $row->nama_divisi; ?></td>
                                <td><?php echo $row->nama_jabatan; ?></td>
                                <td class='text-center'><?php echo $row->no_ktp; ?></td>
                                <td class='text-center'><?php echo $row->no_hp; ?></td>
                                <td align="right"><?php echo format_rupiah($data_gapok[0]->gaji_pokok); ?></td>
                                <td align="right"><?php echo format_rupiah($row->tunjangan_proyek); ?></td>

                                <?php
                                $no++;
                            }
                            ?>
                    </tbody>
                </table>
                <div id="pages">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
