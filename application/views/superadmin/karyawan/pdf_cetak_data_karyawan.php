<style type="text/css">
    @media all {.page-break { display: block; page-break-before: always; size: landscape }}
    @page {size: 33cm 21.5cm; margin-left: 2cm; margin-bottom: 2cm; margin-right: 2cm; margin-top: 2cm;}
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
    tbody {
        display:table-row-group;
    }
    .txt-l {text-align: left;}
    .txt-c {text-align: center;}
    .txt-r {text-align: right;}
    .txt-j {text-align: justify;}
    .p1 {line-height: 150%; font-size: 12px;}
    .table-bordered td { 
        padding: 5px;
        font-size: 12px;
    }
    .table {
        border-collapse: collapse !important;
    }
    .table td,
    .table th {
        background-color: #fff !important;
    }
    .tahoma10 {
        font-family: Tahoma;
        font-size: 10px;
        text-decoration: none;
    }
    .tahoma10 {
        font-family: Tahoma;
        font-size: 10px;
        text-decoration: none;
    }
    .header20 {
        font-family: Tahoma;
        font-size: 20px;
        text-decoration: none;
        padding: 10px;
        margin: 10px;
    }
    .tahoma11 {
        font-family: Tahoma;
        font-size: 11px;
        text-decoration: none;
        margin: 2px;
        padding: 2px;
        border-top: 1px;
    }
    .tahoma12 {
        font-family: Tahoma;
        font-size: 12px;
        text-decoration: none;
        margin: 2px;
        padding: 2px;
        border-top: 1px;
    }
    .tahoma14 {
        font-family: Tahoma;
        font-size: 14px;
        text-decoration: none;
        margin: 2px;
        padding: 2px;
        border-top: 1px;
    }
    .tahoma16 {
        font-family: Tahoma;
        font-size: 16px;
        text-decoration: none;
    }
    .border-tlb {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-right-style: none !important;
        border-bottom-style: solid !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-lb {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: none !important;
        border-bottom-style: solid !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-lr {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: solid !important;
        border-bottom-style: none !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-r {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: solid !important;
        border-bottom-style: none !important;
        border-left-style: none !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-l {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: none !important;
        border-bottom-style: none !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-tlbr {
        border: 1.2px solid #000000 !important;
    }
    .border-lbr {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: solid !important;
        border-bottom-style: solid !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-tb {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-right-style: none !important;
        border-bottom-style: solid !important;
        border-left-style: none !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-tlb {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-bottom-style: solid !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-tbr {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-right-style: solid !important;
        border-bottom-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-tl {
        border-top-width: 1.2px !important;
        border-left-width: 1.2px !important;

        border-top-style: solid !important;
        border-left-style: solid !important;

        border-top-color: #000000 !important;
        border-left-color: #000000 !important;

    }    
    .border-br {
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-right-style: solid !important;
        border-bottom-style: solid !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
    }
    .border-tr {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-right-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-b {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: none !important;
        border-bottom-style: solid !important;
        border-left-style: none !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-t {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-right-style: none !important;
        border-bottom-style: none !important;
        border-left-style: none !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .style1 {
        font-weight: bold  !important;
    }
    .border-right{
        border-right:1.2px solid #000000 !important;
    }
    .border-missing {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: none !important;
        border-bottom-style: solid !important;
        border-left-style: none !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
</style>
<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div id="print-area-2" class="print-area" style="max-width: 1400px; min-height: 305px">

                    <table class="table table-bordered" cellspacing="0" width="100%">
                        <tr class="tahoma14">
                            <td class="border-tlbr border-tlb txt-c" width="25%"><img src="<?php echo base_url('assets'); ?>/images/logo.png" width="" height="40"></td>
                            <td class="border-tlbr txt-c" width="60%" colspan="7">
                                <strong>LAPORAN DATA KARYAWAN<br>
                                </strong>
                            </td>
                        </tr>
                    </table><br>


                    <table class="table table-bordered" cellspacing="0" width="100%" style="margin-top: 20px !important">
                        <thead>
                            <tr class="tahoma12">
                                <td height="17" class="border-tlb"><div align="center"><strong>No</strong></div></td>
                                <td height="17" class="border-tlb"><div align="center"><strong>Nama Karyawan </strong></div></td>
                                <td height="17" class="border-tlb"><div align="center"><strong>NIK</strong></div></td>
                                <td height="17" class="border-tlb"><div align="center"><strong>Divisi</strong></div></td>
                                <td height="17" class="border-tlb"><div align="center"><strong>Jabatan </strong></div>      <div align="center"></div></td>
                                <td height="17" class="border-tlb"><div align="center"><strong>Status Karyawan</strong></div></td>
                                <td height="17" class="border-tlbr"><div align="center"><strong>No HP</strong></div></td>
                                <td height="17" class="border-tlbr"><div align="center"><strong>Gaji Pokok</strong></div></td>                                                    
                                <td height="17" class="border-tlbr"><div align="center"><strong>Tunjangan Proyek</strong></div></td>


                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($data_karyawan as $row) {
                                $data_gapok = $this->karyawan_m->view('golongan_gaji', array('id_divisi' => $row->id_divisi, 'id_jabatan' => $row->id_jabatan));
                                ?> 
                                <tr class="tahoma12">
                                    <td class='center-x border-lb' align="center"><?php echo $no; ?></td>
                                    <td class="border-lb"><?php echo $row->nama; ?></td>
                                    <td class="border-lb"><?php echo $row->username; ?></td>
                                    <td class="border-lb"> <?php echo $row->nama_divisi; ?></td>
                                    <td class="border-lb"><?php echo $row->nama_jabatan; ?></td>
                                    <td class="border-lb"><?php echo $row->status_karyawan; ?></td>
                                    <td class="border-lbr"><?php echo $row->no_hp; ?></td>
                                    <td class="border-lbr" align="right"><?php echo format_rupiah($data_gapok[0]->gaji_pokok); ?></td>
                                    <td class="border-lbr" align="right"><?php echo format_rupiah($row->tunjangan_proyek); ?></td>

                                    <?php
                                    $no++;
                                }
                                ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>