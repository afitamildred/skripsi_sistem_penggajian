<fieldset>
    <div class="form-group">
        <div class="col-md-12" style="padding: 0px !important;">
            <input name="cari" type="text" class="form-control" placeholder="Cari data berdasarkan nama yang akan dicari.">
        </div>
    </div>
</fieldset>
<?php
$flash_success_message = $this->session->flashdata('success_message');
echo!empty($flash_success_message) ? '<div id="alert" class="alert alert-success">' . $flash_success_message . '</div>' : '';
?>
<?php echo $this->load->view('alert_class.php'); ?>
<div class="table-responsive">
    <table class="display table table-striped table-bordered" id="table">
        <thead>
            <tr class="info">
                <th class="center-x">No.</th>
                <th class="center-x">Nama Admine</th>
                <th class="center-x">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach ($data_jabatan as $row) {
                $row->uniq_kode;
                ?> 
                <tr>
                    <td class='center-x'><?php echo $no; ?></td>
                    <td><?php echo $row->user_name; ?></td>
                    <td class='center-x'>
                        <a class='btn btn-warning conbtn fancybox.ajax' data-toggle='tooltip' data-placement='top' title='Edit' href='<?php echo base_url($urlnya . '/edit/' . $row->user_name); ?>'><i class="fa fa-external-link"></i></a>
                        <a class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus' href='<?php echo base_url($urlnya . '/hapus/' . $row->uniq_kode); ?>'><i class="fa fa-list-ol"></i></a>
                    </td>

                    <?php
                    $no++;
                }
                ?>
        </tbody>
    </table>
    <div id="pages">
        <ul class="pagination">

        </ul>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.page').on('click', function(ev) {
            $.ajax({
                type: 'POST',
                url: $(".page").data('url'),
                data: $('.page').serializeArray(),
                success: function(data) {
                    $("#reload").load($(".page").attr('data-url')).fadeIn(200).delay(2000);
                }
            });
            ev.preventDefault();
        });
    });

    oTable = $('#table').dataTable({
        "paging": true,
        "ordering": false,
        "info": false,
        "bFilter": true,
        "bLengthChange": false
    });
    $('input[name=cari]').keyup(function() {
        oTable.fnFilter(this.value);
    });

</script>