<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
<script>
    // When the document is ready
    $(document).ready(function() {
        //validation rules                       
        $("#form_validate").validate({
            rules: {
                "nama_admin": {
                    required: true
                }
            }
        });
    });
</script>
<div style="min-width:600px;">

    <div class="box-title">
        <h3>Edit Data Admin</h3> 
    </div>
    <div class="box-inner form-horizontal">
        <div id="url" class="hidden" data-alamat="<?php echo base_url($urlnya . '/get_data/') ?>"></div>
        <?php echo form_open($urlnya . '/simpan_edit', array('id' => 'form-tambah', 'data-reload' => base_url($urlnya . '/index?data=reload'))); ?>
        <input type="hidden" name="targetData" value="true">  
       <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Nama Admin</label>
                <div class="col-md-9">   
                    <?php echo form_input($nama_admin); ?>
                    <?php echo form_input($id); ?>
                    <?php echo form_error('nama_admin'); ?>
                    <div class="error_popups" id="error-username"></div>
                </div>
            </div>
        </fieldset>

        <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-10">
                    <?php echo form_submit($submitin); ?>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
</div>
