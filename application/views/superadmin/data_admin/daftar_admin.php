<div class="col-lg-10">
    <div class="breadcrumb-wrapper">            
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('superadmin/dashboard'); ?>">Home</a></li>
            <li class="active">Daftar Admin</li>
        </ol>
    </div>         
    <div class="box">
        <div class="box-title">
            <a class='btn btn-primary conbtn_autocomplete fancybox.ajax pull-right col-sm-2' style="text-align:center;margin-top: 0px;padding: 8px;" data-toggle='tooltip' data-placement='top' title='Tambah Data Admin' href='<?php echo base_url($urlnya . '/add_admin'); ?>'>
                Tambah
            </a>
            <h2><?php echo $page_title; ?></h2>

        </div>
        <div class="clear"></div>
        <div class="box-inner" id="reload">
            <fieldset>
                <div class="form-group">
                    <div class="col-md-10" style="padding: 0px !important;">
                        <input name="cari" type="text" class="form-control" placeholder="Cari data karyawan berdasarkan nama atau divisi . . .">
                    </div>
                </div>
            </fieldset>
            <div class="table-responsive">
                <table class="display table table-striped table-bordered" id="table">
                    <thead>
                        <tr class="info">
                            <th class="text-center" width="5%">No.</th>
                            <th class="text-center" width="80%">Nama Admin</th>
                            <th class="text-center" width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data_jabatan as $row) {
                            ?> 
                            <tr>
                                <td class='center-x'><?php echo $no; ?></td>
                                <td><?php echo $row->user_name; ?></td>
                                <td class="text-center">
                                    <a class='btn btn-warning conbtn fancybox.ajax' data-toggle='tooltip' data-placement='top' title='Edit' href='<?php echo base_url($urlnya . '/edit/' . $row->user_name); ?>'><i class="fa fa-external-link"></i></a>
                                    <a class='btn btn-danger' data-toggle='tooltip' data-placement='top' title='Hapus' href='<?php echo base_url($urlnya . '/hapus/' . $row->uniq_kode); ?>'><i class="fa fa-list-ol"></i></a>
                                </td>

                                <?php
                                $no++;
                            }
                            ?>
                    </tbody>
                </table>
                <div id="pages">
                    <ul class="pagination">

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>