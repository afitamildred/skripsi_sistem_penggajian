<div class="col-lg-10">
    <div class="breadcrumb-wrapper">            
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('superadmin/dashboard'); ?>">Home</a></li>
            <li class="active">Data Divisi</li>
        </ol>
    </div>         
    <div class="box">
        <div class="box-title">
            <h2><?php echo $page_title; ?></h2>
        </div>
        <div class="clear"></div>
        <div class="box-inner" id="reload">            
            <fieldset>
                <div class="form-group">
                    <div class="col-md-10" style="padding: 0px !important;">
                        <input name="cari" type="text" class="form-control" placeholder="Cari data karyawan berdasarkan nama atau divisi . . .">
                    </div>
                    <div class="col-md-2 pull-right" style="padding: 0px !important; text-align: right">
                        <a class='btn btn-success conbtn fancybox.ajax' align="right" data-toggle='tooltip' data-placement='top' title='Cetak Data Divisi' href='<?php echo base_url($urlnya . '/cetak_data'); ?>'>Cetak Data Divisi</a>
                    </div>
                </div>
            </fieldset>
            <div class="table-responsive">
                <table class="display table table-striped table-bordered" id="table">
                    <thead>
                        <tr class="info">
                            <th class="text-center" width="5%">No.</th>
                            <th class="text-center" width="95%">Nama Divisi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data_divisi as $row) {
                            ?> 
                            <tr>
                                <td class='text-center'><?php echo $no; ?></td>
                                <td><?php echo $row->nama_divisi; ?></td>
                                <?php
                                $no++;
                            }
                            ?>
                    </tbody>
                </table>
                <div id="pages">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>