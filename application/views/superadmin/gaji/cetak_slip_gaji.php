<!-- Page Content -->
<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="body-kolom">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <a class="no-print" href="javascript:printDiv('print-area-2');" href="">
                                        <button type="button" class="btn btn-panjang btn-primary pull-right"><i class="fa fa-plus"></i> Cetak</button>
                                    </a>
                                    <a href="<?php echo base_url($urlnya . '/pdf_cetak_slip_gaji/' . $id); ?>">
                                        <button type="button" class="btn btn-panjang btn-warning pull-right download" style="margin-right: 10px"><i class="glyphicon glyphicon-export"></i>Export PDF</button>
                                    </a>
                                    <br>
                                    <script type="text/javascript">
                                        function printDiv(elementId) {
                                            var a = document.getElementById('printing-css').value;
                                            var b = document.getElementById(elementId).innerHTML;
                                            window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
                                            window.frames["print_frame"].window.focus();
                                            window.frames["print_frame"].window.print();
                                        }
                                    </script>
                                    <style type="text/css">
                                        body {
                                            padding:30px
                                        }
                                        .print-area{
                                            padding-top: 2em;
                                        }
                                        @media all {.page-break { display: block; page-break-before: always; size: potrait }}
                                        @page {size:  21.5cm 33cm; margin-left: 2cm; margin-bottom: 2cm; margin-top: 2cm; margin-right: 2cm}
                                        table { page-break-inside:auto }
                                        tr    { page-break-inside:avoid; page-break-after:auto }
                                        thead { display:table-header-group }
                                        tfoot { display:table-footer-group }
                                        tbody {
                                            display:table-row-group;
                                        }
                                        .txt-l {text-align: left;}
                                        .txt-c {text-align: center;}
                                        .txt-r {text-align: right;}
                                        .txt-j {text-align: justify;}
                                        .p1 {line-height: 150%;}
                                        .table-bordered td { 
                                            padding: 5px;
                                            font-size: 11px;
                                        }
                                        .table-bordered th,
                                        .table-bordered td {
                                            border: 1px solid #000 !important;
                                        }
                                        tbody td {
                                            border: 1px solid #000 !important;
                                        }
                                        .table {
                                            border-collapse: collapse !important;
                                        }
                                        .table td,
                                        .table th {
                                            background-color: #fff !important;
                                        }
                                    </style>
                                    <!-- Print area RAB -->
                                    <div id="print-area-2" class="print-area">
                                        <table class="table table-bordered" cellspacing="0" width="100%">
                                            <tr class="tahoma14">
                                                <td class="border-tlb border-tlb txt-c" width="25%"><img src="<?php echo base_url('assets'); ?>/images/logo.png" width="" height="40"></td>
                                                <td class="border-tlb txt-c" width="60%" colspan="7" align="center">
                                                    <h5>
                                                        <strong>SLIP GAJI
                                                        </strong>
                                                    </h5>
                                                <td class="border-tlbr txt-c" width="15%" align="center"><b><strong>Periode <?php echo $periode; ?></strong></td>
                                            </tr>
                                            <table id="example" class="display table table-striped table-bordered"  cellspacing="0" width="100%">
                                                <thead>
                                                    <tr class="tahoma12">
                                                        <td height="17" class="border-tlb" colspan="3">
                                                            <div align="center">
                                                                <strong>PENDAPATAN (+)</strong>
                                                            </div>
                                                        </td>
                                                        <td height="17" class="border-tlbr" colspan="3">
                                                            <div align="center">
                                                                <strong>PENGELUARAN (-)</strong>
                                                            </div
                                                        </td>
                                                    </tr><br />
                                                <tr align="center" class="tahoma12">
                                                    <td  class="border-lbr">No</td>
                                                    <td class="border-lbr">Keterangan</td>
                                                    <td class="border-lbr">Jumlah</td>
                                                    <td class="border-lbr">No</td>
                                                    <td class="border-lbr">Keterangan</td>
                                                    <td class="border-lbr">Jumlah</td>
                                                </tr><br />
                                                <?php
                                                $data_gapok = $this->gaji_m->view('golongan_gaji', array('id_divisi' => $data_gaji[0]->id_divisi, 'id_jabatan' => $data_gaji[0]->id_jabatan));
                                                $data_lembur = $this->gaji_m->view('lembur', array('id_karyawan' => $data_gaji[0]->id_karyawan, 'periode' => $periode));
                                                $data_bonus = $this->gaji_m->view('bonus', array('id_karyawan' => $data_gaji[0]->id_karyawan, 'periode' => $periode));
                                                $data_absensi = $this->gaji_m->view('absensi', array('id_karyawan' => $data_gaji[0]->id_karyawan, 'periode' => $periode));
                                                $data_pinjam = $this->gaji_m->view('pinjaman', array('id_karyawan' => $data_gaji[0]->id_karyawan));

                                                $total_pendapatan = @$data_gapok[0]->gaji_pokok + @$data_gaji[0]->tunjangan_proyek + @$data_bonus[0]->jumlah_bonus + @$data_lembur[0]->jumlah_bonus;
                                                $angsuran = ($total_pendapatan - $data_gaji[0]->gaji_bersih) - @$data_absensi[0]->potongan;
//                                                $total_potongan1 = @$data_absensi[0]->potongan + @$angsuran;
                                                $total_pengeluaran = @$angsuran + @$data_absensi[0]->potongan;
                                                $gaber = @$total_pendapatan - @$total_pengeluaran;
                                                ?>
                                                <tr class="tahoma12">
                                                    <td align="center" class="border-tlb">1</td>
                                                    <td class="border-tlb">Gaji Pokok</td>
                                                    <td class="border-tlb" align="right"><?php echo format_rupiah($data_gapok[0]->gaji_pokok); ?></td>
                                                    <td class="border-tlb" align="center">1</td>
                                                    <td class="border-tlb">Potongan Absensi</td>
                                                    <td class="border-tlbr" align="right"><?php echo format_rupiah(@$data_absensi[0]->potongan); ?></td>
                                                </tr>
                                                <tr class="tahoma12">
                                                    <td class="border-lb" align="center">2</td>
                                                    <td class="border-lb">Tunjangan Proyek</td>
                                                    <td class="border-lb" align="right"><?php echo format_rupiah(@$data_gaji[0]->tunjangan_proyek); ?></td>
                                                    <td class="border-lb" align="center">2</td>
                                                    <td class="border-lb">Potongan Pinjam</td>
                                                    <td class="border-lbr" align="right"><?php echo format_rupiah(@$angsuran); ?></td>
                                                </tr>
                                                <tr class="tahoma12">
                                                    <td class="border-lb" align="center">3</td>
                                                    <td class="border-lb">Lembur</td>
                                                    <td class="border-lb" align="right"><?php echo format_rupiah(@$data_lembur[0]->jumlah_bonus); ?></td>
                                                    <td class="border-lb"></td>
                                                    <td class="border-lb"></td>
                                                    <td class="border-lbr" align="right"></td>
                                                </tr>
                                                <tr class="tahoma12">
                                                    <td class="border-lb" align="center">4</td>
                                                    <td class="border-lb">Jumlah Bonus</td>
                                                    <td class="border-lb" align="right"><?php echo format_rupiah(@$data_bonus[0]->jumlah_bonus); ?></td>
                                                    <td class="border-lb"></td>
                                                    <td class="border-lb"></td>
                                                    <td class="border-lbr" align="right"></td>
                                                </tr>
                                                <tr class="tahoma12">
                                                    <td class="border-lb" colspan="2"><b>Total Pendapatan</b></td>
                                                    <td class="border-lbr" align="right"><b><?php echo format_rupiah(@$total_pendapatan); ?></b></td>

                                                    <td class="border-lb" colspan="2"><b>Total Pengeluaran</b></td>
                                                    <td class="border-lbr" align="right"><b><?php echo format_rupiah(@$total_pengeluaran); ?></b></td>
                                                </tr>
                                                <tr class="tahoma12">
                                                    <td class="border-lb" colspan="2"><b><i><u>Total Gaji yang diterima</u></i></b></td>
                                                    <td class="border-lbr" align="right"><b><?php echo format_rupiah(@$data_gaji[0]->gaji_bersih); ?></b></td>
                                                </tr>
                                                </thead>

                                            </table>
                                            <table id="example" class="display table table-striped table-bordered"  cellspacing="0" width="100%">
                                                <thead>
                                                    <tr align="center" class="tahoma12">
                                                        <td colspan="2"></td>
                                                        <td>Diserahkan oleh,</td>
                                                        <td>Diterima oleh, </td>
                                                    </tr><br />
                                                <tr class="tahoma12">
                                                    <td colspan="2">
                                                        NIK : <?php echo $data_gaji[0]->username; ?><br/>
                                                        Nama Karyawan : <?php echo $data_gaji[0]->nama_karyawan; ?><br/>
                                                        Divisi : <?php echo $data_gaji[0]->nama_divisi; ?><br/>
                                                        Jabatan : <?php echo $data_gaji[0]->nama_jabatan; ?><br/>
                                                    </td>
                                                    <td align="center"><br /><br /><br /><?php echo $this->session->userdata['username']; ?>
                                                    </td></td>
                                                    <td align="center"><br /><br /><br /><?php echo $data_gaji[0]->nama_karyawan; ?>
                                                    </td>
                                                </tr>
                                                </thead>
                                            </table>
                                            <textarea id="printing-css" style="display:none;">
                                                                     @media print {
                                                                     @page {size: 21.5cm 33cm; margin-left: 2cm; margin-bottom: 2cm; margin-top: 2cm; margin-right: 2cm }
                                                                     table { page-break-inside:auto; }
                                                                     tr    { page-break-inside:avoid; page-break-after:auto }
                                                                     thead { display:table-header-group }
                                                                     tfoot { display:table-footer-group }
                                                                     tbody {
                                                                     display:table-row-group;
                                                                 }
                                                                 td { 
                                                                 padding: 5px;
                                                             }
                                                             .table {
                                                             border-collapse: collapse !important;
                                                             font-size: 12px !important;
                                                         }
                                                         .table td,
                                                         .table th {
                                                         background-color: #fff !important;
                                                     }
                                                     .txt-l {text-align: left;}
                                                     .txt-c {text-align: center;}
                                                     .txt-r {text-align: right;}
                                                     .txt-j {text-align: justify;}
                                                     .tahoma10 {
                                                     font-size: 10px;
                                                     text-decoration: none;
                                                 }
                                                 .header20 {
                                                 font-size: 20px;
                                                 text-decoration: none;
                                                 padding: 10px;
                                                 margin: 10px;
                                             }
                                             .tahoma11 {
                                             font-size: 11px;
                                             text-decoration: none;
                                             margin: 2px;
                                             padding: 2px;
                                             border-top: 1px;
                                         }
.tahoma12 {
                                             font-size: 12px;
                                             text-decoration: none;
                                             margin: 2px;
                                             padding: 2px;
                                             border-top: 1px;
                                         }
.tahoma14 {
                                             font-size: 14px;
                                             text-decoration: none;
                                             margin: 2px;
                                             padding: 2px;
                                             border-top: 1px;
                                         }
                                         .tahoma16 {
                                         font-size: 16px;
                                         text-decoration: none;
                                     }
                                     .border-tlb {
                                     border-top-width: 1.2px !important;
                                     border-right-width: 1.2px !important;
                                     border-bottom-width: 1.2px !important;
                                     border-left-width: 1.2px !important;
                                     border-top-style: solid !important;
                                     border-right-style: none !important;
                                     border-bottom-style: solid !important;
                                     border-left-style: solid !important;
                                     border-top-color: #000000 !important;
                                     border-right-color: #000000 !important;
                                     border-bottom-color: #000000 !important;
                                     border-left-color: #000000 !important;
                                 }
                                 .border-tl {
                                 border-top-width: 1.2px !important;
                                 border-right-width: 1.2px !important;
                                 border-bottom-width: 1.2px !important;
                                 border-left-width: 1.2px !important;
                                 border-top-style: solid !important;
                                 border-right-style: none !important;
                                 border-bottom-style: none !important;
                                 border-left-style: solid !important;
                                 border-top-color: #000000 !important;
                                 border-right-color: #000000 !important;
                                 border-bottom-color: #000000 !important;
                                 border-left-color: #000000 !important;
                             }
                             .border-lb {
                             border-top-width: 1.2px !important;
                             border-right-width: 1.2px !important;
                             border-bottom-width: 1.2px !important;
                             border-left-width: 1.2px !important;
                             border-top-style: none !important;
                             border-right-style: none !important;
                             border-bottom-style: solid !important;
                             border-left-style: solid !important;
                             border-top-color: #000000 !important;
                             border-right-color: #000000 !important;
                             border-bottom-color: #000000 !important;
                             border-left-color: #000000 !important;
                         }
                         .border-lr {
                         border-top-width: 1.2px !important;
                         border-right-width: 1.2px !important;
                         border-bottom-width: 1.2px !important;
                         border-left-width: 1.2px !important;
                         border-top-style: none !important;
                         border-right-style: solid !important;
                         border-bottom-style: none !important;
                         border-left-style: solid !important;
                         border-top-color: #000000 !important;
                         border-right-color: #000000 !important;
                         border-bottom-color: #000000 !important;
                         border-left-color: #000000 !important;
                     }
                     .border-r {
                     border-top-width: 1.2px !important;
                     border-right-width: 1.2px !important;
                     border-bottom-width: 1.2px !important;
                     border-left-width: 1.2px !important;
                     border-top-style: none !important;
                     border-right-style: solid !important;
                     border-bottom-style: none !important;
                     border-left-style: none !important;
                     border-top-color: #000000 !important;
                     border-right-color: #000000 !important;
                     border-bottom-color: #000000 !important;
                     border-left-color: #000000 !important;
                 }
                 .border-l {
                 border-top-width: 1.2px !important;
                 border-right-width: 1.2px !important;
                 border-bottom-width: 1.2px !important;
                 border-left-width: 1.2px !important;
                 border-top-style: none !important;
                 border-right-style: none !important;
                 border-bottom-style: none !important;
                 border-left-style: solid !important;
                 border-top-color: #000000 !important;
                 border-right-color: #000000 !important;
                 border-bottom-color: #000000 !important;
                 border-left-color: #000000 !important;
             }
             .border-tlbr {
             border: 1.2px solid #000000 !important;
         }
         .border-lbr {
         border-top-width: 1.2px !important;
         border-right-width: 1.2px !important;
         border-bottom-width: 1.2px !important;
         border-left-width: 1.2px !important;
         border-top-style: none !important;
         border-right-style: solid !important;
         border-bottom-style: solid !important;
         border-left-style: solid !important;
         border-top-color: #000000 !important;
         border-right-color: #000000 !important;
         border-bottom-color: #000000 !important;
         border-left-color: #000000 !important;
     }
     .border-tb {
     border-top-width: 1.2px !important;
     border-right-width: 1.2px !important;
     border-bottom-width: 1.2px !important;
     border-left-width: 1.2px !important;
     border-top-style: solid !important;
     border-right-style: none !important;
     border-bottom-style: solid !important;
     border-left-style: none !important;
     border-top-color: #000000 !important;
     border-right-color: #000000 !important;
     border-bottom-color: #000000 !important;
     border-left-color: #000000 !important;
 }
 .border-tlb {
 border-top-width: 1.2px !important;
 border-right-width: 1.2px !important;
 border-bottom-width: 1.2px !important;
 border-left-width: 1.2px !important;
 border-top-style: solid !important;
 border-bottom-style: solid !important;
 border-left-style: solid !important;
 border-top-color: #000000 !important;
 border-right-color: #000000 !important;
 border-bottom-color: #000000 !important;
 border-left-color: #000000 !important;
}
.border-tbr {
border-top-width: 1.2px !important;
border-right-width: 1.2px !important;
border-bottom-width: 1.2px !important;
border-left-width: 1.2px !important;
border-top-style: solid !important;
border-right-style: solid !important;
border-bottom-style: solid !important;
border-top-color: #000000 !important;
border-right-color: #000000 !important;
border-bottom-color: #000000 !important;
border-left-color: #000000 !important;
}
.border-br {
border-right-width: 1.2px !important;
border-bottom-width: 1.2px !important;
border-right-style: solid !important;
border-bottom-style: solid !important;
border-right-color: #000000 !important;
border-bottom-color: #000000 !important;
}
.border-tr {
border-top-width: 1.2px !important;
border-right-width: 1.2px !important;
border-bottom-width: 1.2px !important;
border-left-width: 1.2px !important;
border-top-style: solid !important;
border-right-style: solid !important;
border-bottom-style: none !important;
border-left-style: none !important;
border-top-color: #000000 !important;
border-right-color: #000000 !important;
border-bottom-color: #000000 !important;
border-left-color: #000000 !important;
}
.border-b {
border-top-width: 1.2px !important;
border-right-width: 1.2px !important;
border-bottom-width: 1.2px !important;
border-left-width: 1.2px !important;
border-top-style: none !important;
border-right-style: none !important;
border-bottom-style: solid !important;
border-left-style: none !important;
border-top-color: #000000 !important;
border-right-color: #000000 !important;
border-bottom-color: #000000 !important;
border-left-color: #000000 !important;
}
.border-t {
border-top-width: 1.2px !important;
border-right-width: 1.2px !important;
border-bottom-width: 1.2px !important;
border-left-width: 1.2px !important;
border-top-style: solid !important;
border-right-style: none !important;
border-bottom-style: none !important;
border-left-style: none !important;
border-top-color: #000000 !important;
border-right-color: #000000 !important;
border-bottom-color: #000000 !important;
border-left-color: #000000 !important;
}
.txt-b {
font-weight: bold  !important;
}
.border-tlr {
border-top-width: 1.2px !important;
border-right-width: 1.2px !important;
border-bottom-width: 1.2px !important;
border-left-width: 1.2px !important;
border-top-style: solid !important;
border-right-style: solid !important;
border-bottom-style: none !important;
border-left-style: solid !important;
border-top-color: #000000 !important;
border-right-color: #000000 !important;
border-bottom-color: #000000 !important;
border-left-color: #000000 !important;
}
.border-missing {
border-top-width: 1.2px !important;
border-right-width: 1.2px !important;
border-bottom-width: 1.2px !important;
border-left-width: 1.2px !important;
border-top-style: none !important;
border-right-style: none !important;
border-bottom-style: none !important;
border-left-style: none !important;
border-top-color: #000000 !important;
border-right-color: #000000 !important;
border-bottom-color: #000000 !important;
border-left-color: #000000 !important;
}
}

                                            </textarea>
                                            <iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>
                                            <!-- Tutup setting for print file -->
                                    </div>
                                    <!-- Tutup Print area RAB -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
