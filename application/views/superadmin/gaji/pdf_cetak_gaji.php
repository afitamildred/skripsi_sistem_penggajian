<style type="text/css">
    @media all {.page-break { display: block; page-break-before: always; size: landscape }}
    @page {size: 33cm 21.5cm; margin-left: 2cm; margin-bottom: 2cm; margin-right: 2cm; margin-top: 2cm;}
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
    tbody {
        display:table-row-group;
    }
    .txt-l {text-align: left;}
    .txt-c {text-align: center;}
    .txt-r {text-align: right;}
    .txt-j {text-align: justify;}
    .p1 {line-height: 150%; font-size: 12px;}
    .table-bordered td { 
        padding: 5px;
        font-size: 12px;
    }
    .table {
        border-collapse: collapse !important;
    }
    .table td,
    .table th {
        background-color: #fff !important;
    }
    .tahoma10 {
        font-family: Tahoma;
        font-size: 10px;
        text-decoration: none;
    }
    .tahoma10 {
        font-family: Tahoma;
        font-size: 10px;
        text-decoration: none;
    }
    .header20 {
        font-family: Tahoma;
        font-size: 20px;
        text-decoration: none;
        padding: 10px;
        margin: 10px;
    }
    .tahoma11 {
        font-family: Tahoma;
        font-size: 11px;
        text-decoration: none;
        margin: 2px;
        padding: 2px;
        border-top: 1px;
    }
    .tahoma12 {
        font-family: Tahoma;
        font-size: 12px;
        text-decoration: none;
        margin: 2px;
        padding: 2px;
        border-top: 1px;
    }
    .tahoma14 {
        font-family: Tahoma;
        font-size: 14px;
        text-decoration: none;
        margin: 2px;
        padding: 2px;
        border-top: 1px;
    }
    .tahoma16 {
        font-family: Tahoma;
        font-size: 16px;
        text-decoration: none;
    }
    .border-tlb {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-right-style: none !important;
        border-bottom-style: solid !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-lb {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: none !important;
        border-bottom-style: solid !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-lr {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: solid !important;
        border-bottom-style: none !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-r {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: solid !important;
        border-bottom-style: none !important;
        border-left-style: none !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-l {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: none !important;
        border-bottom-style: none !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-tlbr {
        border: 1.2px solid #000000 !important;
    }
    .border-lbr {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: solid !important;
        border-bottom-style: solid !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-tb {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-right-style: none !important;
        border-bottom-style: solid !important;
        border-left-style: none !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-tlb {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-bottom-style: solid !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-tbr {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-right-style: solid !important;
        border-bottom-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-tl {
        border-top-width: 1.2px !important;
        border-left-width: 1.2px !important;

        border-top-style: solid !important;
        border-left-style: solid !important;

        border-top-color: #000000 !important;
        border-left-color: #000000 !important;

    }    
    .border-br {
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-right-style: solid !important;
        border-bottom-style: solid !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
    }
    .border-tr {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-right-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-b {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: none !important;
        border-bottom-style: solid !important;
        border-left-style: none !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-t {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-right-style: none !important;
        border-bottom-style: none !important;
        border-left-style: none !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .style1 {
        font-weight: bold  !important;
    }
    .border-right{
        border-right:1.2px solid #000000 !important;
    }
    .border-missing {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: none !important;
        border-bottom-style: solid !important;
        border-left-style: none !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
</style>
<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div id="print-area-2" class="print-area" style="max-width: 1400px; min-height: 305px">

                    <table class="table table-bordered" cellspacing="0" width="100%">
                        <tr class="tahoma14">
                            <td class="border-tlbr border-tlb txt-c" width="25%"><img src="<?php echo base_url('assets'); ?>/images/logo.png" width="" height="40"></td>
                            <td class="border-tlb txt-c" width="60%" colspan="7">
                                <strong>LAPORAN GAJI KARYAWAN<br>
                                    LPTI Pelataran Mataram Yogyakarta <br>
                                </strong>
                            </td>
                            <td class="border-tlbr txt-c" width="15%"><b><strong><?php echo $periode; ?></strong></td>
                        </tr>
                    </table><br>


                    <table class="table table-bordered" cellspacing="0" width="100%" style="margin-top: 20px !important">
                        <tr class="tahoma12">
                            <td height="17" class="border-tlb"><div align="center"><strong>No</strong></div></td>
                            <td height="17" class="border-tlb"><div align="center"><strong>Nama Karyawan </strong></div></td>
                            <td height="17" class="border-tlb"><div align="center"><strong>Divisi</strong></div></td>
                            <td height="17" class="border-tlb"><div align="center"><strong>Gaji Pokok </strong></div></td>
                            <td height="17" class="border-tlb"><div align="center"><strong>Jumlah Lembur </strong></div></td>
                            <td height="17" class="border-tlb"><div align="center"><strong>Tunjangan Proyek </strong></div></td>
                            <td height="17" class="border-tlb"><div align="center"><strong>Jumlah Bonus</strong></div></td>
                            <td height="17" class="border-tlb"><div align="center"><strong>Gaji Kotor </strong></div></td>
                            <td height="17" class="border-tlb"><div align="center"><strong>Potongan Pinjam</strong></div></td>
                            <td height="17" class="border-tlb"><div align="center"><strong>Potongan Absensi</strong></div></td>
                            <td height="17" class="border-tlb"><div align="center"><strong>Total Potongan </strong></div></td>
                            <td height="17" class="border-tlbr"><div align="center"><strong>Gaji Bersih</strong></div></td>

                        </tr>
                        <?php
                        $no = 1;
                        $total_bonus = 0;
                        $total_pinjaman = 0;
                        $total_potongan_absensi = 0;
                        $total_gapok = 0;
                        $total_tunj = 0;
                        $total_gaji = 0;
                        $total_gator = 0;
                        $total_potongan = 0;
                        $total_lembur = 0;

                        foreach ($data_gaji as $row) {
                            $data_gapok = $this->gaji_m->view('golongan_gaji', array('id_divisi' => $row->id_divisi, 'id_jabatan' => $row->id_jabatan));
//                                               //penambahan
                            $data_lembur = $this->gaji_m->view('lembur', array('id_karyawan' => $row->id_karyawan, 'periode' => $row->periode));
                            $data_bonus = $this->gaji_m->view('bonus', array('id_karyawan' => $row->id_karyawan, 'periode' => $row->periode));
                            $gaji_kotor = @$data_gapok[0]->gaji_pokok + @$data_lembur[0]->jumlah_bonus + @$row->tunjangan_proyek + @$data_bonus[0]->jumlah_bonus;
                            //pengurangan
                            $data_absensi = $this->gaji_m->view('absensi', array('id_karyawan' => $row->id_karyawan, 'periode' => $row->periode));
//                            $data_pinjaman = $this->gaji_m->view('pinjaman', array('id_karyawan' => $row->id_karyawan));
                            $angsuran = ($gaji_kotor - $row->gaji_bersih) - @$data_absensi[0]->potongan;
                            $total_potongan1 = @$data_absensi[0]->potongan + @$angsuran;
                            ?> 
                            <tr class="tahoma12">
                                <td class='center-x border-lb' align="center"><?php echo $no; ?></td>
                                <td class="border-lb"><?php echo $row->nama_karyawan; ?></td>
                                <td class="border-lb"> <?php echo $row->nama_divisi; ?></td>                                                    
                                <td class="border-lb" align="right"><?php echo format_rupiah(@$data_gapok[0]->gaji_pokok); ?></td>
                                <td class="border-lb" align="right"><?php echo format_rupiah(@$data_lembur[0]->jumlah_bonus); ?></td>
                                <td class="border-lb" align="right"><?php echo format_rupiah(@$row->tunjangan_proyek); ?></td>
                                <td class="border-lb" align="right"><?php echo format_rupiah(@$data_bonus[0]->jumlah_bonus); ?></td>
                                <td class="border-lb" align="right"><b><i><?php echo format_rupiah($gaji_kotor); ?></i></b></td>
                                <td class="border-lb" align="right"><?php echo format_rupiah(@$angsuran); ?></td>
                                <td class="border-lb" align="right"><?php echo format_rupiah(@$data_absensi[0]->potongan); ?></td>
                                <td class="border-lb" align="right"><b><i><?php echo format_rupiah($total_potongan1); ?></i></b></td>
                                <td class="border-lbr" align="right"><b><u><?php echo format_rupiah($row->gaji_bersih); ?></u></b></td>

                                <?php
                                $no++;
                                $total_bonus = $total_bonus + @$data_bonus[0]->jumlah_bonus;
                                $total_pinjaman = $total_pinjaman + @$angsuran;
                                $total_potongan_absensi = $total_potongan_absensi + @$data_absensi[0]->potongan;
                                $total_gapok = $total_gapok + $data_gapok[0]->gaji_pokok;
                                $total_tunj = $total_tunj + $row->tunjangan_proyek;
                                $total_gaji = $total_gaji + $row->gaji_bersih;
                                $total_lembur = $total_lembur + @$data_lembur[0]->jumlah_bonus;
                                $total_gator = $total_gator + $gaji_kotor;
                                $total_potongan = $total_potongan + @$total_potongan1;
                            }
                            ?>
                            </tbody>
                        <tr>
                            <td height="17" colspan="3" class="border-lb"><div align="center" class="tahoma11"><strong>Jumlah Keseluruhan</strong></div></td>

                            <td class="border-lb" align="right"><strong><u><?php echo format_rupiah($total_gapok); ?></u></strong></td>
                            <td class="border-lb" align="right"><strong><u><?php echo format_rupiah($total_lembur); ?></u></strong></td>
                            <td class="border-lb" align="right"><strong><u><?php echo format_rupiah($total_tunj); ?></u></strong></td>
                            <td class="border-lb" align="right"><strong><u><?php echo format_rupiah($total_bonus); ?></u></strong></td>
                            <td class="border-lb" align="right"><strong><u><?php echo format_rupiah($total_gator); ?></u></strong></td>
                            <td class="border-lb" align="right"><strong><u><?php echo format_rupiah($total_pinjaman); ?></u></strong></td>
                            <td class="border-lb" align="right"><strong><u><?php echo format_rupiah($total_potongan_absensi); ?></u></strong></td>
                            <td class="border-lb" align="right"><strong><u><?php echo format_rupiah($total_potongan); ?></u></strong></td>
                            <td class="border-lbr" align="right"><strong><u><?php echo format_rupiah($total_gaji); ?></u></strong></td>
                        </tr><br/>
                        <strong>  <?php
                            echo '<i>Dicetak oleh <b>' . $this->session->userdata['username'] . '</b> pada tanggal <b>' . date('Y-m-d') . '</b></i>';
                            ?>
                        </strong>
                </div>
                </td>
                </tr><br/>
                </table>
            </div>
        </div>
    </div>
</div>
</div>