<div class="col-lg-10">
    <div class="breadcrumb-wrapper">            
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('superadmin/dashboard'); ?>">Home</a></li>
            <li class="active">Data Absensi Karyawan</li>
        </ol>
    </div>         
    <div class="box">
        <div class="box-title">
            <h2><?php echo $page_title; ?></h2>

        </div>

        <div class="clear"></div>
        <div class="box-inner" id="reload">

            <fieldset>
                <div class="form-group">
                    <div class="col-md-10" style="padding: 0px !important;">
                        <input name="cari" type="text" class="form-control" placeholder="Cari data karyawan berdasarkan nama atau divisi . . .">
                    </div>
                    <div class="col-md-2 pull-right" style="padding: 0px !important; text-align: right">
                        <a class='btn btn-success conbtn fancybox.ajax' align="right" data-toggle='tooltip' data-placement='top' title='Cetak Data Tunjangan Proyek' href='<?php echo base_url($urlnya . '/cetak_data'); ?>'>Cetak Data</a>
                    </div>
                </div>
            </fieldset>
            <div class="table-responsive">
                <table class="display table table-striped table-bordered" id="table">
                    <thead>
                        <tr class="info">
                            <th class="text-center" width="5%">No.</th>
                            <th class="text-center" width="65%">Nama Proyek</th>
                            <th class="text-center" width="30%">Jumlah Tunjangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data_tunjangan_proyek as $row) {
                            ?> 
                            <tr>
                                <td class='text-center'><?php echo $no; ?></td>
                                <td><?php echo $row->nama_proyek; ?></td>
                                <td align="right"><?php echo format_rupiah($row->tunjangan_proyek); ?></td>
                                <?php
                                $no++;
                            }
                            ?>
                    </tbody>
                </table>
                <div id="pages">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>