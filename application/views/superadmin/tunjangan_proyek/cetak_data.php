<!-- Page Content -->
<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="page-header">
                <div class="breadcrumb-wrapper">
                </div>
            </div>
            <div class="col-md-12">
                <div class="box">
                    <div class="judul-kolom">
                        <h4></h4><div class="clearfix"></div>
                    </div>
                    <div class="body-kolom">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <a class="no-print" href="javascript:printDiv('print-area-2');" href="">
                                        <button type="button" class="btn btn-panjang btn-primary pull-right"><i class="fa fa-plus"></i> Cetak</button>
                                    </a>
                                    <a href="<?php echo base_url($urlnya . '/pdf_cetak/'); ?>">
                                        <button type="button" class="btn btn-panjang btn-warning pull-right download" style="margin-right: 10px"><i class="glyphicon glyphicon-export"></i>Export PDF</button>
                                    </a>
                                    <br>
                                    <script type="text/javascript">
                                        function printDiv(elementId) {
                                            var a = document.getElementById('printing-css').value;
                                            var b = document.getElementById(elementId).innerHTML;
                                            window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
                                            window.frames["print_frame"].window.focus();
                                            window.frames["print_frame"].window.print();
                                        }
                                    </script>
                                    <!-- Tutup Script Button Print (Reza) -->
                                    <style type="text/css">
                                        body {
                                            padding:30px
                                        }
                                        .print-area{
                                            padding-top: 2em;
                                        }
                                        @media all {.page-break { display: block; page-break-before: always; size: potrait }}
                                        @page {size: 21.5cm 33cm;  margin-left: 2cm; margin-bottom: 2cm; margin-top: 2cm; margin-right: 2cm}
                                        table { page-break-inside:auto }
                                        tr    { page-break-inside:avoid; page-break-after:auto }
                                        thead { display:table-header-group }
                                        tfoot { display:table-footer-group }
                                        tbody {
                                            display:table-row-group;
                                        }
                                        .txt-l {text-align: left;}
                                        .txt-c {text-align: center;}
                                        .txt-r {text-align: right;}
                                        .txt-j {text-align: justify;}
                                        .p1 {line-height: 150%;}
                                        .table-bordered td { 
                                            padding: 5px;
                                            font-size: 11px;
                                        }
                                        .table-bordered th,
                                        .table-bordered td {
                                            border: 1px solid #000 !important;
                                        }
                                        tbody td {
                                            border: 1px solid #000 !important;
                                        }
                                        .table {
                                            border-collapse: collapse !important;
                                        }
                                        .table td,
                                        .table th {
                                            background-color: #fff !important;
                                        }
                                    </style>
                                    <!-- Print area RAB -->
                                    <div id="print-area-2" class="print-area">
                                        <table class="table table-bordered" cellspacing="0" width="100%">
                                            <tr class="tahoma14">
                                                <td class="border-tlb border-tlb txt-c" width="25%"><img src="<?php echo base_url('assets'); ?>/images/logo.png" width="" height="40"></td>
                                                <td class="border-tlbr txt-c" width="60%" colspan="7" align="center">
                                                    <strong>LAPORAN DATA TUNJANGAN PROYEK<br>
                                                    </strong>
                                                </td>
                                            </tr>
                                        </table><br>
                                        <table id="example" class="display table table-striped table-bordered"  cellspacing="0" width="100%">
                                            <thead>
                            <tr class="tahoma12">
                                <td height="17" class="border-tlb"  width="5%">
                                    <div align="center">
                                        <strong>No</strong>
                                    </div>
                                </td>
                                <td height="17" class="border-tlbr"  width="70%">
                                    <div align="center">
                                        <strong>Nama Proyek </strong>
                                    </div>
                                </td>
                                <td height="17" class="border-tlbr"  width="25%">
                                    <div align="center">
                                        <strong>Tunjangan Proyek </strong>
                                    </div>
                                </td>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;

                            foreach ($data_order as $row) {
                                ?> 
                                <tr>
                                    <td class='center-x border-lb' align="center"><?php echo $no; ?></td>
                                    <td class="border-lbr"><?php echo $row->nama_proyek; ?></td>
                                    <td class="border-lbr" align="right"><?php echo format_rupiah($row->tunjangan_proyek); ?></td>

                                    <?php
                                    $no++;
                                }
                                ?>
                        </tbody>
                                            <tr>
                                            </tr>
                                        </table>
                                        <!-- Setting for print file -->
                                        <textarea id="printing-css" style="display:none;">
                                                                     @media print {
                                                                     @page {size: 33cm 21.5cm; margin-left: 0.5cm; margin-bottom: 1.5cm; margin-top: 2cm; margin-right: 3cm }
                                                                     table { page-break-inside:auto; }
                                                                     tr    { page-break-inside:avoid; page-break-after:auto }
                                                                     thead { display:table-header-group }
                                                                     tfoot { display:table-footer-group }
                                                                     tbody {
                                                                     display:table-row-group;
                                                                 }
                                                                 td { 
                                                                 padding: 5px;
                                                             }
                                                             .table {
                                                             border-collapse: collapse !important;
                                                             font-size: 12px !important;
                                                         }
                                                         .table td,
                                                         .table th {
                                                         background-color: #fff !important;
                                                     }
                                                     .txt-l {text-align: left;}
                                                     .txt-c {text-align: center;}
                                                     .txt-r {text-align: right;}
                                                     .txt-j {text-align: justify;}
                                                     .tahoma10 {
                                                     font-size: 10px;
                                                     text-decoration: none;
                                                 }
                                                 .header20 {
                                                 font-size: 20px;
                                                 text-decoration: none;
                                                 padding: 10px;
                                                 margin: 10px;
                                             }
                                             .tahoma11 {
                                             font-size: 11px;
                                             text-decoration: none;
                                             margin: 2px;
                                             padding: 2px;
                                             border-top: 1px;
                                         }
                                            .tahoma12 {
                                             font-size: 12px;
                                             text-decoration: none;
                                             margin: 2px;
                                             padding: 2px;
                                             border-top: 1px;
                                         }
.tahoma14 {
                                             font-size: 14px;
                                             text-decoration: none;
                                             margin: 2px;
                                             padding: 2px;
                                             border-top: 1px;
                                         }
                                         .tahoma16 {
                                         font-size: 16px;
                                         text-decoration: none;
                                     }
                                     .border-tlb {
                                     border-top-width: 1.2px !important;
                                     border-right-width: 1.2px !important;
                                     border-bottom-width: 1.2px !important;
                                     border-left-width: 1.2px !important;
                                     border-top-style: solid !important;
                                     border-right-style: none !important;
                                     border-bottom-style: solid !important;
                                     border-left-style: solid !important;
                                     border-top-color: #000000 !important;
                                     border-right-color: #000000 !important;
                                     border-bottom-color: #000000 !important;
                                     border-left-color: #000000 !important;
                                 }
                                 .border-tl {
                                 border-top-width: 1.2px !important;
                                 border-right-width: 1.2px !important;
                                 border-bottom-width: 1.2px !important;
                                 border-left-width: 1.2px !important;
                                 border-top-style: solid !important;
                                 border-right-style: none !important;
                                 border-bottom-style: none !important;
                                 border-left-style: solid !important;
                                 border-top-color: #000000 !important;
                                 border-right-color: #000000 !important;
                                 border-bottom-color: #000000 !important;
                                 border-left-color: #000000 !important;
                             }
                             .border-lb {
                             border-top-width: 1.2px !important;
                             border-right-width: 1.2px !important;
                             border-bottom-width: 1.2px !important;
                             border-left-width: 1.2px !important;
                             border-top-style: none !important;
                             border-right-style: none !important;
                             border-bottom-style: solid !important;
                             border-left-style: solid !important;
                             border-top-color: #000000 !important;
                             border-right-color: #000000 !important;
                             border-bottom-color: #000000 !important;
                             border-left-color: #000000 !important;
                         }
                         .border-lr {
                         border-top-width: 1.2px !important;
                         border-right-width: 1.2px !important;
                         border-bottom-width: 1.2px !important;
                         border-left-width: 1.2px !important;
                         border-top-style: none !important;
                         border-right-style: solid !important;
                         border-bottom-style: none !important;
                         border-left-style: solid !important;
                         border-top-color: #000000 !important;
                         border-right-color: #000000 !important;
                         border-bottom-color: #000000 !important;
                         border-left-color: #000000 !important;
                     }
                     .border-r {
                     border-top-width: 1.2px !important;
                     border-right-width: 1.2px !important;
                     border-bottom-width: 1.2px !important;
                     border-left-width: 1.2px !important;
                     border-top-style: none !important;
                     border-right-style: solid !important;
                     border-bottom-style: none !important;
                     border-left-style: none !important;
                     border-top-color: #000000 !important;
                     border-right-color: #000000 !important;
                     border-bottom-color: #000000 !important;
                     border-left-color: #000000 !important;
                 }
                 .border-l {
                 border-top-width: 1.2px !important;
                 border-right-width: 1.2px !important;
                 border-bottom-width: 1.2px !important;
                 border-left-width: 1.2px !important;
                 border-top-style: none !important;
                 border-right-style: none !important;
                 border-bottom-style: none !important;
                 border-left-style: solid !important;
                 border-top-color: #000000 !important;
                 border-right-color: #000000 !important;
                 border-bottom-color: #000000 !important;
                 border-left-color: #000000 !important;
             }
             .border-tlbr {
             border: 1.2px solid #000000 !important;
         }
         .border-lbr {
         border-top-width: 1.2px !important;
         border-right-width: 1.2px !important;
         border-bottom-width: 1.2px !important;
         border-left-width: 1.2px !important;
         border-top-style: none !important;
         border-right-style: solid !important;
         border-bottom-style: solid !important;
         border-left-style: solid !important;
         border-top-color: #000000 !important;
         border-right-color: #000000 !important;
         border-bottom-color: #000000 !important;
         border-left-color: #000000 !important;
     }
     .border-tb {
     border-top-width: 1.2px !important;
     border-right-width: 1.2px !important;
     border-bottom-width: 1.2px !important;
     border-left-width: 1.2px !important;
     border-top-style: solid !important;
     border-right-style: none !important;
     border-bottom-style: solid !important;
     border-left-style: none !important;
     border-top-color: #000000 !important;
     border-right-color: #000000 !important;
     border-bottom-color: #000000 !important;
     border-left-color: #000000 !important;
 }
 .border-tlb {
 border-top-width: 1.2px !important;
 border-right-width: 1.2px !important;
 border-bottom-width: 1.2px !important;
 border-left-width: 1.2px !important;
 border-top-style: solid !important;
 border-bottom-style: solid !important;
 border-left-style: solid !important;
 border-top-color: #000000 !important;
 border-right-color: #000000 !important;
 border-bottom-color: #000000 !important;
 border-left-color: #000000 !important;
}
.border-tbr {
border-top-width: 1.2px !important;
border-right-width: 1.2px !important;
border-bottom-width: 1.2px !important;
border-left-width: 1.2px !important;
border-top-style: solid !important;
border-right-style: solid !important;
border-bottom-style: solid !important;
border-top-color: #000000 !important;
border-right-color: #000000 !important;
border-bottom-color: #000000 !important;
border-left-color: #000000 !important;
}
.border-br {
border-right-width: 1.2px !important;
border-bottom-width: 1.2px !important;
border-right-style: solid !important;
border-bottom-style: solid !important;
border-right-color: #000000 !important;
border-bottom-color: #000000 !important;
}
.border-tr {
border-top-width: 1.2px !important;
border-right-width: 1.2px !important;
border-bottom-width: 1.2px !important;
border-left-width: 1.2px !important;
border-top-style: solid !important;
border-right-style: solid !important;
border-bottom-style: none !important;
border-left-style: none !important;
border-top-color: #000000 !important;
border-right-color: #000000 !important;
border-bottom-color: #000000 !important;
border-left-color: #000000 !important;
}
.border-b {
border-top-width: 1.2px !important;
border-right-width: 1.2px !important;
border-bottom-width: 1.2px !important;
border-left-width: 1.2px !important;
border-top-style: none !important;
border-right-style: none !important;
border-bottom-style: solid !important;
border-left-style: none !important;
border-top-color: #000000 !important;
border-right-color: #000000 !important;
border-bottom-color: #000000 !important;
border-left-color: #000000 !important;
}
.border-t {
border-top-width: 1.2px !important;
border-right-width: 1.2px !important;
border-bottom-width: 1.2px !important;
border-left-width: 1.2px !important;
border-top-style: solid !important;
border-right-style: none !important;
border-bottom-style: none !important;
border-left-style: none !important;
border-top-color: #000000 !important;
border-right-color: #000000 !important;
border-bottom-color: #000000 !important;
border-left-color: #000000 !important;
}
.txt-b {
font-weight: bold  !important;
}
.border-tlr {
border-top-width: 1.2px !important;
border-right-width: 1.2px !important;
border-bottom-width: 1.2px !important;
border-left-width: 1.2px !important;
border-top-style: solid !important;
border-right-style: solid !important;
border-bottom-style: none !important;
border-left-style: solid !important;
border-top-color: #000000 !important;
border-right-color: #000000 !important;
border-bottom-color: #000000 !important;
border-left-color: #000000 !important;
}
.border-missing {
border-top-width: 1.2px !important;
border-right-width: 1.2px !important;
border-bottom-width: 1.2px !important;
border-left-width: 1.2px !important;
border-top-style: none !important;
border-right-style: none !important;
border-bottom-style: none !important;
border-left-style: none !important;
border-top-color: #000000 !important;
border-right-color: #000000 !important;
border-bottom-color: #000000 !important;
border-left-color: #000000 !important;
}
}

                                        </textarea>
                                        <iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
