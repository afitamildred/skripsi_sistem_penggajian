<script src="<?php echo base_url('assets/js/jquery-1.7.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/autoNumeric.js'); ?>"></script>
<script>
    function convertToRupiah(angka) {
        var rupiah = '';
        var angkarev = angka.toString().split('').reverse().join('');
        for (var i = 0; i < angkarev.length; i++)
            if (i % 3 === 0)
                rupiah += angkarev.substr(i, 3) + '.';
        return rupiah.split('', rupiah.length - 1).reverse().join('');
    }
    $(document).ready(function() {
        $("#username").change(function() {
            var username = $("#username").val();
            if (username == '') {
                $("#nama").val('');
                $("#divisi").val('');
                $("#jabatan").val('');
                $("#gapok_tunj").val(0);
                $("#gapok_unNum").val(0);
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url($urlnya); ?>/get_data_karyawan",
                    data: "username=" + username,
                    success: function(data) {
//            alert(data);
                        var pecah = data.split(',');
                        var nama_karyawan = pecah[0];
                        var divisi = pecah[1];
                        var jabatan = pecah[2];
                        var lembur_perjam = pecah[3];
                        $("#nama").val(nama_karyawan);
                        $("#divisi").val(divisi);
                        $("#jabatan").val(jabatan);
                        $("#jml").val(convertToRupiah(lembur_perjam));
                        $("#jml_unNum").val(lembur_perjam);
                    }
                });
            }
        });
        $("#form_validate").validate({
            rules: {
                "jumlah_jam": {
                    required: true
                },
                "tanggal_lembur": {
                    required: true
                },
                "username": {
                    required: true
                }
            }
        });
        $("#golongan_lembur").change(function() {
            var value = $(this).val();
            var pecah = value.split('_');
            var id = pecah[0];
            var jml = pecah[1];
            var divisi = pecah[1];
            $("#id_golongan_lembur").val(id);
            $("#jml").val(jml);
        });

        $('#jumlah_jam').keyup(function() {
            var jumlah_jam = $(this).val();
            var jumlah = $("#jml_unNum").val();
            var kali = jumlah_jam * jumlah;
            $("#jumlah_bonus").val(convertToRupiah(kali));
            $("#jumlah_bonus_unNum").val(kali);

        });
    });
</script>
<div style="min-width:600px;">

    <div class="box-title">
        <h3>Tambah Data Lembur</h3> 
    </div>
    <div class="box-inner form-horizontal">
        <div id="url" class="hidden" data-alamat="<?php echo base_url($urlnya . '/get_data/') ?>"></div>
        <?php echo form_open($urlnya . '/simpan', array('id' => 'form_validate', 'data-reload' => base_url($urlnya . '/index?data=reload'))); ?>
        <input type="hidden" name="targetData" value="true">  
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Periode </label>
                <div class="col-md-9">
                    <select name="periode" id="periode" class="form-control">
                        <?php
                        $i = 1;
                        foreach ($bul as $row) {
                            if ($bulan == $i) {
                                ?>
                                <option value="<?php echo $row ?>" selected=""><?php echo $row; ?></option>
                            <?php } else {
                                ?>
                                <option value="<?php echo $row ?>"><?php echo $row; ?></option>

                                <?php
                            }$i++;
                        }
                        ?>
                    </select>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">   
                    <select name="username" id="username" class="form-control m-b">
                        <option value="">--Select Username Karyawan--</option>
                        <?php foreach ($username_karyawan as $row) { ?>
                            <option value="<?php echo $row->username ?>"><?php echo $row->username . '  ||  ' . $row->nama ?></option>
                        <?php } ?>
                    </select>
                    <?php echo form_error('username'); ?>  
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Nama</label>
                <div class="col-md-9">
                    <?php echo form_input($nama); ?>
                </div>
            </div>
        </fieldset>    
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Divisi</label>
                <div class="col-md-9">                          
                    <?php echo form_input($divisi); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jabatan</label>
                <div class="col-md-9">                          
                    <?php echo form_input($jabatan); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Tanggal Lembur</label>
                <div class="col-md-9">
                    <?php echo form_input($tanggal_lembur); ?>         
                    <?php echo form_error('tanggal_lembur'); ?>  
                </div>
            </div>
        </fieldset>  
        <input type="hidden" name="id_golongan_lembur" value="" id="id_golongan_lembur"  class="form-control">
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jumlah/Jam</label>
                <div class="col-md-9">
                    <input type="text" name="jml" value="" id="jml"  class="form-control" readonly="">
                    <input type="hidden" name="jml_unNum" value="" id="jml_unNum" >
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jumlah Jam</label>
                <div class="col-md-9">
                    <?php echo form_input($jumlah_jam); ?>            
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jumlah Bonus Lembur</label>
                <div class="col-md-9">
                    <?php echo form_input($jumlah_bonus); ?>       
                    <input type="hidden" name="jumlah_bonus_unNum" value="" id="jumlah_bonus_unNum" >
                </div>
            </div>
        </fieldset>

        <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-10">
                    <?php echo form_submit($submitin); ?>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
</div>

<?php
if (isset($scripts)) {
    foreach ($scripts as $script) {
        echo $script;
    }
}
?>
<script>
    jQuery(function($) {
        var anc = {};
        $('.datetimepicker').datetimepicker({
            lang: 'id',
            timepicker: false,
            format: 'Y-m-d'
        });
    });
</script>