<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/autoNumeric.js'); ?>"></script>
<script>
    $(document).ready(function() {
        function convertToRupiah(angka) {
            var rupiah = '';
            var angkarev = angka.toString().split('').reverse().join('');
            for (var i = 0; i < angkarev.length; i++)
                if (i % 3 === 0)
                    rupiah += angkarev.substr(i, 3) + '.';
            return rupiah.split('', rupiah.length - 1).reverse().join('');
        }
        $("#form_validate").validate({
            rules: {
                "jumlah_jam": {
                    required: true
                }
            }
        });
        $('#jumlah_jam').keyup(function() {
            var jumlah_jam = parseInt($(this).val());
            var jumlah = parseInt($("#jml_unNum").val());
            var kali = jumlah_jam * jumlah;
            $("#jumlah_bonus").val(convertToRupiah(kali));
            $("#jumlah_bonus_unNum").val(kali);
        });
        jQuery(function($) {
            $('#jml').autoNumeric('init', {aSep: '.', aDec: ','});
        });
        jQuery(function($) {
            $('#jumlah_bonus').autoNumeric('init', {aSep: '.', aDec: ','});
        });
    });
</script>
<div style="min-width:600px;">
    <div class="box-title">
        <h3>Edit Data Lembur</h3> 
    </div>
    <div class="box-inner form-horizontal">
        <div id="url" class="hidden" data-alamat="<?php echo base_url($urlnya . '/get_data/') ?>"></div>
        <?php echo form_open($urlnya . '/simpan_edit/' . $usernamenya, array('id' => 'form-tambah', 'data-reload' => base_url($urlnya . '/index?data=reload'))); ?>
        <input type="hidden" name="targetData" value="true">  
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Periode </label>
                <div class="col-md-9">
                    <select name="periode" id="periode" class="form-control">
                        <?php
                        foreach ($bul as $row) {
                            if ($row == $periode) {
                                ?>
                                <option value="<?php echo $row ?>" selected=""><?php echo $row; ?></option>
                            <?php } else {
                                ?>
                                <option value="<?php echo $row ?>"><?php echo $row; ?></option>

                                <?php
                            }$i++;
                        }
                        ?>
                    </select>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">   
                    <?php echo form_input($id); ?>
                    <?php echo form_input($username); ?>
                    <div class="error_popups" id="error-username"></div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Nama</label>
                <div class="col-md-9">
                    <?php echo form_input($nama); ?>
                </div>
            </div>
        </fieldset>                    
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Divisi</label>
                <div class="col-md-9">                          
                    <?php echo form_input($divisi); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jabatan</label>
                <div class="col-md-9">                          
                    <?php echo form_input($jabatan); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Tanggal Lembur</label>
                <div class="col-md-9">
                    <?php echo form_input($tanggal_lembur); ?>                                    
                    <div class="error_popups" id="error-no-sk"></div>
                </div>
            </div>
        </fieldset> 
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jumlah/jam</label>
                <div class="col-md-9">
                    <?php echo form_input($jml); ?>                   
                    <input type="hidden" name="jml_unNum" value="<?php echo $lembur_perjam; ?>" id="jml_unNum" >
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jumlah jam</label>
                <div class="col-md-9">
                    <?php echo form_input($jumlah_jam); ?>                                    
                    <div class="error_popups" id="error-no-sk"></div>
                </div>
            </div>
        </fieldset> 
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jumlah Bonus</label>
                <div class="col-md-9">
                    <?php echo form_input($jumlah_bonus); ?>   
                    <input type="hidden" name="jumlah_bonus_unNum" value="<?php echo $jml_bonus; ?>" id="jumlah_bonus_unNum" >
                </div>
            </div>
        </fieldset>

        <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-10">
                    <?php echo form_submit($submitin); ?>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
</div>
<?php
if (isset($scripts)) {
    foreach ($scripts as $script) {
        echo $script;
    }
}
?>
<script>
    jQuery(function($) {
        var anc = {};
        $('.datetimepicker').datetimepicker({
            lang: 'id',
            timepicker: false,
            format: 'Y-m-d'
        });
    });
</script>