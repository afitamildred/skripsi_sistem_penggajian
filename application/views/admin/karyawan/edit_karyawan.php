<script src="<?php echo base_url('assets/js/jquery-1.7.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/autoNumeric.js'); ?>"></script>

<?php
if (isset($scripts)) {
    foreach ($scripts as $script) {
        echo $script;
    }
}
?>
<script>
    function convertToRupiah(angka) {
        var rupiah = '';
        var angkarev = angka.toString().split('').reverse().join('');
        for (var i = 0; i < angkarev.length; i++)
            if (i % 3 === 0)
                rupiah += angkarev.substr(i, 3) + '.';
        return rupiah.split('', rupiah.length - 1).reverse().join('');
    }
    $(document).ready(function() {
        jQuery(function($) {
            var anc = {};
            $('.datetimepicker').datetimepicker({
                lang: 'id',
                timepicker: false,
                format: 'Y-m-d'
            });
        });
        $("#form_validate").validate({
            rules: {
                "nama": {
                    required: true
                },
                "no_ktp": {
                    required: true
                }
            }
        });
        $("#jabatan").change(function() {
            var id_divisi = $("#divisi").val();
            var id_jabatan = $('#jabatan').val();
            var status_karyawan = $('#status_karyawan').val();
            var sijab = id_divisi + '_' + id_jabatan + '_' + status_karyawan;
            $.ajax({
                type: "POST",
                url: "<?php echo base_url($urlnya); ?>/get_gapok",
                data: "sijab=" + sijab,
                success: function(data) {
                    if (data == 0) {
                        alert('Divisi dan Jabatan belum terdaftar di Golongan Gaji, Mohon inputkan data terlebih dahulu!')
                    } else {
//                        $("#gaji_pokok").val(data);
                        $("#gaji_pokok").val(convertToRupiah(data));
                    }
                }
            });
        });
        $("#id_proyek").change(function() {
            var id_proyek = $("#id_proyek").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url($urlnya); ?>/get_tunjangan_proyek",
                data: "id_proyek=" + id_proyek,
                success: function(data) {
                    if (data == 0) {
                        alert('Divisi dan Jabatan belum terdaftar di Golongan Gaji, Mohon inputkan data terlebih dahulu!')
                    } else {
                        $("#tunjangan_proyek").val(data);
                        $("#tunjangan_proyek").val(convertToRupiah(data));
                    }
                }
            });
        });
        //apabila nulll pada select
        $("#divisi").on('change', function() {
            var divisi = $("#divisi").val();
            if (divisi == '') {
                alert('Mohon pilih divisi yang benar!');
            } else {
                $('#gaji_pokok').val(0);
            }
        });

        $("#jabatan").on('change', function() {
            var jabatan = $("#divisi").val();
            if (jabatan == '') {
                alert('Mohon pilih jabatan yang benar!');
            } else {
                $('#gaji_pokok').val(0);
            }
        });
        $("#status_karyawan").on('change', function() {
            var status_karyawan = $("#status_karyawan").val();
            if (status_karyawan == '') {
                alert('Mohon pilih status karyawan yang benar!');
                $('#gaji_pokok').val(0);
            } else {
                $('#gaji_pokok').val(0);
            }
        });
        
        //autonumeric
        jQuery(function($) {
            $('#gaji_pokok').autoNumeric('init', {aSep: '.', aDec: ','});
        });
        jQuery(function($) {
            $('#tunjangan_proyek').autoNumeric('init', {aSep: '.', aDec: ','});
        });
    });
</script>
<div style="min-width:650px;">

    <div class="box-title">
        <h3>Edit Data Karyawan</h3> 
    </div>
    <div class="box-inner form-horizontal">
        <div id="url" class="hidden" data-alamat="<?php echo base_url($urlnya . '/get_data/') ?>"></div>
        <?php echo form_open($urlnya . '/simpan_edit', array('id' => 'form-tambah', 'data-reload' => base_url($urlnya . '/index?data=reload'))); ?>
        <input type="hidden" name="targetData" value="true">  
        <?php echo form_input($id); ?>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">
                    <?php echo form_input($username); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Nama</label>
                <div class="col-md-9">
                    <?php echo form_input($nama); ?>    
                    <?php echo form_error('nama'); ?>  
                </div>
            </div>
        </fieldset>  
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Status Karyawan</label>
                <div class="col-md-9">                       
                    <?php echo form_input($id); ?>
                    <select name="status_karyawan" id="status_karyawan" class="form-control m-b">
                        <option value="">--Pilih Status Karyawan--</option>
                        <option value="Kontrak" 
                        <?php
                        if ($status_karyawan == 'Kontrak') {
                            echo 'selected';
                        };
                        ?>>Kontrak</option>
                        <option value="Tetap"
                        <?php
                        if ($status_karyawan == 'Tetap') {
                            echo 'selected';
                        };
                        ?>>Tetap</option>

                    </select>
                </div>
            </div>
        </fieldset>                 
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Divisi</label>
                <div class="col-md-9">                      
                    <select name="divisi" id="divisi" class="form-control m-b">
                        <option value="">--Pilih Divisi--</option>
                        <?php foreach ($data_divisi as $row) { ?>
                            <option value="<?php echo $row->id; ?>" 
                            <?php
                            if ($row->id == $data_karyawan[0]->id_divisi) {
                                echo 'selected';
                            };
                            ?>> <?php echo $row->nama_divisi ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jabatan</label>
                <div class="col-md-9">                      
                    <select name="jabatan" id="jabatan" class="form-control m-b">
                        <option value="">--Pilih Jabatan--</option>
                        <?php foreach ($data_jabatan as $row) { ?>
                            <option value="<?php echo $row->id; ?>" 
                            <?php
                            if ($row->id == $data_karyawan[0]->id_jabatan) {
                                echo 'selected';
                            };
                            ?>> <?php echo $row->nama_jabatan ?>
                            </option>
                        <?php } ?>
                    </select>   
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Gaji Pokok</label>
                <div class="col-md-9">                          
                    <?php echo form_input($gaji_pokok); ?>     
                </div>
            </div>
        </fieldset>

        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Nama Proyek</label>
                <div class="col-md-9">                      
                    <select name="id_proyek" id="id_proyek" class="form-control m-b">
                        <option value="">--Pilih Nama Proyek--</option>
                        <?php foreach ($data_proyek as $row) { ?>
                            <option value="<?php echo $row->id; ?>" 
                            <?php
                            if ($row->id == $data_proyek[0]->id) {
                                echo 'selected';
                            };
                            ?>> <?php echo $row->nama_proyek ?>
                            </option>
                        <?php } ?>
                    </select>   
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Tunjangan Proyek</label>
                <div class="col-md-9">                          
                    <?php echo form_input($tunjangan_proyek); ?>     
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">No KTP</label>
                <div class="col-md-9">
                    <?php echo form_input($no_ktp); ?>                        
                    <?php echo form_error('no_ktp'); ?>  
                </div>
            </div>
        </fieldset>  
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Tanggal Menjabat</label>
                <div class="col-md-9">
                    <?php echo form_input($tanggal_masuk); ?> 
                </div>
            </div>
        </fieldset> 
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Tanggal Berakhir</label>
                <div class="col-md-9">
                    <?php echo form_input($tanggal_akhir_kontrak); ?>                                    
                    <div class="error_popups" id="error-no-sk"></div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-10">
                    <?php echo form_submit($submitin); ?>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
</div>