<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
<script>
    $(document).ready(function() {
        //validasi
        $("#form_validate").validate({
            rules: {
                "nama_divisi": {
                    required: true
                }
            }
        });
    });
</script>
<div style="min-width:600px;">
    <div class="box-title">
        <h3>Tambah Data Divisi</h3> 
    </div>
    <div class="box-inner form-horizontal">
        <div id="url" class="hidden" data-alamat="<?php echo base_url($urlnya . '/get_data/') ?>"></div>
        <?php echo form_open($urlnya . '/simpan', array('id' => 'form_validate', 'data-reload' => base_url($urlnya . '/index?data=reload'))); ?>
        <input type="hidden" name="targetData" value="true">  
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Nama Divisi</label>
                <div class="col-md-9">   
                    <?php echo form_input($nama_divisi); ?>
                    <?php echo form_error('nama_divisi'); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-10">
                    <?php echo form_submit($submitin); ?>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
</div>
<script>
    // When the document is ready
    $(document).ready(function() {
        //validation rules                       
        $("#form_validate").validate({
            rules: {
                "nama_jabatan": {
                    required: true
                }
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
//        $('#status_panitia').on('change', function() {
//            if (this.checked === true) {//centang
//                $('#status_panitia').val('aktif');
//                $('#check-value').html('aktif');
//            }
//            else {
//                $('#status_panitia').val('tidak aktif');
//                $('#check-value').html('tidak aktif');
//            }
//        });
//
//        var frm = $('#form-tambah');
//        frm.on('submit', function(ev) {
//            console.log(frm.serializeArray());
//            $.ajax({
//                type: 'POST',
//                url: frm.attr('action'),
//                data: frm.serializeArray(),
//                success: function(data) {
//                    var m = data.search('sukses');
//                    if (m != -1) {
//                        $('#submitin').val('Data tersimpan');
//                        $('#tambah-alert').fadeIn(200).delay(2000).fadeOut(200);
//                        window.setTimeout(function() {
//                            $('#reload').load(frm.attr('data-reload'));
//                            $.fancybox.close();
//                        }, 500);
//                    } else {
//                        //$('#submitin').val('Cek inputan');
//                        $('#error-no-sk').html($('#nomor_sk').data('error-empty')).fadeIn(200).delay(2000).fadeOut(200);
//                        $('#error-username').html($('#autocomplete').data('error-empty')).fadeIn(200).delay(2000).fadeOut(200);
//                    }
//                }
//            });
//            ev.preventDefault();
//        });

    });
</script>