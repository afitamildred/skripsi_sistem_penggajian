
<script src="<?php echo base_url('assets/js/jquery-1.7.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/autoNumeric.js'); ?>"></script>
<script>
    $(document).ready(function() {
        //validasi
        $("#form_validate").validate({
            rules: {
                "nama_bonus": {
                    required: true
                },
                "jumlah_bonus": {
                    required: true
                },
                "username": {
                    required: true
                }

            }
        });

        $("#username").change(function() {
            var username = $("#username").val();

            if (username == '') {
                $("#nama").val('');
                $("#divisi").val('');
                $("#jabatan").val('');
                $("#gapok_tunj").val(0);
                $("#gapok_unNum").val(0);
            } else {
//            alert(kr_akun_level1_kode);
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url($urlnya); ?>/get_data_karyawan",
                    data: "username=" + username,
                    success: function(data) {
//            alert(data);
                        var pecah = data.split(',');
                        var nama_karyawan = pecah[0];
                        var divisi = pecah[1];
                        var jabatan = pecah[2];
                        $("#nama").val(nama_karyawan);
                        $("#divisi").val(divisi);
                        $("#jabatan").val(jabatan);
                    }
                });
            }
        });
        jQuery(function($) {
            $('#jumlah_bonus').autoNumeric('init', {aSep: '.', aDec: ','});
        });
        $(document).on('keyup', '#jumlah_bonus', function() {
            var jumlah_bonus = parseInt($('#jumlah_bonus').autoNumeric('get'));
            $('#jumlah_bonus_unNum').val(jumlah_bonus);
        });


    });
</script>
<div style="min-width:600px;">

    <div class="box-title">
        <h3>Tambah Data Bonus</h3> 
    </div>
    <div class="box-inner form-horizontal">
        <div id="url" class="hidden" data-alamat="<?php echo base_url($urlnya . '/get_data/') ?>"></div>
        <?php echo form_open($urlnya . '/simpan', array('id' => 'form_validate', 'data-reload' => base_url($urlnya . '/index?data=reload'))); ?>
        <input type="hidden" name="targetData" value="true">  
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Periode </label>
                <div class="col-md-9">
                    <select name="periode" id="periode" class="form-control">
                        <?php
                        $i = 1;
                        foreach ($bul as $row) {
                            if ($bulan == $i) {
                                ?>
                                <option value="<?php echo $row ?>" selected=""><?php echo $row; ?></option>
                            <?php } else {
                                ?>
                                <option value="<?php echo $row ?>"><?php echo $row; ?></option>

                                <?php
                            }$i++;
                        }
                        ?>
                    </select>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">   
                    <select name="username" id="username" class="form-control m-b">
                        <option value="">--Select Username Karyawan--</option>
                        <?php foreach ($username_karyawan as $row) { ?>
                            <option value="<?php echo $row->username ?>"><?php echo $row->username . '  ||  ' . $row->nama ?></option>
                        <?php } ?>
                    </select>

                    <?php echo form_error('username'); ?>  
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Nama</label>
                <div class="col-md-9">
                    <?php echo form_input($nama); ?>
                </div>
            </div>
        </fieldset>                    
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Divisi</label>
                <div class="col-md-9">                          
                    <?php echo form_input($divisi); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jabatan</label>
                <div class="col-md-9">                          
                    <?php echo form_input($jabatan); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Nama Bonus</label>
                <div class="col-md-9">
                    <?php echo form_input($nama_bonus); ?>           
                    <?php echo form_error('nama_bonus'); ?>                                 
                    <div class="error_popups" id="error-no-sk"></div>
                </div>
            </div>
        </fieldset>  
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jumlah Bonus</label>
                <div class="col-md-9">
                    <?php echo form_input($jumlah_bonus); ?>          
                    <?php echo form_error('jumlah_bonus'); ?>   
                    <input type="hidden" name="jumlah_bonus_unNum" value="" id="jumlah_bonus_unNum" >     
                </div>
            </div>
        </fieldset> 
        <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-10">
                    <?php echo form_submit($submitin); ?>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
</div>

<script>
    $(document).ready(function() {
//        $('#status_panitia').on('change', function() {
//            if (this.checked === true) {//centang
//                $('#status_panitia').val('aktif');
//                $('#check-value').html('aktif');
//            }
//            else {
//                $('#status_panitia').val('tidak aktif');
//                $('#check-value').html('tidak aktif');
//            }
//        });
//
//        var frm = $('#form-tambah');
//        frm.on('submit', function(ev) {
//            console.log(frm.serializeArray());
//            $.ajax({
//                type: 'POST',
//                url: frm.attr('action'),
//                data: frm.serializeArray(),
//                success: function(data) {
//                    var m = data.search('sukses');
//                    if (m != -1) {
//                        $('#submitin').val('Data tersimpan');
//                        $('#tambah-alert').fadeIn(200).delay(2000).fadeOut(200);
//                        window.setTimeout(function() {
//                            $('#reload').load(frm.attr('data-reload'));
//                            $.fancybox.close();
//                        }, 500);
//                    } else {
//                        //$('#submitin').val('Cek inputan');
//                        $('#error-no-sk').html($('#nomor_sk').data('error-empty')).fadeIn(200).delay(2000).fadeOut(200);
//                        $('#error-username').html($('#autocomplete').data('error-empty')).fadeIn(200).delay(2000).fadeOut(200);
//                    }
//                }
//            });
//            ev.preventDefault();
//        });

    });
</script>