</div>
<!-- JavaScript -->
<?php
if (isset($scripts)) {
    foreach ($scripts as $script) {
        echo $script;
    }
}
?>

<script type="text/javascript">

    $(document).ready(function() {
        $("body").tooltip({selector: '[data-toggle=tooltip]'});
        $("#loader").fadeIn(2000, function() {
            window.setTimeout(function() {
                $.ajax({
                    type: 'POST',
                    url: $("#loader").attr('data-url'),
                    data: $('#loader').serializeArray(),
                    success: function(data) {
                        $("#reload").load($("#loader").attr('data-url')).fadeIn(200).delay(2000);
                    }
                });
            }, 1000);
        });
    });
<?php
//datetimepicker
if (isset($datepicker_js)) {
    ?>
        jQuery(function($) {
            var anc = {};
            $('.datetimepicker').datetimepicker({
                lang: 'id',
                timepicker: false,
                format: 'Y-m-d',
            });
        });
    <?php
}
?>
    //alert
    $(function() {
        setTimeout(
                function() {
                    $("#alert").hide();
                }, 3000);
    });
    //sidebar menu
    $(function() {
        $('.sidebar-nav').metisMenu({
            toggle: false
        });
    });

    //tooltip
    $(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
    $(document).ready(function() {//begin: document ready
<?php
//popups
if (isset($popups_js)) {
    echo '$(".conbtn").fancybox();';
}
//dataTable http://localhost/edelivery/ppk/pemb_secara_elektronik/pemb_elektronik
if (isset($dataTable_pemb_elektronik_js)) {
    ?>
            oTable = $('#table').dataTable({
                "paging": false,
                "ordering": false,
                "info": false,
                "bFilter": true,
                "bLengthChange": false
            });
            //kondisi pilih nama pekerjaan
            $('#pekerjaan_list').on('change', function() {
                oTable.fnFilter(this.value);
            });
            //text
            $('input[name=cari]').keyup(function() {
                oTable.fnFilter(this.value);
            });

            //kegiatan
            $('#kegiatan').on('change', function() {
                oTable.fnFilter(this.value);
            });
    <?php
}

//dataTable http://localhost/edelivery/ppk/pemb_secara_elektronik/pemb_elektronik
if (isset($dataTable_draft_spk_lelang)) {
    ?>
            oTable = $('#table').dataTable({
                "paging": false,
                "ordering": false,
                "info": false,
                "bFilter": true,
                "bLengthChange": false
            });
            //kondisi pilih nama pekerjaan
            $('#pekerjaan_list').on('change', function() {
                oTable.fnFilter(this.value);
            });
            //text
            $('input[name=cari]').keyup(function() {
                oTable.fnFilter(this.value);
            });

            //kegiatan
            $('#kegiatan').on('change', function() {
                oTable.fnFilter(this.value);
            });
    <?php
}
//dataTable http://localhost/edelivery/ppk/hps, http://localhost/edelivery/ppk/panitia, http://localhost/edelivery/ppk/pengawas
if (isset($dataTable_edit_spk)) {
    ?>
            $('.editor').summernote({
                height: 250, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: true,
            });
            oTable = $('#table').dataTable({
                "paging": false,
                "ordering": false,
                "info": false,
                "bFilter": true,
                "bLengthChange": false
            });
            $('#pekerjaan_list').on('change', function() {
                oTable.fnFilter(this.value);
            });

            //kondisi jenis addendum
            $('#jenis').on('change', function() {
                oTable.fnFilter(this.value);
            });

            //kondisi status addendum
            $('#status').on('change', function() {
                oTable.fnFilter(this.value);
            });



            $('input[name=cari]').keyup(function() {
                oTable.fnFilter(this.value);
            });

            $('.nomor_surat').on('keyup', function() {
                var id_nomor_surat = $(this).attr('id');
                if (this.value) {
                    $('#tanggal_surat_' + id_nomor_surat).prop('disabled', false);
                } else {
                    $('#tanggal_surat_' + id_nomor_surat).prop('disabled', true);
                    $('#tanggal_surat_' + id_nomor_surat).val('');
                }
            });
    <?php
}

if (isset($dataTable_js)) {
    ?>

            oTable = $('#table').dataTable({
                "paging": true,
                "ordering": false,
                "info": false,
                "bFilter": true,
                "bLengthChange": false
            });
            $('#pekerjaan_list').on('change', function() {
                oTable.fnFilter(this.value);
            });

            //kondisi jenis addendum
            $('#jenis').on('change', function() {
                oTable.fnFilter(this.value);
            });

            //kondisi status addendum
            $('#status').on('change', function() {
                oTable.fnFilter(this.value);
            });

            $('input[name=cari]').keyup(function() {
                oTable.fnFilter(this.value);
            });

            $('.nomor_surat').on('keyup', function() {
                var id_nomor_surat = $(this).attr('id');
                if (this.value) {
                    $('#tanggal_surat_' + id_nomor_surat).prop('disabled', false);
                } else {
                    $('#tanggal_surat_' + id_nomor_surat).prop('disabled', true);
                    $('#tanggal_surat_' + id_nomor_surat).val('');
                }
            });
    <?php
}

if (isset($dataTable_set_pengawas_js)) {
    ?>
            oTable = $('#table').dataTable({
                "paging": true,
                "ordering": false,
                "info": false,
                "bFilter": true,
                "bLengthChange": false,
                "iDisplayLength": 5,
                "columnDefs": [
                    {
                        "targets": [5],
                        "visible": false,
                        "searchable": true
                    }
                ]
            });
            $('input[name=cari]').keyup(function() {
                oTable.fnFilter(this.value);
            });
            $('#jenis').on('change', function() {
                oTable.fnFilter(this.value);
            });
            $('#pilih_semua').on('change', function() {
                if (this.checked === true) {//centang
                    $('.id_rab22').prop('checked', true);
                }
                else {
                    $('.id_rab22').prop('checked', false);
                }
            });
    <?php
}
if (isset($autocomplete_instansi)) {
    ?>
            $('#autocomplete').on('keyup', function() {
                var urlbaru = $('#url').data('npwp');
                $("#autocomplete").autocomplete({
                    minLength: 1,
                    source:
                            function(req, add) {
                                $.ajax({
                                    url: urlbaru,
                                    dataType: 'json',
                                    type: 'POST',
                                    data: req,
                                    success: function(data) {
                                        if (data.response === "true") {
                                            add(data.message);
                                        } else {
                                            add(data.value);
                                        }
                                    }
                                });
                            }, select: function(event, ui) {
                        // Pencarian NPWP Perusahaan
                        $("#autocomplete").val(ui.item.value);
                        $("#alamat").val(ui.item.alamat);
                        $("#email").val(ui.item.email);
                        $("#direktur").val(ui.item.direktur);
                        $("#id_perusahaan").val(ui.item.id_perusahaan);
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url($urlnya); ?>/get_pekerjaan_perusahaan",
                            data: "id_perusahaan=" + ui.item.id_perusahaan,
                            success: function(data) {
                                $("#tabel_pekerjaan").html(data);
                            }
                        });
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    return $("<li>")
                            .append("<a style='cursor:pointer;'><strong>" + item.value + "</strong><br />" + "\
                                            <span style='margin-left:20px'>" + item.alamat + "</span>" + "\
                                            <span style='margin-left:20px'>" + item.email + "</span>" + "\
                                            <span style='margin-left:20px'>" + item.direktur + "</span>" + "\
                                            </a>")
                            .appendTo(ul);
                };
            });
    <?php
}
//dataTable http://localhost/edelivery/ppk/progress_kontrak/progress
if (isset($dataTable_progres_kontrak_js)) {
    ?>
            oTable = $('#table').dataTable({
                "paging": false,
                "ordering": false,
                "info": false,
                "bFilter": true,
                "bLengthChange": false
            });
            //kondisi status
            $('#status_kontrak').on('change', function() {
                oTable.fnFilter(this.value);
            });
            //kondisi status
            $('#status_pengadaan').on('change', function() {
                oTable.fnFilter(this.value);
            });
            //text
            $('input[name=cari]').keyup(function() {
                oTable.fnFilter(this.value);
            });
    <?php
}
//http://localhost/edelivery/ppk/pengadaan_langsung/addendum/daftar_addendum
if (isset($dataTable_pl_daftar_addendum_js)) {
    ?>
            $(document).ready(function() {
                oTable = $('#table').dataTable({
                    "paging": false,
                    "ordering": false,
                    "info": false,
                    "bFilter": true,
                    "bLengthChange": false,
                    "columnDefs": [
                        {
                            "targets": [4],
                            "visible": true,
                            "searchable": true
                        }
                    ]
                });

                //kondisi nama kegiatan
                $('#kegiatan').on('change', function() {
                    oTable.fnFilter(this.value);
                });

                //kondisi jenis addendum
                $('#jenis').on('change', function() {
                    oTable.fnFilter(this.value);
                });

                //kondisi status addendum
                $('#status').on('change', function() {
                    oTable.fnFilter(this.value);
                });

                //kondisi berdasar ketikan
                $('input[name=cari]').keyup(function() {
                    oTable.fnFilter(this.value);
                });
            });
    <?php
}
?>

    });//end: document ready
</script>

</body>
</html>