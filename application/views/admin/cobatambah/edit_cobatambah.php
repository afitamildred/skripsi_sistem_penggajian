<div style="min-width:600px;">

    <div class="box-title">
        <h3>Edit Data Golongan Cobatambah</h3> 
    </div>
    <div class="box-inner form-horizontal">
        <div id="url" class="hidden" data-alamat="<?php echo base_url($urlnya . '/get_data/') ?>"></div>
        <?php echo form_open($urlnya . '/simpan_edit', array('id' => 'form-tambah', 'data-reload' => base_url($urlnya . '/index?data=reload'))); ?>
        <input type="hidden" name="targetData" value="true">  
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Status Karyawan</label>
                <div class="col-md-9">                       
                    <?php echo form_input($id); ?>
                    <select name="status_karyawan" id="status_karyawan" class="form-control m-b">
                        <option value="">--Pilih Status Karyawan--</option>
                        <option value="Kontrak" 
                        <?php
                        if ($status_karyawan == 'Kontrak') {
                            echo 'selected';
                        };
                        ?>>Kontrak</option>
                        <option value="Tetap"
                        <?php
                        if ($status_karyawan == 'Tetap') {
                            echo 'selected';
                        };
                        ?>>Tetap</option>

                    </select>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Divisi</label>
                <div class="col-md-9">                          
                    <select name="divisi" id="divisi" class="form-control m-b">
                        <option value="">--Pilih Divisi--</option>
                        <?php foreach ($data_divisi as $row) { ?>
                            <option value="<?php echo $row->id; ?>" 
                            <?php
                            if ($row->id == $cobatambah[0]->id_divisi) {
                                echo 'selected';
                            };
                            ?>> <?php echo $row->nama_divisi ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jabatan</label>
                <div class="col-md-9">                      
                    <select name="jabatan" id="jabatan" class="form-control m-b">
                        <option value="">--Pilih Jabatan--</option>
                        <?php foreach ($data_jabatan as $row) { ?>
                            <option value="<?php echo $row->id; ?>" 
                            <?php
                            if ($row->id == $cobatambah[0]->id_jabatan) {
                                echo 'selected';
                            };
                            ?>> <?php echo $row->nama_jabatan ?>
                            </option>
                        <?php } ?>
                    </select>   
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Gaji Pokok </label>
                <div class="col-md-9">
                    <?php echo form_input($gaji_pokok); ?>         
                    <?php echo form_error('gaji_pokok'); ?>    
                    <input type="hidden" name="gaji_pokok_unNum" value="<?php echo $cobatambah[0]->gaji_pokok;?>" id="gaji_pokok_unNum" >
                </div>
            </div>
        </fieldset> 
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jumlah Lembur Perjam</label>
                <div class="col-md-9">
                    <?php echo form_input($lembur_perjam); ?>       
                    <?php echo form_error('lembur_perjam'); ?>    
                    <input type="hidden" name="lembur_perjam_unNum" value="<?php echo $cobatambah[0]->lembur_perjam;?>" id="lembur_perjam_unNum" >     
                </div>
            </div>
        </fieldset> 
        <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-10">
                    <?php echo form_submit($submitin); ?>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
</div>
<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/autoNumeric.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $("#form_validate").validate({
            rules: {
                "gaji_pokok": {
                    required: true
                },
                "lembur_perjam": {
                    required: true
                }
            }
        });
        jQuery(function($) {
            $('#lembur_perjam').autoNumeric('init', {aSep: '.', aDec: ','});
        });
        $(document).on('keyup', '#lembur_perjam', function() {
            var lembur_perjam = parseInt($('#lembur_perjam').autoNumeric('get'));
//            alert(lembur_perjam);
            $('#lembur_perjam_unNum').val(lembur_perjam);
        });
        jQuery(function($) {
            $('#gaji_pokok').autoNumeric('init', {aSep: '.', aDec: ','});
        });
        $(document).on('keyup', '#gaji_pokok', function() {
            var gaji_pokok = parseInt($('#gaji_pokok').autoNumeric('get'));
//            alert(gaji_pokok);
            $('#gaji_pokok_unNum').val(gaji_pokok);
        });
    });
</script>