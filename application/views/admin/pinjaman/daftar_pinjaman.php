<div class="col-lg-10">
    <div class="breadcrumb-wrapper">            
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/dashboard'); ?>">Home</a></li>
            <li class="active">Daftar Pinjaman</li>
        </ol>
    </div>         
    <div class="box">
        <div class="box-title">
            <a class='btn btn-primary conbtn_autocomplete fancybox.ajax pull-right col-sm-2' style="text-align:center;margin-top: 0px;padding: 8px;" data-toggle='tooltip' data-placement='top' title='Tambah Data Pinjaman' href='<?php echo base_url($urlnya . '/add_pinjaman'); ?>'>
                Tambah
            </a>
            <h2><?php echo $page_title; ?></h2>

        </div>

        <div class="clear"></div>
        <div class="box-inner" id="reload">
            
            <fieldset>
                <div class="form-group">
                    <div class="col-md-10" style="padding: 0px !important;">
                        <input name="cari" type="text" class="form-control" placeholder="Cari data karyawan berdasarkan nama atau divisi . . .">
                    </div>
                    <div class="col-md-2 pull-right" style="padding: 0px !important; text-align: right">
                        <a class='btn btn-success conbtn fancybox.ajax' align="right" data-toggle='tooltip' data-placement='top' title='Cetak Data Pinjaman' href='<?php echo base_url($urlnya . '/cetak_data'); ?>'>Cetak Data Pinjaman</a>
                    </div>
                </div>
            </fieldset>
            <div class="table-responsive">
                <table class="display table table-striped table-bordered" id="table">
                    <thead>
                        <tr class="info">
                            <th class="center-x">No.</th>
                            <th class="center-x">Nama Karyawan</th>
                            <th class="center-x">Divisi</th>
                            <!--<th class="center-x">Jabatan</th>-->
                            <th class="center-x">Jumlah Pinjam</th>
                            <th class="center-x">Angsuran Pinjam</th>
                            <th class="center-x">Sisa Pinjam</th>
                            <th class="center-x">Status Pinjam</th>
                            <th class="center-x">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data_pinjaman as $row) {
                            ?> 
                            <tr>
                                <td class='center-x'><?php echo $no; ?></td>
                                <td><?php echo $row->nama_karyawan; ?></td>
                                <td><?php echo $row->nama_divisi; ?></td>
                                <!--<td><?php // echo $row->nama_jabatan;     ?></td>-->
                                <td align='right'><?php echo format_rupiah($row->jumlah_pinjam); ?></td>

                                <td align='right'><?php echo format_rupiah($row->angsuran_pinjam); ?></td>
                                <td align='right'><?php echo format_rupiah($row->sisa_pinjam); ?></td>
                                <!--<td class='center-x'><?php // echo $row->alasan_pinjam;     ?></td>-->
                                <td><?php echo $row->status_pinjam; ?></td>
                                <td class='center-x'>
                                    <?php if ($row->status_pinjam == 'belum_lunas') { ?>
                                        <a class='btn btn-warning conbtn fancybox.ajax' data-toggle='tooltip' data-placement='top' title='Edit' href='<?php echo base_url($urlnya . '/edit/' . $row->id); ?>'><i class="fa fa-pencil"></i></a>

                                        <?php
                                    } else {
                                        echo '';
                                    }
                                    ?>
                                    <a class='btn btn-danger conbtn fancybox.ajax' data-toggle='tooltip' data-placement='top' title='Hapus' href='<?php echo base_url($urlnya . '/validasi_hapus/' . $row->id); ?>'><i class="fa fa-trash"></i></a>
                                </td>
                                <?php
                                $no++;
                            }
                            ?>
                    </tbody>
                </table>
                <div id="pages">
                    <ul class="pagination">

                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>