<?php
//echo $id;
?>
<div style="min-width:450px;">
    <div class="box-inner form-horizontal">
        <fieldset>
            <div class="">
                Apakah anda yakin akan menghapus data <?php echo '<b>'.$nama.'</b>'; ?> ?
            </div><br />
            <div align="right">
                <a class='btn btn-primary' data-toggle='tooltip' data-placement='top'  href='<?php echo base_url($urlnya . '/hapus/' . $id); ?>'>Ya</a>
                <a class='btn btn-danger' data-toggle='tooltip' data-placement='top' href='<?php echo base_url($urlnya); ?>'>Tidak</a>
            </div>
        </fieldset>
    </div>
</div>
