<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/autoNumeric.js'); ?>"></script>
<?php
if (isset($scripts)) {
    foreach ($scripts as $script) {
        echo $script;
    }
}
?>
<script>
    $(document).ready(function() {
        jQuery(function($) {
            var anc = {};
            $('.datetimepicker').datetimepicker({
                lang: 'id',
                timepicker: false,
                format: 'Y-m-d'
            });
        });
        //validasi
        $("#form_validate").validate({
            rules: {
                "jumlah_pinjam": {
                    required: true
                },
                "alasan_pinjam": {
                    required: true
                }
            }
        });
        jQuery(function($) {
            $('#jumlah_pinjam').autoNumeric('init', {aSep: '.', aDec: ','});
        });
        $(document).on('keyup', '#jumlah_pinjam', function() {
            var jumlah_pinjam = parseInt($('#jumlah_pinjam').autoNumeric('get'));
            $('#jumlah_pinjam_unNum').val(jumlah_pinjam);
        });
    });
</script>
<div style="min-width:600px;">

    <div class="box-title">
        <h3>Edit Data Pinjaman</h3> 
    </div>
    <div class="box-inner form-horizontal">
        <div id="url" class="hidden" data-alamat="<?php echo base_url($urlnya . '/get_data/') ?>"></div>
        <?php echo form_open($urlnya . '/simpan_edit', array('id' => 'form-tambah', 'data-reload' => base_url($urlnya . '/index?data=reload'))); ?>
        <input type="hidden" name="targetData" value="true">  
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">   
                    <?php echo form_input($id); ?>
                    <?php echo form_input($username); ?>
                    <div class="error_popups" id="error-username"></div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Nama</label>
                <div class="col-md-9">
                    <?php echo form_input($nama); ?>
                </div>
            </div>
        </fieldset>                    
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Divisi</label>
                <div class="col-md-9">                          
                    <?php echo form_input($divisi); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jabatan</label>
                <div class="col-md-9">                          
                    <?php echo form_input($jabatan); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Tanggal Pinjam</label>
                <div class="col-md-9">
                    <?php echo form_input($tanggal_pinjam); ?>                                    
                    <div class="error_popups" id="error-no-sk"></div>
                </div>
            </div>
        </fieldset>  
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jumlah Pinjam</label>
                <div class="col-md-9">
                    <?php echo form_input($jumlah_pinjam); ?>          
                    <?php echo form_error('jumlah_pinjam'); ?>
                    <input type="hidden" name="jumlah_pinjam_unNum" value="" id="jumlah_pinjam_unNum" > 
                </div>
            </div>
        </fieldset> 
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Alasan Pinjam</label>
                <div class="col-md-9">
                    <?php echo form_input($alasan_pinjam); ?>                                    
                    <div class="error_popups" id="error-no-sk"></div>
                </div>
            </div>
        </fieldset> 

        <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-10">
                    <?php echo form_submit($submitin); ?>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
</div>

