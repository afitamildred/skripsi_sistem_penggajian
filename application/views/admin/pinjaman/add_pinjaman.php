<script src="<?php echo base_url('assets/js/jquery-1.7.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/autoNumeric.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $("#form_validate").validate({
            rules: {
                "jumlah_pinjam": {
                    required: true
                },
                "alasan_pinjam": {
                    required: true
                },
                "tanggal_pinnjam": {
                    required: true
                },
                "username": {
                    required: true
                }
            }
        });
        $("#username").change(function() {
            var username = $("#username").val();
//            alert(kr_akun_level1_kode);
            if (username == '') {
                $("#nama").val('');
                $("#divisi").val('');
                $("#jabatan").val('');
                $("#gapok_tunj").val(0);
                $("#gapok_unNum").val(0);
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url($urlnya); ?>/get_data_karyawan",
                    data: "username=" + username,
                    success: function(data) {
                        var pecah = data.split(',');
                        var nama_karyawan = pecah[0];
                        var divisi = pecah[1];
                        var jabatan = pecah[2];
                        $("#nama").val(nama_karyawan);
                        $("#divisi").val(divisi);
                        $("#jabatan").val(jabatan);
                    }
                });
            }
        });
        jQuery(function($) {
            $('#jumlah_pinjam').autoNumeric('init', {aSep: '.', aDec: ','});
        });
        $(document).on('keyup', '#jumlah_pinjam', function() {
            var jumlah_pinjam = parseInt($('#jumlah_pinjam').autoNumeric('get'));
            $('#jumlah_pinjam_unNum').val(jumlah_pinjam);
        });
    });
</script>

<div style="min-width:600px;">
    <div class="box-title">
        <h3>Tambah Data Pinjaman</h3> 
    </div>
    <div class="box-inner form-horizontal">
        <div id="url" class="hidden" data-alamat="<?php echo base_url($urlnya . '/get_data/') ?>"></div>
        <?php echo form_open($urlnya . '/simpan', array('id' => 'form_validate', 'data-reload' => base_url($urlnya . '/index?data=reload'))); ?>
        <input type="hidden" name="targetData" value="true">  
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">   
                    <select name="username" id="username" class="form-control m-b">
                        <option value="">--Select Username Karyawan--</option>
                        <?php foreach ($username_karyawan as $row) { ?>
                            <option value="<?php echo $row->username ?>"><?php echo $row->username . '  ||  ' . $row->nama ?></option>
                        <?php } ?>
                    </select>
                    <?php echo form_error('username'); ?>  
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Nama</label>
                <div class="col-md-9">
                    <?php echo form_input($nama); ?>
                </div>
            </div>
        </fieldset>                    
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Divisi</label>
                <div class="col-md-9">                          
                    <?php echo form_input($divisi); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jabatan</label>
                <div class="col-md-9">                          
                    <?php echo form_input($jabatan); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Tanggal Pinjam</label>
                <div class="col-md-9">
                    <?php echo form_input($tanggal_pinjam); ?>                                    
                    <?php echo form_error('tanggal_pinjam'); ?> 
                </div>
            </div>
        </fieldset>  
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jumlah Pinjam</label>
                <div class="col-md-9">
                    <?php echo form_input($jumlah_pinjam); ?>           
                    <?php echo form_error('jumlah_pinjam'); ?>
                    <input type="hidden" name="jumlah_pinjam_unNum" value="" id="jumlah_pinjam_unNum" > 
                </div>
            </div>
        </fieldset> 
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Alasan Pinjam</label>
                <div class="col-md-9">
                    <?php echo form_input($alasan_pinjam); ?>            
                    <?php echo form_error('alasan_pinjam'); ?>            
                </div>
            </div>
        </fieldset> 
        <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-10">
                    <?php echo form_submit($submitin); ?>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
</div>
<?php
if (isset($scripts)) {
    foreach ($scripts as $script) {
        echo $script;
    }
}
?>
<script type="text/javascript">
    jQuery(function($) {
        var anc = {};
        $('.datetimepicker').datetimepicker({
            lang: 'id',
            timepicker: false,
            format: 'Y-m-d'
        });
    });
</script>