<div style="min-width:600px;">

    <div class="box-title">
        <h3>Edit Data Tunjangan Proyek</h3> 
    </div>
    <div class="box-inner form-horizontal">
        <div id="url" class="hidden" data-alamat="<?php echo base_url($urlnya . '/get_data/') ?>"></div>
        <?php echo form_open($urlnya . '/simpan_edit', array('id' => 'form-tambah', 'data-reload' => base_url($urlnya . '/index?data=reload'))); ?>
        <input type="hidden" name="targetData" value="true">  
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Nama Proyek</label>
                <div class="col-md-8">   
                    <?php echo form_input($nama_proyek); ?>
                    <?php echo form_input($id); ?>
                    <div class="error_popups" id="error-username"></div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Jumlah Tunjangan</label>
                <div class="col-md-8">                          
                    <?php echo form_input($tunjangan_proyek); ?>     
                    <?php echo form_error('tunjangan_proyek'); ?>    
                    <input type="hidden" name="tunjangan_proyek_unNum" value="" id="tunjangan_proyek_unNum" > 
                </div>
            </div>
        </fieldset>   
        <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-10">
                    <?php echo form_submit($submitin); ?>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
</div>
<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/autoNumeric.js'); ?>"></script>
<script>
    $(document).ready(function() {
        //validasi
        $("#form_validate").validate({
            rules: {
                "nama_proyek": {
                    required: true
                },
                "tunjangan_proyek": {
                    required: true
                }
            }
        });
        //autonumeric
        jQuery(function($) {
            $('#tunjangan_proyek').autoNumeric('init', {aSep: '.', aDec: ','});
        });
        //isi total jika sudah diisi
        $(document).on('keyup', '#tunjangan_proyek', function() {
            var tunjangan_proyek = parseInt($('#tunjangan_proyek').autoNumeric('get'));
//            alert(lembur_perjam);
            $('#tunjangan_proyek_unNum').val(tunjangan_proyek);
        });
    });
</script>