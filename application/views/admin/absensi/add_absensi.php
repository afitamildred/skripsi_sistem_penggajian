<script src="<?php echo base_url('assets/js/jquery-1.7.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/autoNumeric.js'); ?>"></script>

<script>
    function convertToRupiah(angka) {
        var rupiah = '';
        var angkarev = angka.toString().split('').reverse().join('');
        for (var i = 0; i < angkarev.length; i++)
            if (i % 3 === 0)
                rupiah += angkarev.substr(i, 3) + '.';
        return rupiah.split('', rupiah.length - 1).reverse().join('');
    }
    $(document).ready(function() {
        //validasi
        $("#form_validate").validate({
            rules: {
                "jumlah_harus_hadir": {
                    required: true
                },
                "jumlah_hadir": {
                    required: true
                },
                "username": {
                    required: true
                }
            }
        });
        $("#username").change(function() {
            var username = $("#username").val();
            if (username == '') {
                $("#nama").val('');
                $("#divisi").val('');
                $("#jabatan").val('');
                $("#gapok_tunj").val(0);
                $("#gapok_unNum").val(0);
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url($urlnya); ?>/get_data_karyawan",
                    data: "username=" + username,
                    success: function(data) {
                        var pecah = data.split(',');
                        var nama_karyawan = pecah[0];
                        var divisi = pecah[1];
                        var jabatan = pecah[2];
                        var tunj = parseInt(pecah[3]);
                        var gapok = parseInt(pecah[4]);
                        var gapok_tunj = parseInt(tunj + gapok);
                        $("#nama").val(nama_karyawan);
                        $("#divisi").val(divisi);
                        $("#jabatan").val(jabatan);
                        $("#gapok_tunj").val(convertToRupiah(gapok_tunj));
                        $("#gapok_unNum").val(gapok_tunj);
                    }
                });
            }

        });
        jQuery(function($) {
            $('#potongan').autoNumeric('init', {aSep: '.', aDec: ','});
        });

        $('#jumlah_hadir').keyup(function() {
            var jumlah_hadir = $(this).val();
            var jml_hrus = $("#jumlah_harus_hadir").val();
            var absen = jml_hrus - jumlah_hadir;
            var total_gaji = parseInt($("#gapok_unNum").val());
            var itung = total_gaji / jml_hrus;
            var minus = itung * absen;
            var pembulatan1 = minus / 1000;
            var pembulatan2 = Math.floor(pembulatan1);
            var pembulatan3 = pembulatan2 * 1000;
            $("#potongan").val(convertToRupiah(pembulatan3));
            $("#potongan_unNum").val(pembulatan3);

        });

    });
</script>
<div style="min-width:750px;">

    <div class="box-title">
        <h3>Tambah Data Absensi Karyawan</h3> 
    </div>
    <div class="box-inner form-horizontal">
        <?php echo form_open($urlnya . '/simpan', array('id' => 'form_validate', 'data-reload' => base_url($urlnya . '/index?data=reload'))); ?>

        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Periode </label>
                <div class="col-md-8">
                    <select name="periode" id="periode" class="form-control">
                        <?php
                        $i = 1;
                        foreach ($bul as $row) {
                            if ($bulan == $i) {
                                ?>
                                <option value="<?php echo $row ?>" selected=""><?php echo $row; ?></option>
                            <?php } else {
                                ?>
                                <option value="<?php echo $row ?>"><?php echo $row; ?></option>

                                <?php
                            }$i++;
                        }
                        ?>
                    </select>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Username</label>
                <div class="col-md-8">   
                    <select name="username" id="username" class="form-control m-b">
                        <option value="">--Select Username Karyawan--</option>
                        <?php foreach ($username_karyawan as $row) { ?>
                            <option value="<?php echo $row->username ?>"><?php echo $row->username . '  ||  ' . $row->nama ?></option>
                        <?php } ?>
                    </select>
                    <?php echo form_error('username'); ?>  
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Nama Lengkap</label>
                <div class="col-md-8">
                    <?php echo form_input($nama); ?>
                </div>
            </div>
        </fieldset>                    
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Divisi</label>
                <div class="col-md-8">                          
                    <?php echo form_input($divisi); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Jabatan</label>
                <div class="col-md-8">
                    <?php echo form_input($jabatan); ?>                  
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Gaji Pokok + Tunjangan Proyek</label>
                <div class="col-md-8">
                    <?php echo form_input($gapok_tunj); ?>            
                    <input type="hidden" name="gapok_unNum" value="" id="gapok_unNum" >
                </div>
            </div>
        </fieldset>

        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Jumlah harus hadir</label>
                <div class="col-md-8">
                    <?php echo form_input($jumlah_harus_hadir); ?>           
                    <?php echo form_error('jumlah_harus_hadir'); ?>  
                </div>
            </div>
        </fieldset> 
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Jumlah hadir</label>
                <div class="col-md-8">
                    <?php echo form_input($jumlah_hadir); ?>                
                    <?php echo form_error('jumlah_hadir'); ?>  
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Potongan</label>
                <div class="col-md-8">
                    <?php echo form_input($potongan); ?>                
                    <input type="hidden" name="potongan_unNum" value="" id="potongan_unNum" >
                </div>
            </div>
        </fieldset> 
        <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-10">
                    <?php echo form_submit($submitin); ?>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
</div>
