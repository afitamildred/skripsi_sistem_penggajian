<script src="<?php echo base_url('assets/js/jquery-1.7.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/autoNumeric.js'); ?>"></script>

<script>
    function convertToRupiah(angka) {
        var rupiah = '';
        var angkarev = angka.toString().split('').reverse().join('');
        for (var i = 0; i < angkarev.length; i++)
            if (i % 3 === 0)
                rupiah += angkarev.substr(i, 3) + '.';
        return rupiah.split('', rupiah.length - 1).reverse().join('');
    }
    $(document).ready(function() {
        var gapok = $("#gapok_tunj").val();
        $("#gapok_tunj").val(convertToRupiah(gapok));
        $("#gapok_unNum").val(gapok);
        var pot = $("#potongan").val();
        $("#potongan").val(convertToRupiah(pot));
        $("#potongan_unNum").val(pot);
        //validasi
        $("#form_validate").validate({
            rules: {
                "jumlah_harus_hadir": {
                    required: true
                },
                "jumlah_hadir": {
                    required: true
                }
            }
        });

        $('#jumlah_hadir').keyup(function() {
            var jumlah_hadir = $(this).val();
            var jml_hrus = $("#jumlah_harus_hadir").val();
            var absen = jml_hrus - jumlah_hadir;

            var total_gaji = $("#gapok_unNum").val();
//            alert(total_gaji);
            var itung = total_gaji / jml_hrus;
            var minus = itung * absen;
            var pembulatan1 = minus / 1000;
            var pembulatan2 = Math.floor(pembulatan1);
            var pembulatan3 = pembulatan2 * 1000;
            $("#potongan").val(convertToRupiah(pembulatan3));
            $("#potongan_unNum").val(pembulatan3);

        });

    });
</script>
<div style="min-width:750px;">

    <div class="box-title">
        <h3>Edit Data Absensi Karyawan</h3> 
    </div>
    <div class="box-inner form-horizontal">
        <div id="url" class="hidden" data-alamat="<?php echo base_url($urlnya . '/get_data/') ?>"></div>
        <?php echo form_open($urlnya . '/simpan_edit', array('id' => 'form-tambah', 'data-reload' => base_url($urlnya . '/index?data=reload'))); ?>
        <input type="hidden" name="targetData" value="true">  
        <?php echo form_input($id); ?>
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Username</label>
                <div class="col-md-8">   
                    <?php echo form_input($username); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Nama Lengkap</label>
                <div class="col-md-8">
                    <?php echo form_input($nama); ?>
                </div>
            </div>
        </fieldset>                    
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Divisi</label>
                <div class="col-md-8">                          
                    <?php echo form_input($divisi); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Jabatan</label>
                <div class="col-md-8">
                    <?php echo form_input($jabatan); ?>                  
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Gaji Pokok + Tunjangan Proyek</label>
                <div class="col-md-8">
                    <?php echo form_input($gapok_tunj); ?>            
                    <input type="hidden" name="gapok_unNum" value="" id="gapok_unNum" >
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Periode </label>
                <div class="col-md-8">
                    <select name="periode" id="periode" class="form-control">
                        <?php
                        foreach ($bul as $row) {
                            if ($row == $periode) {
                                ?>
                                <option value="<?php echo $row ?>" selected=""><?php echo $row; ?></option>
                            <?php } else {
                                ?>
                                <option value="<?php echo $row ?>"><?php echo $row; ?></option>

                                <?php
                            }$i++;
                        }
                        ?>
                    </select>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Jumlah harus hadir</label>
                <div class="col-md-8">
                    <?php echo form_input($jumlah_harus_hadir); ?>           
                    <?php echo form_error('jumlah_harus_hadir'); ?>  
                </div>
            </div>
        </fieldset> 
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Jumlah hadir</label>
                <div class="col-md-8">
                    <?php echo form_input($jumlah_hadir); ?>                
                    <?php echo form_error('jumlah_hadir'); ?>  
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-4 control-label">Potongan</label>
                <div class="col-md-8">
                    <?php echo form_input($potongan); ?>                
                    <input type="hidden" name="potongan_unNum" value="" id="potongan_unNum" >
                </div>
            </div>
        </fieldset> 
        <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-10">
                    <?php echo form_submit($submitin); ?>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
</div>
