<script src="<?php echo base_url('assets/js/jquery-1.7.js') ?>" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        $("#username").change(function() {
            var username = $("#username").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url($urlnya); ?>/get_data_karyawan",
                data: "username=" + username,
                success: function(data) {
                    var pecah = data.split(',');
                    var nama_karyawan = pecah[0];
                    var divisi = pecah[1];
                    var jabatan = pecah[2];
                    var jml_bonus = parseInt(pecah[3]);
                    var jml_pinjam = parseInt(pecah[4]);
                    var jml_potongan_absensi = parseInt(pecah[5]);
                    var bonus_lembur = parseInt(pecah[6]);
                    var gapok = parseInt(pecah[8]);
                    var tunjangan_proyek = parseInt(pecah[7]);
                    var pengurangan = jml_potongan_absensi;
                    var penambahan = parseInt(gapok + bonus_lembur + jml_bonus + tunjangan_proyek);
                    var gaber = parseInt(penambahan - pengurangan);
                    $("#pengurangan").val(pengurangan);
                    $("#penambahan").val(penambahan);
                    $("#gaji_pokok").val(gapok);
                    $("#tunjangan_proyek").val(tunjangan_proyek);
                    $("#nama").val(nama_karyawan);
                    $("#divisi").val(divisi);
                    $("#jabatan").val(jabatan);
                    $("#jml_bonus").val(jml_bonus);
                    $("#pinjaman").val(jml_pinjam);
                    $("#potongan_absensi").val(jml_potongan_absensi);
                    $("#lembur").val(bonus_lembur);
                    $("#gaji_bersih").val(gaber);
                }
            });
        });

        $('#persen_bayar').keyup(function() {
            var persen_bayar = $(this).val();
            var jml_pinjam = parseInt($("#pinjaman").val());
            var hitung = (persen_bayar / 100) * jml_pinjam;
            var sisa_pinjam = jml_pinjam - hitung;

            $("#sisa_pinjam").val(sisa_pinjam);
            $("#total_angsuran").val(hitung);
            var pengurangan = parseInt($("#potongan_absensi").val()) + hitung;
            $("#pengurangan").val(pengurangan);

            var gaber = $("#penambahan").val() - pengurangan;
//            alert(gaber);
            $("#gaji_bersih").val(gaber);

        });
    });
</script>
<div style="min-width:600px;">

    <div class="box-title">
        <h3>Tambah Data Gaji</h3> 
    </div>
    <div class="box-inner form-horizontal">
        <div id="url" class="hidden" data-alamat="<?php echo base_url($urlnya . '/get_data/') ?>"></div>
        <?php echo form_open($urlnya . '/simpan', array('id' => 'form-tambah', 'data-reload' => base_url($urlnya . '/index?data=reload'))); ?>
        <input type="hidden" name="targetData" value="true">  
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">   
                    <select name="username" id="username" class="form-control m-b">
                        <option value="">--Select Username Karyawan--</option>
                        <?php foreach ($username_karyawan as $row) { ?>
                            <option value="<?php echo $row->username ?>"><?php echo $row->username . '  ||  ' . $row->nama_karyawan ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Nama</label>
                <div class="col-md-9">
                    <?php echo form_input($nama); ?>
                </div>
            </div>
        </fieldset>                    
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Divisi</label>
                <div class="col-md-9">                          
                    <?php echo form_input($divisi); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jabatan</label>
                <div class="col-md-9">                          
                    <?php echo form_input($jabatan); ?>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Periode </label>
                <div class="col-md-9">
                    <select name="periode" id="periode" class="form-control m-b">
                        <option value="">--Pilih Periode--</option>
                        <option value="Januari">Januari</option>
                        <option value="Februari">Februari</option>
                        <option value="Maret">Maret</option>
                        <option value="April">April</option>
                        <option value="Mei">Mei</option>
                        <option value="Juni">Juni</option>
                        <option value="Juli">Juli</option>
                        <option value="Agustus">Agustus</option>
                        <option value="September">September</option>
                        <option value="Oktober">Oktober</option>
                        <option value="November">November</option>
                        <option value="Desember">Desember</option>
                    </select>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Gaji Pokok</label>
                <div class="col-md-9">
                    <?php echo form_input($gaji_pokok); ?>                                    
                    <div class="error_popups" id="error-no-sk"></div>
                </div>
            </div>
        </fieldset> 
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Tunjangan Proyek</label>
                <div class="col-md-9">
                    <?php echo form_input($tunjangan_proyek); ?>                                    
                    <div class="error_popups" id="error-no-sk"></div>
                </div>
            </div>
        </fieldset> 

        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Lembur</label>
                <div class="col-md-9">
                    <?php echo form_input($lembur); ?>                                    
                    <div class="error_popups" id="error-no-sk"></div>
                </div>
            </div>
        </fieldset>

        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Jumlah Bonus</label>
                <div class="col-md-9">
                    <?php echo form_input($jml_bonus); ?>                                    
                    <div class="error_popups" id="error-no-sk"></div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label"><b>Pendapatan</b></label>
                <div class="col-md-9">
                    <b><?php echo form_input($penambahan); ?>  </b>                                  
                    <div class="error_popups" id="error-no-sk"></div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Potongan Absensi</label>
                <div class="col-md-9">
                    <?php echo form_input($potongan_absensi); ?>                                    
                    <div class="error_popups" id="error-no-sk"></div>
                </div>
            </div>
        </fieldset>

        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Pinjaman</label>
                <div class="col-md-9">
                    <?php echo form_input($pinjaman); ?>                                    
                    <div class="error_popups" id="error-no-sk"></div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Angsuran Pinjaman</label>
                <div class="col-md-2">
                    <input type="text" name="persen_bayar" value="" id="persen_bayar" class="form-control" >     
                </div>
                <label class="col-md-1 control-label">%</label>
                <div class="col-md-6">
                    <input type="text" name="total_angsuran" value="" id="total_angsuran" class="form-control" readonly="">     
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label">Sisa Pinjam</label>
                <div class="col-md-9">
                    <input type="text" name="sisa_pinjam" value="" id="sisa_pinjam" class="form-control" readonly="">     
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label"><b>Pengeluaran</label>
                <div class="col-md-9">
                    <?php echo form_input($pengurangan); ?>                                    
                    <div class="error_popups" id="error-no-sk"></div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label"><b>Gaji Bersih</label>
                <div class="col-md-9">
                    <?php echo form_input($gaji_bersih); ?>                                    
                    <div class="error_popups" id="error-no-sk"></div>
                </div>
            </div>
        </fieldset>


        <fieldset>
            <div class="form-group">
                <label class="col-md-2 control-label"></label>
                <div class="col-md-10">
                    <?php echo form_submit($submitin); ?>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
</div>
<?php
if (isset($scripts)) {
    foreach ($scripts as $script) {
        echo $script;
    }
}
?>
<script type="text/javascript">
    jQuery(function($) {
        var anc = {};
        $('.datetimepicker').datetimepicker({
            lang: 'id',
            timepicker: false,
            format: 'Y-m-d'
        });
    });
</script>
<script>
//    $(document).ready(function() {
//        jQuery(function($) {
//            $('.datetimepicker').datetimepicker({
//                lang: 'id',
//                timepicker: false,
//                format: 'Y-m-d'
//            });
//        });
//        $('#status_panitia').on('change', function() {
//            if (this.checked === true) {//centang
//                $('#status_panitia').val('aktif');
//                $('#check-value').html('aktif');
//            }
//            else {
//                $('#status_panitia').val('tidak aktif');
//                $('#check-value').html('tidak aktif');
//            }
//        });
//
//        var frm = $('#form-tambah');
//        frm.on('submit', function(ev) {
//            console.log(frm.serializeArray());
//            $.ajax({
//                type: 'POST',
//                url: frm.attr('action'),
//                data: frm.serializeArray(),
//                success: function(data) {
//                    var m = data.search('sukses');
//                    if (m != -1) {
//                        $('#submitin').val('Data tersimpan');
//                        $('#tambah-alert').fadeIn(200).delay(2000).fadeOut(200);
//                        window.setTimeout(function() {
//                            $('#reload').load(frm.attr('data-reload'));
//                            $.fancybox.close();
//                        }, 500);
//                    } else {
//                        //$('#submitin').val('Cek inputan');
//                        $('#error-no-sk').html($('#nomor_sk').data('error-empty')).fadeIn(200).delay(2000).fadeOut(200);
//                        $('#error-username').html($('#autocomplete').data('error-empty')).fadeIn(200).delay(2000).fadeOut(200);
//                    }
//                }
//            });
//            ev.preventDefault();
//        });
//
//    });
</script>