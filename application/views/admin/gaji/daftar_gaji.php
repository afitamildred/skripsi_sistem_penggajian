<div class="col-lg-10">
    <div class="breadcrumb-wrapper">            
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/dashboard'); ?>">Home</a></li>
            <li class="active">Data Gaji</li>
        </ol>
    </div>         
    <div class="box">
        <div class="box-title">
            <a class='btn btn-primary conbtn_autocomplete fancybox.ajax pull-right col-sm-2' style="text-align:center;margin-top: 0px;padding: 8px;" data-toggle='tooltip' data-placement='top' title='Tambah Data Gaji' href='<?php echo base_url($urlnya . '/add_gaji'); ?>'>
                Tambah
            </a>
            <h2><?php echo $page_title; ?></h2>

        </div>
        <fieldset>
            <div class="form-group">
                <label class="col-md-1 control-label">Periode </label>
                <div class="col-md-3">
                    <select name="periode" id="periode" class="form-control">
                        <option value="0">--Pilih Periode--</option>
                        <?php
                        foreach ($bul as $row) {
                            ?>
                            <option value="<?php echo $row ?>"><?php echo $row; ?></option>
                        <?php }
                        ?>
                    </select>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="form-group">
                <label class="col-md-1 control-label">Divisi</label>
                <div class="col-md-3">                     
                    <select name="divisi" id="divisi" class="form-control m-b">
                        <option value="0">--Pilih Divisi--</option>
                        <?php foreach ($data_divisi as $row) { ?>
                            <option value="<?php echo $row->id ?>"><?php echo $row->nama_divisi ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </fieldset>
        <div class="box-inner" id="reload">
            <?php
            if ($this->session->flashdata('message')) {
                echo $this->session->flashdata('message');
            }
            ?>
            <div id="loads">
                <center>
                    <img id="img-loader" src="<?php echo base_url('assets/images/loading_big.gif'); ?>" style="margin-top: 5%">

                </center>
            </div>
            <div id="data"></div>
            <div id="data2"></div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js'); ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#divisi').on('change', function() {
            $(this).each(function(e) {
                var periode = $('#periode').val();
//                alert(periode);
//                alert(periode);
                var isi = $(this).val() + '_' + $('#periode').val();
//                alert(isi);
                console.log(isi);
                $.ajax({
                    url: '<?php echo base_url() ?>admin/gaji/lewat',
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.status_data === true) {
                            $("#data2").slideUp(500, function() {
                                var data = $("#data2").load('<?php echo base_url('admin/gaji/daftar_gaji_klik') ?>' + "/" + isi).hide();
                                $("#loads").slideDown(100, function() {
                                    $("#loads").fadeOut(1000, function() {
//                                        alert(data);
                                        $("#data2").load(data).slideDown(1000);
                                    });
                                });
                            });
                            $("#data").remove();
                        }
                    }, error: function() {
                        alert("Terjadi Kesalahan!");
                        location.reload();
                    }
                });
            });
        });

        $("#loads").slideDown(1000, function() {
            var home = $("#data").load('<?php echo base_url('admin/gaji/daftar_gaji'); ?>').hide();
            $("#loads").fadeOut(500, function() {
                $("#data").load(home).slideDown(1000);
            });
        });
    });
</script>