<style type="text/css">
    @media all {.page-break { display: block; page-break-before: always; size: landscape }}
    @page {size: 21.5cm 33cm ; margin-left: 2cm; margin-bottom: 2cm; margin-right: 2cm; margin-top: 2cm;}
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
    tbody {
        display:table-row-group;
    }
    .txt-l {text-align: left;}
    .txt-c {text-align: center;}
    .txt-r {text-align: right;}
    .txt-j {text-align: justify;}
    .p1 {line-height: 150%; font-size: 12px;}
    .table-bordered td { 
        padding: 5px;
        font-size: 12px;
    }
    .table {
        border-collapse: collapse !important;
    }
    .table td,
    .table th {
        background-color: #fff !important;
    }
    .tahoma10 {
        font-family: Tahoma;
        font-size: 10px;
        text-decoration: none;
    }
    .header20 {
        font-family: Tahoma;
        font-size: 20px;
        text-decoration: none;
        padding: 10px;
        margin: 10px;
    }
    .tahoma11 {
        font-family: Tahoma;
        font-size: 11px;
        text-decoration: none;
        margin: 2px;
        padding: 2px;
        border-top: 1px;
    }
    .tahoma12 {
        font-family: Tahoma;
        font-size: 12px;
        text-decoration: none;
        margin: 2px;
        padding: 2px;
        border-top: 1px;
    }
    .tahoma14 {
        font-family: Tahoma;
        font-size: 14px;
        text-decoration: none;
        margin: 2px;
        padding: 2px;
        border-top: 1px;
    }
    .tahoma16 {
        font-family: Tahoma;
        font-size: 16px;
        text-decoration: none;
    }
    .border-tlb {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-right-style: none !important;
        border-bottom-style: solid !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-lb {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: none !important;
        border-bottom-style: solid !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-lr {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: solid !important;
        border-bottom-style: none !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-r {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: solid !important;
        border-bottom-style: none !important;
        border-left-style: none !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-l {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: none !important;
        border-bottom-style: none !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-tlbr {
        border: 1.2px solid #000000 !important;
    }
    .border-lbr {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: solid !important;
        border-bottom-style: solid !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-tb {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-right-style: none !important;
        border-bottom-style: solid !important;
        border-left-style: none !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-tlb {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-bottom-style: solid !important;
        border-left-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-tbr {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-right-style: solid !important;
        border-bottom-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-tl {
        border-top-width: 1.2px !important;
        border-left-width: 1.2px !important;

        border-top-style: solid !important;
        border-left-style: solid !important;

        border-top-color: #000000 !important;
        border-left-color: #000000 !important;

    }    
    .border-br {
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-right-style: solid !important;
        border-bottom-style: solid !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
    }
    .border-tr {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-right-style: solid !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-b {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: none !important;
        border-bottom-style: solid !important;
        border-left-style: none !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .border-t {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: solid !important;
        border-right-style: none !important;
        border-bottom-style: none !important;
        border-left-style: none !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
    .style1 {
        font-weight: bold  !important;
    }
    .border-right{
        border-right:1.2px solid #000000 !important;
    }
    .border-missing {
        border-top-width: 1.2px !important;
        border-right-width: 1.2px !important;
        border-bottom-width: 1.2px !important;
        border-left-width: 1.2px !important;
        border-top-style: none !important;
        border-right-style: none !important;
        border-bottom-style: solid !important;
        border-left-style: none !important;
        border-top-color: #000000 !important;
        border-right-color: #000000 !important;
        border-bottom-color: #000000 !important;
        border-left-color: #000000 !important;
    }
</style>
<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div id="print-area-2" class="print-area" style="max-width: 1400px; min-height: 305px">

                    <table class="table table-bordered" cellspacing="0" width="100%">
                        <tr class="tahoma14">
                            <td class="border-tlbr border-tlb txt-c" width="25%"><img src="<?php echo base_url('assets'); ?>/images/logo.png" width="" height="40"></td>
                            <td class="border-tlb txt-c" width="60%" colspan="7">
                                <h5>
                                    <strong>SLIP GAJI
                                    </strong>
                                </h5>
                            </td>
                            <td class="border-tlbr txt-c" width="15%"><b><strong>Periode <?php echo $periode; ?></strong></td>
                        </tr>
                    </table><br>
                    <table class="table table-bordered" cellspacing="0" width="100%" style="margin-top: 20px !important">
                        <tr class="tahoma12">
                            <td height="17" class="border-tlb" colspan="3">
                                <div align="center">
                                    <strong>PENDAPATAN (+)</strong>
                                </div>
                            </td>
                            <td height="17" class="border-tlbr" colspan="3">
                                <div align="center">
                                    <strong>PENGELUARAN (-)</strong>
                                </div
                            </td>
                        </tr><br />
                        <tr align="center" class="tahoma12">
                            <td  class="border-lbr">No</td>
                            <td class="border-lbr">Keterangan</td>
                            <td class="border-lbr">Jumlah</td>
                            <td class="border-lbr">No</td>
                            <td class="border-lbr">Keterangan</td>
                            <td class="border-lbr">Jumlah</td>
                        </tr><br />
                        <?php
                        $data_gapok = $this->gaji_m->view('golongan_gaji', array('id_divisi' => $data_gaji[0]->id_divisi, 'id_jabatan' => $data_gaji[0]->id_jabatan));
                        $data_lembur = $this->gaji_m->view('lembur', array('id_karyawan' => $data_gaji[0]->id_karyawan, 'periode' => $periode));
                        $data_bonus = $this->gaji_m->view('bonus', array('id_karyawan' => $data_gaji[0]->id_karyawan, 'periode' => $periode));
                        $data_absensi = $this->gaji_m->view('absensi', array('id_karyawan' => $data_gaji[0]->id_karyawan, 'periode' => $periode));
                        $data_pinjam = $this->gaji_m->view('pinjaman', array('id_karyawan' => $data_gaji[0]->id_karyawan));

                        $total_pendapatan = @$data_gapok[0]->gaji_pokok + @$data_gaji[0]->tunjangan_proyek + @$data_bonus[0]->jumlah_bonus + @$data_lembur[0]->jumlah_bonus;
                        $angsuran = ($total_pendapatan - $data_gaji[0]->gaji_bersih) - @$data_absensi[0]->potongan;
//                                                $total_potongan1 = @$data_absensi[0]->potongan + @$angsuran;
                        $total_pengeluaran = @$angsuran + @$data_absensi[0]->potongan;
                        $gaber = @$total_pendapatan - @$total_pengeluaran;
                        ?>
                        <tr class="tahoma12">
                            <td align="center" class="border-tlb">1</td>
                            <td class="border-tlb">Gaji Pokok</td>
                            <td class="border-tlb" align="right"><?php echo format_rupiah($data_gapok[0]->gaji_pokok); ?></td>
                            <td class="border-tlb" align="center">1</td>
                            <td class="border-tlb">Potongan Absensi</td>
                            <td class="border-tlbr" align="right"><?php echo format_rupiah(@$data_absensi[0]->potongan); ?></td>
                        </tr>
                        <tr class="tahoma12">
                            <td class="border-lb" align="center">2</td>
                            <td class="border-lb">Tunjangan Proyek</td>
                            <td class="border-lb" align="right"><?php echo format_rupiah(@$data_gaji[0]->tunjangan_proyek); ?></td>
                            <td class="border-lb" align="center">2</td>
                            <td class="border-lb">Potongan Pinjam</td>
                            <td class="border-lbr" align="right"><?php echo format_rupiah(@$angsuran); ?></td>
                        </tr>
                        <tr class="tahoma12">
                            <td class="border-lb" align="center">3</td>
                            <td class="border-lb">Lembur</td>
                            <td class="border-lb" align="right"><?php echo format_rupiah(@$data_lembur[0]->jumlah_bonus); ?></td>
                            <td class="border-lb"></td>
                            <td class="border-lb"></td>
                            <td class="border-lbr" align="right"></td>
                        </tr>
                        <tr class="tahoma12">
                            <td class="border-lb" align="center">4</td>
                            <td class="border-lb">Jumlah Bonus</td>
                            <td class="border-lb" align="right"><?php echo format_rupiah(@$data_bonus[0]->jumlah_bonus); ?></td>
                            <td class="border-lb"></td>
                            <td class="border-lb"></td>
                            <td class="border-lbr" align="right"></td>
                        </tr>
                        <tr class="tahoma12">
                            <td class="border-lb" colspan="2"><b>Total Pendapatan</b></td>
                            <td class="border-lbr" align="right"><b><?php echo format_rupiah(@$total_pendapatan); ?></b></td>

                            <td class="border-lb" colspan="2"><b>Total Pengeluaran</b></td>
                            <td class="border-lbr" align="right"><b><?php echo format_rupiah(@$total_pengeluaran); ?></b></td>
                        </tr>
                        <tr class="tahoma12">
                            <td class="border-tlbr" colspan="2"><b><i><u>Total Gaji yang diterima</u></i></b></td>
                            <td class="border-tlbr" align="right"><b><?php echo format_rupiah($data_gaji[0]->gaji_bersih); ?></b></td>
                            <td class="border-lbr" colspan="3" align="right"><b></b></td>
                        </tr>
                        </tbody>
                    </table>
                    <br />
                    <table id="example" class="display table table-striped table-bordered"  cellspacing="0" width="100%">
                        <thead>
                            <tr align="center" class="tahoma12">
                                <td colspan="2"></td>
                                <td>Diserahkan oleh,</td>
                                <td>Diterima oleh, </td>
                            </tr>
                            <tr class="tahoma12">
                                <td colspan="2">
                                    NIK : <?php echo $data_gaji[0]->username; ?><br/>
                                    Nama Karyawan : <?php echo $data_gaji[0]->nama_karyawan; ?><br/>
                                    Divisi : <?php echo $data_gaji[0]->nama_divisi; ?><br/>
                                    Jabatan : <?php echo $data_gaji[0]->nama_jabatan; ?><br/>
                                </td>
                                <td align="center"><br /><br /><br /><?php echo $this->session->userdata['username']; ?>
                                </td></td>
                                <td align="center"><br /><br /><br /><?php echo $data_gaji[0]->nama_karyawan; ?>
                                </td>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>