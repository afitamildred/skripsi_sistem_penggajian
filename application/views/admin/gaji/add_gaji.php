<div>
    <div class="box-title">
        <h3>Tambah Data Gaji</h3> 
    </div>
    <div class="box-inner form-horizontal">
        <?php echo form_open($urlnya . '/simpan', array('id' => 'form-tambah', 'data-reload' => base_url($urlnya . '/index?data=reload'))); ?>
        <table>
            <tr>
                <td>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Periode </label>
                            <div class="col-md-8">
                                <select name="periode2" id="periode2" class="form-control">
                                    <?php
                                    $i = 1;
                                    foreach ($bul as $row) {
                                        if ($bulan == $i) {
                                            ?>
                                            <option value="<?php echo $row ?>" selected=""><?php echo $row; ?></option>
                                        <?php } else {
                                            ?>
                                            <option value="<?php echo $row ?>"><?php echo $row; ?></option>

                                            <?php
                                        }$i++;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Username</label>
                            <div class="col-md-8">   
                                <select name="username" id="username" class="form-control m-b">
                                    <option value="">--Select Username Karyawan--</option>
                                    <?php foreach ($username_karyawan as $row) { ?>
                                        <option value="<?php echo $row->username ?>"><?php echo $row->username . '  ||  ' . $row->nama ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nama</label>
                            <div class="col-md-8">
                                <?php echo form_input($nama); ?>
                            </div>
                        </div>
                    </fieldset>                    
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Divisi</label>
                            <div class="col-md-8">                          
                                <?php echo form_input($div); ?>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Gaji Pokok</label>
                            <div class="col-md-8">
                                <?php echo form_input($gaji_pokok); ?>      
                                <input type="hidden" name="gaji_pokok_unNum" value="" id="gaji_pokok_unNum" class="form-control">
                            </div>
                        </div>
                    </fieldset> 
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Tunjangan Proyek</label>
                            <div class="col-md-8">
                                <?php echo form_input($tunjangan_proyek); ?>       
                                <input type="hidden" name="tunjangan_proyek_unNum" value="" id="tunjangan_proyek_unNum" class="form-control">
                            </div>
                        </div>
                    </fieldset> 

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Lembur</label>
                            <div class="col-md-8">
                                <?php echo form_input($lembur); ?>                                    
                                <input type="hidden" name="lembur_unNum" value="" id="lembur_unNum" class="form-control">

                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Jumlah Bonus</label>
                            <div class="col-md-8">
                                <?php echo form_input($jml_bonus); ?>                    
                                <input type="hidden" name="jml_bonus_unNum" value="" id="jml_bonus_unNum" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label"><b>Jumlah Pendapatan</b></label>
                            <div class="col-md-8">
                                <b><?php echo form_input($penambahan); ?>  </b>     
                                <input type="hidden" name="penambahan_unNum" value="" id="penambahan_unNum" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                </td>
                <td valign="top">
                    <!--<div class="clearfix"></div>-->
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Potongan Absensi</label>
                            <div class="col-md-8">
                                <?php echo form_input($potongan_absensi); ?>       
                                <input type="hidden" name="potongan_absensi_unNum" value="" id="potongan_absensi_unNum" class="form-control">

                            </div>
                        </div>
                    </fieldset>

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Pinjaman</label>
                            <div class="col-md-8">
                                <?php echo form_input($pinjaman); ?>    
                                <input type="hidden" name="pinjaman_unNum" value="" id="pinjaman_unNum" class="form-control" >     
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Angsuran Pinjaman</label>
                            <div class="col-md-2">
                                <input type="text" name="persen_bayar" value="" id="persen_bayar" class="form-control" >     
                            </div>
                            <label class="col-md-1 control-label">%</label>
                            <div class="col-md-5">
                                <input type="text" name="total_angsuran" value="" id="total_angsuran" class="form-control" readonly="">     
                                <input type="hidden" name="total_angsuran_unNum" value="" id="total_angsuran_unNum" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Sisa Pinjam</label>
                            <div class="col-md-8">
                                <input type="text" name="sisa_pinjam" value="" id="sisa_pinjam" class="form-control" readonly="">     
                                <input type="hidden" name="sisa_pinjam_unNum" value="" id="sisa_pinjam_unNum" class="form-control" readonly="">     
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label"><b>Jumlah Pengeluaran</label>
                            <div class="col-md-8">
                                <?php echo form_input($pengurangan); ?>       
                                <input type="hidden" name="pengurangan_unNum" value="" id="pengurangan_unNum" class="form-control">  

                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label"><b>Gaji Bersih</label>
                            <div class="col-md-8">
                                <?php echo form_input($gaji_bersih); ?>  
                                <input type="hidden" name="gaji_bersih_unNum" value="" id="gaji_bersih_unNum" class="form-control">

                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-8">
                                <?php echo form_submit($submitin); ?>
                            </div>
                        </div>
                    </fieldset>
                    <?php echo form_close(); ?>
                </td>
            </tr>
        </table>
    </div>

</div>
<script src="<?php echo base_url('assets/js/jquery-1.7.js') ?>" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        function convertToRupiah(angka) {
            var rupiah = '';
            var angkarev = angka.toString().split('').reverse().join('');
            for (var i = 0; i < angkarev.length; i++)
                if (i % 3 === 0)
                    rupiah += angkarev.substr(i, 3) + '.';
            return rupiah.split('', rupiah.length - 1).reverse().join('');
        }
        $("#submitin").on('click', function() {
            if ($('#username').val() == '') {
                alert('mohon pilih username terlebih dahulu!');
                return false;
                if ($('#potongan_absensi').val() == 0) {
                    alert('mohon inputkan data absensi terlebih dahulu!');
                    return false;
                } else {
                    return true;
                }
            } else {
                if ($('#potongan_absensi').val() == 0) {
                    alert('mohon inputkan data absensi terlebih dahulu!');
                    return false;
                } else {
                    return true;
                }

                return true;
            }

        });
        $("#username").change(function() {
            if ($('#username').val() == '') {
                alert('mohon pilih username yang benar!');
                $("#nama").val('');
                $("#div").val('');
                $("#pengurangan").val(0);
                $("#penambahan").val(0);
                $("#gaji_pokok").val(0);
                $("#tunjangan_proyek").val(0);
                $("#jml_bonus").val(0);
                $("#lembur").val(0);
                $("#pinjaman").val(0);
                $("#potongan_absensi").val(0);
                $("#gaji_bersih").val(0);
            } else {
                $(this).each(function(e) {
                    var periode = $('#periode2').val();
//                alert(periode);
                    var username = $('#username').val();
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url($urlnya); ?>/get_data_karyawan/" + periode,
                        data: "username=" + username,
                        success: function(data) {
                            //            alert(data);
                            var pecah = data.split(',');
                            var nama_karyawan = pecah[0];
//                        alert(data);
                            $("#nama").val(nama_karyawan);
                            var divisi = pecah[1];
                            $("#div").val(divisi);
//                    var jabatan = pecah[2];
                            //                    $("#jabatan").val(jabatan);
                            var jml_bonus = parseInt(pecah[3]);
                            var jml_pinjam = parseInt(pecah[4]);
                            var jml_potongan_absensi = parseInt(pecah[5]);
                            var bonus_lembur = parseInt(pecah[6]);
                            var gapok = parseInt(pecah[8]);
                            var tunjangan_proyek = parseInt(pecah[7]);
                            var pengurangan = jml_potongan_absensi;
                            var penambahan = parseInt(gapok + bonus_lembur + jml_bonus + tunjangan_proyek);
                            var gaber = parseInt(penambahan - pengurangan);
                            $("#pengurangan").val(convertToRupiah(pengurangan));
                            $("#pengurangan_unNum").val(pengurangan);
                            $("#penambahan").val(convertToRupiah(penambahan));
                            $("#penambahan_unNum").val(penambahan);
                            $("#gaji_pokok").val(convertToRupiah(gapok));
                            $("#gaji_pokok_unNum").val(gapok);
                            $("#tunjangan_proyek").val(convertToRupiah(tunjangan_proyek));
                            $("#tunjangan_proyek_unNum").val(tunjangan_proyek);
                            $("#jml_bonus").val(convertToRupiah(jml_bonus));
                            $("#jml_bonus_unNum").val(jml_bonus);
                            $("#pinjaman").val(convertToRupiah(jml_pinjam));
                            $("#pinjaman_unNum").val(jml_pinjam);
                            $("#potongan_absensi").val(convertToRupiah(jml_potongan_absensi));
                            $("#potongan_absensi_unNum").val(jml_potongan_absensi);
                            $("#lembur").val(convertToRupiah(bonus_lembur));
                            $("#lembur_unNum").val(bonus_lembur);
                            $("#gaji_bersih").val(convertToRupiah(gaber));
                            $("#gaji_bersih_unNum").val(gaber);
                        }
                    });
                });
            }

            $('#persen_bayar').keyup(function() {
                var persen_bayar = $(this).val();
                if (persen_bayar > 100) {
                    alert('Maksimal 100%');
                    $(this).val(0);
                    $("#sisa_pinjam").val(0);
                    $("#total_angsuran").val(0);
                    $("#gaji_bersih").val();
                } else {
                    var jml_pinjam = parseInt($("#pinjaman_unNum").val());
                    var hitung = (persen_bayar / 100) * jml_pinjam;
                    var sisa_pinjam = jml_pinjam - hitung;
                    $("#sisa_pinjam").val(convertToRupiah(sisa_pinjam));
                    $("#sisa_pinjam_unNum").val(sisa_pinjam);
                    $("#total_angsuran").val(convertToRupiah(hitung));
                    $("#total_angsuran_unNum").val(hitung);
                    var pengurangan = parseInt($("#potongan_absensi_unNum").val()) + hitung;
                    $("#pengurangan").val(convertToRupiah(pengurangan));
                    $("#pengurangan_unNum").val(pengurangan);
                    var gaber = $("#penambahan_unNum").val() - pengurangan;
                    //            alert(gaber);
                    $("#gaji_bersih").val(convertToRupiah(gaber));
                    $("#gaji_bersih_unNum").val(gaber);
                }

            });
        });
    });
</script>
