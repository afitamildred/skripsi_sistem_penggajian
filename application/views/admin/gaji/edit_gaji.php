<script src="<?php echo base_url('assets/js/jquery-1.7.js') ?>" type="text/javascript"></script>
<script>
    function convertToRupiah(angka) {
        var rupiah = '';
        var angkarev = angka.toString().split('').reverse().join('');
        for (var i = 0; i < angkarev.length; i++)
            if (i % 3 === 0)
                rupiah += angkarev.substr(i, 3) + '.';
        return rupiah.split('', rupiah.length - 1).reverse().join('');
    }
    $(document).ready(function() {
        var gapok = $("#gaji_pokok").val();
        $("#gaji_pokok").val(convertToRupiah(gapok));
        var tunjangan_proyek = $("#tunjangan_proyek").val();
        $("#tunjangan_proyek").val(convertToRupiah(tunjangan_proyek));
        var lembur = $("#lembur").val();
        $("#lembur").val(convertToRupiah(lembur));
        var jumlah_bonus = $("#jumlah_bonus").val();
        $("#jumlah_bonus").val(convertToRupiah(jumlah_bonus));
        var penambahan = $("#penambahan").val();
        $("#penambahan").val(convertToRupiah(penambahan));
        var pot_absen = parseInt($("#potongan_absensi").val());
//        var penambahan = parseInt(gaji_pokok + tunj_proyek + jml_bonus + lembur);
//        var pengurangan = parseInt(pot_absen);
//        $("#pengurangan").val(pengurangan);
//        $("#penambahan").val(penambahan);
        var total_angsuran = parseInt($("#total_angsuran").val());
        var pinjaman = parseInt($("#pinjaman").val());
        var persen = total_angsuran / pinjaman * 100;
        $("#persen_bayar").val(persen);
        $('#persen_bayar').keyup(function() {
            var persen_bayar = $(this).val();
            var jml_pinjam = parseInt($("#pinjaman").val());
            var hitung = (persen_bayar / 100) * jml_pinjam;
            var sisa_pinjam = jml_pinjam - hitung;

            $("#sisa_pinjam").val(sisa_pinjam);
            $("#total_angsuran").val(hitung);
            var pengurangan = parseInt($("#potongan_absensi").val()) + hitung;
            $("#pengurangan").val(pengurangan);
            var gaber = $("#penambahan_unNum").val() - pengurangan;
            $("#gaji_bersih").val(gaber);
        });
    });
</script>
<div style="min-width:600px;">
    <div class="box-title">
        <h3>Edit Data Gaji</h3> 
    </div>
    <div class="box-inner form-horizontal">
         <?php echo form_open($urlnya . '/simpan_edit', array('id' => 'form-tambah', 'data-reload' => base_url($urlnya . '/index?data=reload'))); ?>
        <?php echo form_input($id); ?>
        <?php echo form_input($username); ?>
        <table>
            <tr>
                <td>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama</label>
                            <div class="col-md-9">
                                <?php echo form_input($nama); ?>
                            </div>
                        </div>
                    </fieldset>                    
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Divisi</label>
                            <div class="col-md-9">                          
                                <?php echo form_input($divisi); ?>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Jabatan</label>
                            <div class="col-md-9">                          
                                <?php echo form_input($jabatan); ?>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Gaji Pokok</label>
                            <div class="col-md-9">
                                <?php echo form_input($gaji_pokok); ?>           
                            </div>
                        </div>
                    </fieldset> 

                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tunjangan Proyek</label>
                            <div class="col-md-9">
                                <?php echo form_input($tunjangan_proyek); ?>       
                            </div>
                        </div>
                    </fieldset> 
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Lembur</label>
                            <div class="col-md-9">
                                <?php echo form_input($lembur); ?>                  
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Jumlah Bonus</label>
                            <div class="col-md-9">
                                <?php echo form_input($jumlah_bonus); ?>          
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><b>Pendapatan</b></label>
                            <div class="col-md-9">
                                <b><?php echo form_input($penambahan); ?>  </b> 
                                <b><?php echo form_input($penambahan_unNum); ?>  </b> 
                            </div>
                        </div>
                    </fieldset>
                </td>
                <td>
<!--                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Periode </label>
                            <div class="col-md-9">
                                <select name="periode" id="periode" class="form-control">
                                    <?php
                                    foreach ($bul as $row) {
                                        if ($row == $periode) {
                                            ?>
                                            <option value="<?php echo $row ?>" selected=""><?php echo $row; ?></option>
                                        <?php } else {
                                            ?>
                                            <option value="<?php echo $row ?>"><?php echo $row; ?></option>

                                            <?php
                                        }$i++;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </fieldset>-->
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Potongan Absensi</label>
                            <div class="col-md-9">
                                <?php echo form_input($potongan_absensi); ?>       
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Pinjaman</label>
                            <div class="col-md-9">
                                <?php echo form_input($pinjaman); ?>                                    
                                <div class="error_popups" id="error-no-sk"></div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Angsuran Pinjaman</label>
                            <div class="col-md-2">
                                <input type="text" name="persen_bayar" value="" id="persen_bayar" class="form-control" >     
                            </div>
                            <label class="col-md-1 control-label">%</label>
                            <div class="col-md-6">
                                <input type="text" name="total_angsuran" value="<?php echo $gaji[0]->angsuran_pinjam; ?>" id="total_angsuran" class="form-control" readonly="">     
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Sisa Pinjam</label>
                            <div class="col-md-9">
                                <input type="text" name="sisa_pinjam" value="<?php echo $gaji[0]->sisa_pinjam; ?>" id="sisa_pinjam" class="form-control" readonly="">     
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><b>Pengeluaran</label>
                            <div class="col-md-9">
                                <?php echo form_input($pengurangan); ?>          
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><b>Gaji Bersih</label>
                            <div class="col-md-9">
                                <?php echo form_input($gaji_bersih); ?>            
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-2 control-label"></label>
                            <div class="col-md-10">
                                <?php echo form_submit($submitin); ?>
                            </div>
                        </div>
                    </fieldset>
                </td>
            </tr>
        </table>


        <?php echo form_close(); ?>
    </div>
</div>