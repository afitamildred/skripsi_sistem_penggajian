<fieldset>
    <div class="form-group">
        <div class="col-md-2 pull-right" style="padding: 0px !important; text-align: right">
            <a class='btn btn-success conbtn fancybox.ajax' align="right" data-toggle='tooltip' data-placement='top' title='Cetak Gaji Karyawan' href='<?php echo base_url($urlnya . '/cetak_gaji/' . $isi); ?>'>Cetak Gaji Karyawan</a>
        </div>
    </div>
</fieldset>
<div class="table-responsive">
    <table class="table table-striped table-bordered" id="table" width="100%">
        <thead>
            <tr class="info">
                <th class="text-center" width="2%">No.</th>
                <th class="text-center" width="9%">Nama Karyawan</th>
                <th class="text-center" width="8%">Divisi</th>
                <th class="text-center" width="7%">Gaji Pokok</th>
                <th class="text-center" width="7%">Jumlah Lembur</th>
                <th class="text-center" width="7%">Tunjangan Proyek</th>
                <th class="text-center" width="7%">Jumlah Bonus</th>
                <th class="text-center" width="8%">Gaji Kotor</th>
                <th class="text-center" width="8%">Angsuran Pinjaman</th>
                <th class="text-center" width="8%">Potongan Absensi</th>
                <th class="text-center" width="8%">Total Potongan</th>
                <th class="text-center" width="8%">Gaji Bersih</th>
                <th class="text-center" width="13%">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            $datak = count($data_gaji);
            if ($datak == 0) {
                echo '<tr><td colspan="13" align="center">Maaf data tidak ada</td>
                    </tr>';
            } else {
                foreach ($data_gaji as $row) {
                    $data_gapok = $this->gaji_m->view('golongan_gaji', array('id_divisi' => $row->id_divisi, 'id_jabatan' => $row->id_jabatan));
                    //penambahan
                    $data_lembur = $this->gaji_m->view('lembur', array('id_karyawan' => $row->id_karyawan, 'periode' => $row->periode));
                    $data_bonus = $this->gaji_m->view('bonus', array('id_karyawan' => $row->id_karyawan, 'periode' => $row->periode));
                    $gaji_kotor = @$data_gapok[0]->gaji_pokok + @$data_lembur[0]->jumlah_bonus + @$row->tunjangan_proyek + @$data_bonus[0]->jumlah_bonus;
                    //pengurangan
                    $data_absensi = $this->gaji_m->view('absensi', array('id_karyawan' => $row->id_karyawan, 'periode' => $row->periode));
                    $data_pinjaman = $this->gaji_m->view('pinjaman', array('id_karyawan' => $row->id_karyawan));


                    $angsuran = ($gaji_kotor - $row->gaji_bersih) - @$data_absensi[0]->potongan;
                    $total_potongan = @$data_absensi[0]->potongan + @$angsuran;
                    ?> 
                    <tr>
                        <td class='text-center'><?php echo $no; ?></td>
                        <td><?php echo $row->nama_karyawan; ?></td>
                        <td><?php echo $row->nama_divisi; ?></td>
                        <td class="border-lb" align="right"><?php echo format_rupiah(@$data_gapok[0]->gaji_pokok); ?></td>
                        <td align="right"><?php echo format_rupiah(@$data_lembur[0]->jumlah_bonus); ?></td>
                        <td class="border-lb" align="right"><?php echo format_rupiah(@$row->tunjangan_proyek); ?></td>
                        <td align="right"><?php echo format_rupiah(@$data_bonus[0]->jumlah_bonus); ?></td>
                        <td align="right"><b><i><?php echo format_rupiah(@$gaji_kotor); ?></i></b></td>
                        <td align="right"><?php echo format_rupiah(@$angsuran); ?></td>
                        <td align="right"><?php echo format_rupiah(@$data_absensi[0]->potongan); ?></td>
                        <td align="right"><b><i><?php echo format_rupiah(@$total_potongan); ?></i></b></td>
                        <td align="right"><b><u><?php echo format_rupiah(@$row->gaji_bersih); ?></u></b></td>
                        <td class='text-center'>
                            <a class='btn btn-warning conbtn fancybox.ajax' data-toggle='tooltip' data-placement='top' title='Edit' href='<?php echo base_url($urlnya . '/edit/' . $row->id); ?>'><i class="fa fa-pencil"></i></a>
                            <a class='btn btn-primary conbtn fancybox.ajax' data-toggle='tooltip' data-placement='top' title='Cetak Slip Gaji' href='<?php echo base_url($urlnya . '/cetak_slip_gaji/' . $row->id); ?>'><i class="fa fa-list-ol"></i></a>
                            <a class='btn btn-danger conbtn fancybox.ajax' data-toggle='tooltip' data-placement='top' title='Hapus' href='<?php echo base_url($urlnya . '/validasi_hapus/' . $row->id); ?>'><i class="fa fa-trash"></i></a>
                        </td>
                        <?php
                        $no++;
                    }
                }
                ?>
        </tbody>
    </table>
    <div id="pages">
        <ul class="pagination">

        </ul>
    </div>
</div>