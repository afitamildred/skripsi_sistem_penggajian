<div class="col-lg-10">
    <div class="breadcrumb-wrapper">            
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/dashboard'); ?>">Home</a></li>
            <li class="active">Biodata</li>
        </ol>
    </div>         
    <div class="box">
        <div class="box-title">
            <h2><?php echo $page_title; ?></h2>

        </div>

        <div class="clear"></div>
        <div class="box-inner" id="reload">
            <?php
            $flash_success_message = $this->session->flashdata('success_message');
            echo!empty($flash_success_message) ? '<div id="alert" class="alert alert-success">' . $flash_success_message . '</div>' : '';
            ?>
            <?php echo $this->load->view('alert_class.php'); ?>
            <div class="table-responsive">
                <table class="display table table-striped table-bordered" id="table">
                    <thead>
                        <tr class="info">
                            <th class="center-x">No.</th>
                            <th class="center-x">Periode</th>
                            <th class="center-x">Gaji Pokok</th>
                            <th class="center-x">Jumlah Lembur</th>
                            <th class="center-x">Tunjangan Proyek</th>
                            <th class="center-x">Jumlah Bonus</th>
                            <th class="center-x">Angsuran Pinjaman</th>
                            <th class="center-x">Potongan Absensi</th>
                            <th class="center-x">Gaji Bersih</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data_gaji as $row) {
//                            $jumlah_hadir = $row->jumlah_harus_hadir - $row->jumlah_hadir;
                            $data_gapok = $this->rincian_gaji_m->view('golongan_gaji', array('id_divisi' => $row->id_divisi, 'id_jabatan' => $row->id_jabatan));
                            $data_lembur = $this->rincian_gaji_m->view('lembur', array('id_karyawan' => $row->id_karyawan, 'periode' => $row->periode));
                            $data_bonus = $this->rincian_gaji_m->view('bonus', array('id_karyawan' => $row->id_karyawan, 'periode' => $row->periode));
                            $data_absensi = $this->rincian_gaji_m->view('absensi', array('id_karyawan' => $row->id_karyawan, 'periode' => $row->periode));
                            $angsuran = ($data_bonus[0]->jumlah_bonus +$row->tunjangan_proyek + $data_lembur[0]->jumlah_bonus + $data_gapok[0]->gaji_pokok)-$row->gaji_bersih - $data_absensi[0]->potongan;
                            ?> 
                            <tr>
                                <td class='center-x'><?php echo $no; ?></td>
                                <td class='center-x'><?php echo $row->periode; ?></td>
                                <td class="border-lb" align="right"><?php echo format_rupiah($data_gapok[0]->gaji_pokok); ?></td>
                                <td align="right"><?php echo format_rupiah($data_lembur[0]->jumlah_bonus); ?></td>
                                <td class="border-lb" align="right"><?php echo format_rupiah($row->tunjangan_proyek); ?></td>
                                <td align="right"><?php echo format_rupiah($data_bonus[0]->jumlah_bonus); ?></td>
                                <td align="right"><?php echo format_rupiah($angsuran); ?></td>
                                <!--<td><?php // echo $jumlah_hadir;       ?></td>-->
                                <td align="right"><?php echo format_rupiah($data_absensi[0]->potongan); ?></td>
                                <td align="right"><?php echo format_rupiah($row->gaji_bersih); ?></td>

                                <?php
                                $no++;
                            }
                            ?>
                    </tbody>
                </table>
                <div id="pages">
                    <ul class="pagination">

                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
