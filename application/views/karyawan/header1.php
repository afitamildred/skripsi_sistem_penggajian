<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="LPTI Pelataran Mataram">
        <title>LPTI Pelataran Mataram</title>
        <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet"> 
        <link href="<?php echo base_url('assets/css/form.css'); ?>" rel="stylesheet">         
        <link href="<?php echo base_url('assets/css/font-awesome.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/hover.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/awesome-bootstrap-checkbox.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/dataTables.bootstrap.css'); ?>" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,700,400,600' rel='stylesheet' type='text/css'>
        <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/jquery.datetimepicker.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/summernote.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/fancybox'); ?>/source/jquery.fancybox.css?v=2.1.5" rel="stylesheet" type="text/css" media="screen" />                   
        <link href="<?php echo base_url('assets/css/alert/twitter.css'); ?>" rel="stylesheet" type="text/css"><!--alert-->
        <!--<link href="<?php // echo base_url('assets/images/icon_url.jpg');     ?>" >-->
        <!--<link href="<?php // echo base_url('assets/images/icon_url.jpg');     ?>" rel="icon" type="image/ico" href="favicon.ico">-->
    </head>

    <body>
        <header role="banner" id="top" class="header navbar navbar-fixed-top">
            <a class="logout btn" href="#"><span class="fa fa-sign-out"></span>Logout</a>
            <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    <img class="img-responsive" src="<?php echo base_url('assets/images/logo.png'); ?>">
                </a>
            </div>
            <nav class="navbar-collapse bs-navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
                <ul class="nav navbar-nav top-nav">
                    <li <?php if ($this->uri->segment(2) == 'dashboard') echo 'class="active"'; ?>><a class="hvr-underline-reveal" href="<?php echo base_url('karyawan/dashboard'); ?>"><i class="fa fa-home"></i>Home</a></li>
                    <li><a class="hvr-underline-reveal conbtn fancybox.ajax" href="<?php echo base_url('login/ubah_password'); ?>"><i class="fa fa-user"></i>Ubah Password</a></li>

                </ul>
                <ul class="nav navbar-nav navbar-right user">
                    <li class="dropdown hvr-shadow">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">               
                            <!--                            <div class="profile-thumb">
                                                            <img src="<?php echo base_url('assets/uploads/avatar/' . (isset($avatar) ? $avatar : 'default.png') . ''); ?>">
                                                        </div>-->
                            <div href="<?php echo base_url('login/edit_profil'); ?>" class="profile-detail conbtn fancybox.ajax" data-toggle="tooltip" data-placement="left" title="Edit Akun">
                                <div class="profile-name">
                                    <?php
                                    if (isset($user)) {
                                        echo $user;
                                    }
                                    ?>
                                </div>
                                <div class="profile-nip">Username: 
                                    <?php
                                    if (isset($nip)) {
                                        echo $nip;
                                    }
                                    ?>
                                </div>
                            </div>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a class="hvr-underline-reveal" href="<?php echo base_url('login/logout/'); ?>">
                                    <div class="menu-icon"><i class="fa fa-sign-out"></i> Logout</div>
                                </a>            
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </header>

        <div id="page">
            <div id="main-sidebar">                
                <ul class="sidebar-nav">
                    <li><a href="<?php echo base_url('karyawan/biodata'); ?>"><i class="fa fa-credit-card"></i> Data Pribadi</a></li>
                    <li><a href="<?php echo base_url('karyawan/rincian_gaji'); ?>"><i class="fa fa-credit-card"></i> Rincian Gaji</a></li>

<!--                    <li><a href="<?php echo base_url('admin/golongan_gaji'); ?>"><i class="fa fa-credit-card"></i> Golongan Gaji</a></li>
                    <li><a href="<?php // echo base_url('admin/golongan_lembur');     ?>"><i class="fa fa-credit-card"></i> Golongan Lembur</a></li>
                    <li><a href="<?php echo base_url('admin/tunjangan_proyek'); ?>"><i class="fa fa-credit-card"></i> Tunjangan Proyek</a></li>
                    <li><a href="<?php echo base_url('admin/karyawan'); ?>"><i class="fa fa-credit-card"></i> Karyawan</a></li>
                    <li><a href="<?php echo base_url('admin/absensi'); ?>"><i class="fa fa-credit-card"></i> Absensi</a></li>
                    <li><a href="<?php echo base_url('admin/bonus'); ?>"><i class="fa fa-credit-card"></i> Bonus</a></li>
                    <li><a href="<?php echo base_url('admin/lembur'); ?>"><i class="fa fa-credit-card"></i> Lembur</a></li>
                    <li><a href="<?php echo base_url('admin/pinjaman'); ?>"><i class="fa fa-credit-card"></i> Pinjaman</a></li>
                    <li><a href="<?php echo base_url('admin/gaji'); ?>"><i class="fa fa-credit-card"></i> Gaji</a></li>-->

                </ul>

            </div>