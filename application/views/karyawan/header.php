<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="LPTI Pelataran Mataram">
        <title>LPTI Pelataran Mataram</title>
        <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">         
        <link href="<?php echo base_url('assets/css/font-awesome.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/hover.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/dataTables.bootstrap.css'); ?>" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,700,400,600' rel='stylesheet' type='text/css'>
        <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/jquery.datetimepicker.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/fancybox'); ?>/source/jquery.fancybox.css?v=2.1.5" rel="stylesheet" type="text/css" media="screen" />                   
    </head>

    <body>
        <header class="header navbar navbar-fixed-top" role="banner">
            <div class="brand">
                <!-- Logo -->
                <a class="navbar-brand" href="<?php echo base_url('karyawan/dashboard'); ?>">
                    <img class="img-responsive" src="<?php echo base_url('assets/images/logo.png'); ?>">
                </a>
            </div>
            <ul class="nav navbar-nav navbar-left">
                <li <?php if ($this->uri->segment(2) == 'dashboard') echo 'class="active"'; ?>><a class="hvr-underline-reveal" href="<?php echo base_url('karyawan/dashboard'); ?>"> <i class="fa fa-home"></i>Home</a></li>
                <li><a class="hvr-underline-reveal conbtn fancybox.ajax" href="<?php echo base_url('login/ubah_password'); ?>"><i class="fa fa-user"></i> Ubah Password</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right user">
                <li class="dropdown hvr-underline-reveal">
                    <a href="<?php echo base_url('login/logout'); ?>" class="hvr-underline-reveal"><i class="fa fa-power-off hvr-underline-reveal"></i> Log Out</a>
                </li>
            </ul>
        </header>
        <div class="container-fluid"></div>
        <div class="col-lg-2" style="padding-right: 15px ">             
            <ul class="sidebar-nav">
                <li><a href="<?php echo base_url('karyawan/biodata'); ?>"><i class="fa fa-credit-card"></i> Data Pribadi</a></li>
                <li><a href="<?php echo base_url('karyawan/rincian_gaji'); ?>"><i class="fa fa-credit-card"></i> Rincian Gaji</a></li>
            </ul>
        </div>