 
<div class="col-lg-10">
    <div class="breadcrumb-wrapper">            
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/dashboard'); ?>">Home</a></li>
            <li class="active">Biodata</li>
        </ol>
    </div>         
    <div class="box">
        <div class="box-title">
            <h2><?php echo $page_title; ?></h2>

        </div>

        <div class="clear"></div>
        <div class="box-inner" id="reload">
            <div class="table-responsive">
                <form action="<?php echo base_url($urlnya . '/simpan/') ?>" method="POST">
                    <table class="display table table-striped table-bordered" id="table">
                        <tbody>
                        <div class="box-inner form-horizontal">
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Username</label>
                                    <div class="col-md-9">
                                        <?php echo form_input($username); ?>   
                                    </div>
                                </div>
                            </fieldset> 
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nama Lengkap</label>
                                    <div class="col-md-9">
                                        <?php echo form_input($nama); ?>   
                                    </div>
                                </div>
                            </fieldset>  
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Status Karyawan</label>
                                    <div class="col-md-9">
                                        <?php echo form_input($status_karyawan); ?>   
                                    </div>
                                </div>
                            </fieldset>                  
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Divisi</label>
                                    <div class="col-md-9">
                                        <?php echo form_input($divisi); ?>   
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Jabatan</label>
                                    <div class="col-md-9">
                                        <?php echo form_input($jabatan); ?>   
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">No KTP</label>
                                    <div class="col-md-9">
                                        <?php echo form_input($no_ktp); ?>                        
                                        <?php echo form_error('no_ktp'); ?>  
                                    </div>
                                </div>
                            </fieldset>  
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tanggal Menjabat</label>
                                    <div class="col-md-9">
                                        <?php echo form_input($tanggal_masuk); ?> 
                                    </div>
                                </div>
                            </fieldset> 
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tanggal Berakhir</label>
                                    <div class="col-md-9">
                                        <?php echo form_input($tanggal_akhir_kontrak); ?>                                    
                                        <div class="error_popups" id="error-no-sk"></div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Jenis Kelamin</label>
                                    <div class="col-md-9">
                                        <select name="jenis_kelamin" id="jenis_kelamin" class="form-control m-b">
                                            <option value="">--Pilih Jenis Kelamin--</option>
                                            <option value="Laki-laki" 
                                            <?php
                                            if ($jenis_kelamin == 'Laki-laki') {
                                                echo 'selected';
                                            };
                                            ?>>Laki-laki</option>
                                            <option value="Perempuan"
                                            <?php
                                            if ($jenis_kelamin == 'Perempuan') {
                                                echo 'selected';
                                            };
                                            ?>>Perempuan</option>
                                        </select>


                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Golongan Darah</label>
                                    <div class="col-md-9">
                                        <select name="golongan_darah" id="golongan_darah" class="form-control m-b">
                                            <option value="">--Pilih Golongan Darah--</option>
                                            <option value="A" 
                                            <?php
                                            if ($golongan_darah == 'A') {
                                                echo 'selected';
                                            };
                                            ?>>A</option>
                                            <option value="B"
                                            <?php
                                            if ($golongan_darah == 'B') {
                                                echo 'selected';
                                            };
                                            ?>>B</option>
                                            <option value="AB"
                                            <?php
                                            if ($golongan_darah == 'AB') {
                                                echo 'selected';
                                            };
                                            ?>>AB</option>
                                            <option value="O"
                                            <?php
                                            if ($golongan_darah == 'O') {
                                                echo 'selected';
                                            };
                                            ?>>O</option>
                                        </select>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"> Agama</label>
                                    <div class="col-md-9">
                                        <select name="agama" id="agama" class="form-control m-b">
                                            <option value="">--Pilih Agama--</option>
                                            <option value="Islam" 
                                            <?php
                                            if ($agama == 'Islam') {
                                                echo 'selected';
                                            };
                                            ?>>Islam</option>
                                            <option value="Kristen"
                                            <?php
                                            if ($agama == 'Kristen') {
                                                echo 'selected';
                                            };
                                            ?>>Kristen</option>
                                            <option value="Hindu"
                                            <?php
                                            if ($agama == 'Hindu') {
                                                echo 'selected';
                                            };
                                            ?>>Hindu</option>
                                            <option value="Budha"
                                            <?php
                                            if ($agama == 'Budha') {
                                                echo 'selected';
                                            };
                                            ?>>Budha</option>
                                        </select>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Alamat Asal</label>
                                    <div class="col-md-9">
                                        <?php echo form_input($alamat_asal); ?>                                    
                                        <div class="error_popups" id="error-no-sk"></div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Alamat Tinggal</label>
                                    <div class="col-md-9">
                                        <?php echo form_input($alamat_tinggal); ?>                                    
                                        <div class="error_popups" id="error-no-sk"></div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">No HP</label>
                                    <div class="col-md-9">
                                        <?php echo form_input($no_hp); ?>          
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tanggal Lahir</label>
                                    <div class="col-md-9">
                                        <?php echo form_input($tanggal_lahir); ?>          
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">
                                        <?php echo form_input($email); ?>          
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-10">
                                        <?php echo form_submit($submitin); ?>
                                    </div>
                                </div>
                            </fieldset>
                            <?php // echo form_close(); ?>
                        </div>

                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
if (isset($scripts)) {
    foreach ($scripts as $script) {
        echo $script;
    }
}
?>
<script>
    jQuery(function($) {
        var anc = {};
        $('.datetimepicker').datetimepicker({
            lang: 'id',
            timepicker: false,
            format: 'Y-m-d'
        });
    });


    $(document).ready(function() {
        $("#jenis_kelamin").on('change', function() {
            var jenis_kelamin = $("#jenis_kelamin").val();
            if (jenis_kelamin == '') {
                alert('Mohon pilih jenis kelamin yang benar!');
            }
        });
        $("#golongan_darah").on('change', function() {
            var golongan_darah = $("#golongan_darah").val();
            if (golongan_darah == '') {
                alert('Mohon pilih golongan darah yang benar!');
            }
        });
        $("#agama").on('change', function() {
            var agama = $("#agama").val();
            if (agama == '') {
                alert('Mohon pilih agama yang benar!');
            }
        });

    });
</script>