<?php
$page = $this->uri->segment(4);
if ($page == 'edit_spk' || $page == 'add_spk') {
    $spk = 'current';
    $nama_header = 'Data SPK';
} elseif ($page == 'aturan_pembayaran') {
    $aturan_pembayaran = 'current';
    $nama_header = 'Aturan Pembayaran';
} elseif ($page == 'add_berita_acara') {
    $ba = 'current';
    $nama_header = 'Berita Acara';    
} elseif ($page == 'set_pengawas') {
    $pengawas = 'current';
    $nama_header = 'Pengawas';    
}
?>
<div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url('ppk/dashboard'); ?>">Home</a></li>
        <li><a href="<?php echo base_url($urlnya.'/'.$classnya); ?>">Daftar SPK Pengadaan Langsung</a></li>
        <li class="active"><?php echo $nama_header;?></li>
    </ol>
</div>
<div class="row" style="margin:15px 0 15px 0;">
    <ul id="breadcrumbs-two">
        <li><a class="col-md-12 <?php echo @$spk; ?>" href="<?php echo base_url($urlnya . '/' . $classnya . '/cek_spk/' . $id_kontrak . '/' . $id_paket_kegiatan); ?>" data-toggle="tooltip" data-placement="bottom" title="Form Data SPK">Data SPK</a></li>
        <li><a class="col-md-12 <?php echo @$aturan_pembayaran; ?>" href="<?php echo base_url($urlnya . '/' . $classnya . '/aturan_pembayaran/' . $id_kontrak); ?>" data-toggle="tooltip" data-placement="bottom" title="Isi Data SPK terlebih dulu">Aturan Pembayaran</a></li>
        <li><a class="col-md-12 <?php echo @$ba; ?>" href="<?php echo base_url($urlnya . '/' . $classnya . '/add_berita_acara/' . $id_kontrak); ?>" data-toggle="tooltip" data-placement="bottom" title="Isi Aturan Pembayaran terlebih dulu">Berita Acara</a></li>
        <li><a class="col-md-12 <?php echo @$pengawas; ?>" href="<?php echo base_url($urlnya . '/' . $classnya . '/set_pengawas/' . $id_kontrak); ?>" data-toggle="tooltip" data-placement="bottom" title="Isi Berita Acara terlebih dulu">Pengawas</a></li>    
    </ul>
</div>