<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="refresh" content="3;<?php echo base_url('login'); ?>">

    <title>Let's start GRMS</title>

    <!-- plugin CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.css');?>" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,700,400,600' rel='stylesheet' type='text/css'>

    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/demo/style.css');?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <header class="header navbar navbar-fixed-top" role="banner">
        <div class="container-fluid">
            <div class="brand">
                <!-- Logo -->
                <a class="navbar-brand" href="index.html">
                    <img src="<?php echo base_url('assets/images/logo.png');?>" alt="logo" />
                </a>

            </div>


            
            
        </div>
    </header>

        <div class="wrap">
            <div class="container">
                <div class="col-lg-12">
                    <div class="login-box">
                        <div class="judul">
                            <h1>User Tidak Terdaftar</h1>
                            <ul class="sub-judul">
                                <li><a href="<?php echo base_url('login/logout'); ?>"><i class="fa fa-sitemap"></i>Maaf User anda tidak memiliki hak akses ke dalam aplikasi ini</a></li>
                            </ul>
                        </div>

                    </div>
                    
                </div>
                
            </div>
        </div>

        <div class="copyright">
            <span>&copy 2014 <a href="http://jatengprov.go.id">Pemerintah Provinsi Jawa Tengah</a></span>
        </div>
    </div>
    
<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>

    <script>
        $(".alert").alert()
    </script>

</body>

</html>
