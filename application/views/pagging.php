<script type="text/javascript">
$(document).ready(function () {
        $('.page').on('click', function(ev){
            $.ajax({
                type: 'POST',
                url: $(".page").data('url'),
                data: $('.page').serializeArray(),
                success: function (data) {
                    $("#reload").load($(".page").attr('data-url')).fadeIn(200).delay(2000);
                }
            });
            ev.preventDefault();
        }); 
    });
    var count = 5;
    var length = 5;
    oTable = $('#table').dataTable({
        "paging": true,
        "ordering": false,
        "info": false,
        "bFilter": true,
        "bLengthChange": false,
        "iDisplayLength": length,
        "iDisplayStart": count
    }); 

    $('input[name=cari]').keyup(function () {
        oTable.fnFilter(this.value);
    });  
</script>
