<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//model ini digunakan di semua controller untuk mengambil data user berdasar session login/

class User_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function get($tablename) {
        $query = $this->db->get($tablename, 10);
        return $query->result();
    }

    function view($tablename, $data) {
        $this->db->where($data);
        $query = $this->db->get($tablename);

        return $query->result();
    }

    function get_data_perusahaan($username) {
        $this->db->select('*');
        $this->db->from('perusahaan');
        $this->db->join('user', 'name = username');
        $this->db->where('username', $username);
        return $this->db->get()->result();
    }

    function get_data_user($username) {
        $this->db->select('*');
        $this->db->from('pegawai');
        $this->db->join('user', 'name = nip');
        $this->db->where('nip', $username);
        return $this->db->get()->result();
    }

    function update($tablename, $data, $yang_diedit) {
        $this->db->where($yang_diedit);
        $query = $this->db->update($tablename, $data);
        return $query;
    }

}
