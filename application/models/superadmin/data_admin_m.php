<?php

class Data_admin_m extends CI_Model {

    public function Jabatan_m() {
        parent:: __construct();
    }

    function get($tablename) {
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function insert($tablename, $data) {
        $query = $this->db->insert($tablename, $data);
        return $query;
    }

    function delete($tablename, $data) {
        $query = $this->db->delete($tablename, $data);
        return $query;
    }

    function view($tablename, $data) {
        $this->db->where($data);
        $query = $this->db->get($tablename);

        return $query->result();
    }

    function update($tablename, $data, $uniq_kode) {
        $this->db->where('user_name', $uniq_kode);
        $query = $this->db->update($tablename, $data);
        return $query;
    }

}

?>    	