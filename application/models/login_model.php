<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class login_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function get($tablename) {
        $query = $this->db->get($tablename, 10);
        return $query->result();
    }

    function view($tablename, $data) {
        $this->db->where($data);
        $query = $this->db->get($tablename);

        return $query->result();
    }

    function update($tablename, $data, $yang_diedit) {
        $this->db->where($yang_diedit);
        $query = $this->db->update($tablename, $data);
        return $query;
    }

    function update_entry() {
        $this->db->update('entries', $this, array('id' => $_POST['id']));
    }

    function cekLogin($tableName, $username, $password, $keyEnc) {
        $this->db->where('user_name', $username);
        //$this->db->where('password', "crypt('$password','$keyEnc')", FALSE);
        $query = $this->db->get($tableName);
        return $query->result();
    }

    function cekLogin2($tableName, $username) {
        $this->db->where('name', $username);
        $query = $this->db->get($tableName);
        return $query->result();
    }

    function insert($tablename, $data) {
        $query = $this->db->insert($tablename, $data);

        return $query;
    }

    function delete($tablename, $data) {
        $query = $this->db->delete($tablename, $data);

        return $query;
    }

    function getPegSes($tablename, $data) {
        $this->db->where($data);
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function get_data_untuk_session($username) {
        $this->db->select('user.name,app_user_role.role_uniq_kode,
			role.name as role_name,app_config.uniq_kode as app_kode, pegawai.nama as nama_pegawai');
        $this->db->from('user');
        $this->db->join('pegawai', 'pegawai.nip = user.name');
        $this->db->join('app_user_role', 'user.name = app_user_role.user_name');
        $this->db->join('role', 'role.uniq_kode = app_user_role.role_uniq_kode');
        $this->db->join('app_config', 'app_config.uniq_kode = app_user_role.app_config_uniq_kode');

        $this->db->where('user.name', $username);
        return $this->db->get()->result();
    }

    function cekDahLogin($nip, $app_config_uniq_kode) {
        $sql = "select *
from ci_sessions
where user_data like '%\"$nip\"%'
and user_data like '%\"app_config_uniq_kode\"%'
and cast(split_part(split_part(split_part(user_data, '\"app_config_uniq_kode\";', 2),':\"',2),'\";',1) 
as NUMERIC) = $app_config_uniq_kode
";
        $query = $this->db->query($sql);

        return $query->result();
    }

//    function cekDahLogin($nip) {
//        $sql = "select *
//from ci_sessions
//where user_data = $nip
//";
//        $query = $this->db->query($sql);
//
//        return $query->result();
//    }

    function cek_password_lama($password_lama_encrypt, $username) {
        $this->db->where('user_name', $username);
        $this->db->where('password', $password_lama_encrypt);
        $query = $this->db->get('app_user_role');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
