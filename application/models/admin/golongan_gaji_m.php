<?php

class Golongan_gaji_m extends CI_Model {

    function get($tablename) {
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function insert($tablename, $data) {
        $query = $this->db->insert($tablename, $data);
        return $query;
    }

    function delete($tablename, $data) {
        $query = $this->db->delete($tablename, $data);
        return $query;
    }

    function view($tablename, $data) {
        $this->db->where($data);
        $query = $this->db->get($tablename);

        return $query->result();
    }

    function update($tablename, $data, $uniq_kode) {
        $this->db->where('id', $uniq_kode);
        $query = $this->db->update($tablename, $data);
        return $query;
    }

    function get_order_by($tablename) {
//        $this->db->where($where);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function get_order_by_name($tablename) {
//        $this->db->where($where);
        $this->db->order_by('nama_jabatan', 'ASC');
        $query = $this->db->get($tablename);
        return $query->result();
    }
    function get_order_by_name_divisi($tablename) {
//        $this->db->where($where);
        $this->db->order_by('nama_divisi', 'ASC');
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function last_insert($tablename, $id) {
        $this->db->select_max($id);
        $result = $this->db->get($tablename)->row();
        return $result->$id + 1;
    }
function view_order($tablename,$data) {
//        $this->db->where($where);
        $this->db->order_by($data, 'ASC');
        $query = $this->db->get($tablename);
        return $query->result();
    }
}

?>    	