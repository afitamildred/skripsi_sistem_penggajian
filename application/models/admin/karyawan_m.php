<?php

class Karyawan_m extends CI_Model {

    function get($tablename) {
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function insert($tablename, $data) {
        $query = $this->db->insert($tablename, $data);
        return $query;
    }

    function delete($tablename, $data) {
        $query = $this->db->delete($tablename, $data);
        return $query;
    }

    function view($tablename, $data) {
        $this->db->where($data);
        $query = $this->db->get($tablename);

        return $query->result();
    }

    function update($tablename, $data, $uniq_kode) {
        $this->db->where('id', $uniq_kode);
        $query = $this->db->update($tablename, $data);
        return $query;
    }

    function get_order_by($tablename,$data,$by) {
//        $this->db->where($where);
        $this->db->order_by($data, $by);
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function get_order_by_name($tablename) {
//        $this->db->where($where);
        $this->db->order_by('nama_jabatan', 'ASC');
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function last_insert($tablename, $id) {
        $this->db->select_max($id);
        $result = $this->db->get($tablename)->row();
        return $result->$id + 1;
    }

    function get_order_by_name_divisi($tablename) {
//        $this->db->where($where);
        $this->db->order_by('nama_divisi', 'ASC');
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function get_order_by_name_proyek($tablename) {
//        $this->db->where($where);
        $this->db->order_by('nama_proyek', 'ASC');
        $query = $this->db->get($tablename);
        return $query->result();
    }
    
    function get_order_by_nama_karyawan($tablename) {
//        $this->db->where($where);
        $this->db->order_by('nama', 'ASC');
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function get_divisi($username) {
        $this->db->select("view_pinjaman.id,
                            view_pinjaman.nama_divisi,
                            view_pinjaman.nama_jabatan,
                            view_pinjaman.nama_karyawan
                            
                    ");
        $this->db->from("view_pinjaman");
        $this->db->join('karyawan', 'karyawan.username=view_karyawan.username');
        $this->db->where("karyawan.username", $username);
        $this->db->distinct();
        $result = $this->db->get();

        if ($result->num_rows() > 0) {

            return $result->result_array();
        } else {
            return array();
        }

//        $this->db->where('username', $username);
//        return $this->db->get('view_pinjaman')->result();
    }

}

?>    	