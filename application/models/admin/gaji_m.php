<?php

class Gaji_m extends CI_Model {

    function get($tablename) {
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function insert($tablename, $data) {
        $query = $this->db->insert($tablename, $data);
        return $query;
    }

    function delete($tablename, $data) {
        $query = $this->db->delete($tablename, $data);
        return $query;
    }

    function view($tablename, $data) {
        $this->db->where($data);
        $query = $this->db->get($tablename);

        return $query->result();
    }

    function update($tablename, $data, $uniq_kode) {
        $this->db->where('id', $uniq_kode);
        $query = $this->db->update($tablename, $data);
        return $query;
    }

    function get_order_by($tablename, $data, $order) {
//        $this->db->where($where);
        $this->db->order_by($data, $order);
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function get_order_by_where($tablename, $data, $order, $data2) {
//        $this->db->where($where);
        $this->db->order_by($data, $order);
        $this->db->where($data2);
        $query = $this->db->get($tablename);
        return $query->result();
    }

    function get_data_karyawan($username) {
        $this->db->where('username', $username);
        $result = $this->db->get('view_hitung_gaji');
        if ($result->num_rows() > 0) {

            return $result->result_array();
        } else {
            return array();
        }
    }

    function get_order_by_username_karyawan($tablename, $where) {
//        $this->db->where($where);
        $this->db->order_by('username', 'ASC');
        $this->db->where($where);
        $query = $this->db->get($tablename);
        return $query->result();
    }

}

?>    	