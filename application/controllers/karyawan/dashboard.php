<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_karyawan.php');

class dashboard extends base_karyawan {

//    private $tahun = CONS_TAHUN;
//    private $tahap = CONS_TAHAP;

    public function __construct() {
        parent:: __construct();
        $this->load->model('karyawan_m');
        session_start();
    }

   function index() {
        $datah['username'] = $this->session->userdata('username'); //ambil data user
//        $datauser = $this->admin_m->view('pegawai', array('nip' => $datah['nip']));
//        $datah['user'] = $datauser[0]->nama; //session user logged
//        $datah['avatar'] = $datauser[0]->avatar; //session user logged

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
//        $data['daftar_pekerjaan'] = $this->admin_m->get_daftar_pekerjaan();
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
        );
        $data['scripts'] = $scripts;
        //isset javascript
        $data['popups_js'] = 'ada';
        $this->load->view('karyawan/header', $datah);
        $this->load->view('karyawan/dashboard', $data);
        $this->load->view('karyawan/footer');
    }
    

}
