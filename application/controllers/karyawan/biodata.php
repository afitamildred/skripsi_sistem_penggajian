<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_karyawan.php');

class biodata extends base_karyawan {

    public function __construct() {
        parent:: __construct();
        $this->load->model('karyawan/biodata_m');
        session_start();
    }

    function index() {
        $data['page_title'] = 'Biodata';
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];

        @$data["data_karyawan"] = $this->biodata_m->view('view_data_karyawan', array('username' => $this->session->userdata('username')));
        $data['jenis_kelamin'] = @$data["data_karyawan"][0]->jenis_kelamin;
        $data['golongan_darah'] = @$data["data_karyawan"][0]->golongan_darah;
        $data['agama'] = @$data["data_karyawan"][0]->agama;
        $data['jenis/_kelamin'] = @$data["data_karyawan"][0]->jenis_kelamin;

        $data['username'] = array(
            'name' => 'username',
            'id' => 'username',
            'class' => 'form-control',
            'type' => 'text',
            'disabled' => 'disabled',
            'value' => @$data["data_karyawan"][0]->username
        );
        //
        $data['nama'] = array(
            'name' => 'nama',
            'id' => 'nama',
            'class' => 'form-control',
            'type' => 'text',
//            'disabled' => 'disabled',
            'value' => @$data["data_karyawan"][0]->nama_karyawan
        );
        $data['tanggal_masuk'] = array(
            'name' => 'tanggal_masuk',
            'id' => 'tanggal_masuk',
            'class' => 'form-control ',
            'disabled' => 'disabled',
            'type' => 'text',
            'value' => @$data["data_karyawan"][0]->tanggal_masuk
        );
        $data['lama_kontrak'] = array(
            'name' => 'lama_kontrak',
            'id' => 'lama_kontrak',
            'disabled' => 'disabled',
            'class' => 'form-control',
            'type' => 'text',
//            'value' => $data_karyawan[0]->id
        );
        $data['tanggal_akhir_kontrak'] = array(
            'name' => 'tanggal_akhir_kontrak',
            'id' => 'tanggal_akhir_kontrak',
            'class' => 'form-control ',
            'type' => 'text',
            'disabled' => 'disabled',
            'value' => @$data["data_karyawan"][0]->tanggal_akhir_kontrak
        );
        $data['status_karyawan'] = array(
            'name' => 'status_karyawan',
            'id' => 'status_karyawan',
            'class' => 'form-control',
            'type' => 'text',
            'disabled' => 'disabled',
            'value' => @$data["data_karyawan"][0]->status_karyawan
        );
        $data['divisi'] = array(
            'name' => 'divisi',
            'id' => 'divisi',
            'class' => 'form-control',
            'type' => 'text',
            'disabled' => 'disabled',
            'value' => @$data["data_karyawan"][0]->nama_divisi
        );
        $data['jabatan'] = array(
            'name' => 'jabatan',
            'id' => 'jabatan',
            'class' => 'form-control',
            'type' => 'text',
            'disabled' => 'disabled',
            'value' => @$data["data_karyawan"][0]->nama_jabatan
        );
        $data['no_ktp'] = array(
            'name' => 'no_ktp',
            'id' => 'no_ktp',
            'placeholder' => 'Masukkan No. SK',
            'data-error-empty' => 'No SK harus diisi',
            'class' => 'form-control',
            'type' => 'text',
            'value' => @$data["data_karyawan"][0]->no_ktp
        );
        $data['email'] = array(
            'name' => 'email',
            'id' => 'email',
            'class' => 'form-control',
            'type' => 'text',
            'value' => @$data["data_karyawan"][0]->email
        );
//        $data['jenis_kelamin'] = array(
//            'name' => 'jenis_kelamin',
//            'id' => 'jenis_kelamin',
//            'class' => 'form-control',
//            'readonly' => 'readonly',
//            'type' => 'text',
//        );
        $data['no_hp'] = array(
            'name' => 'no_hp',
            'id' => 'no_hp',
            'placeholder' => 'Masukkan no hp',
            'class' => 'form-control',
            'type' => 'text',
            'value' => @$data["data_karyawan"][0]->no_hp
        );
        $data['alamat_asal'] = array(
            'name' => 'alamat_asal',
            'id' => 'alamat_asal',
            'placeholder' => 'Masukkan alamat asal',
            'class' => 'form-control',
            'type' => 'text',
            'value' => @$data["data_karyawan"][0]->alamat_asal
        );
        $data['alamat_tinggal'] = array(
            'name' => 'alamat_tinggal',
            'id' => 'alamat_tinggal',
            'placeholder' => 'Masukkan alamat tinggal',
            'class' => 'form-control',
            'type' => 'text',
            'value' => @$data["data_karyawan"][0]->alamat_tinggal
        );

        $data['tanggal_lahir'] = array(
            'name' => 'tanggal_lahir',
            'id' => 'tanggal_lahir',
            'placeholder' => 'Masukkan tanggal lahir',
            'class' => "form-control datetimepicker",
            'type' => 'text',
            'value' => @$data["data_karyawan"][0]->tanggal_lahir
//            'value' => $data_karyawan[0]->tanggal_lahir
        );

        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );

        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default            
            '<script src="' . base_url('assets/js/jquery-ui.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/autocomplete_panitia.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/jquery.dataTables.min.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/dataTables.bootstrap.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/jquery.datetimepicker.js') . '"></script>' . "\n", //datepicker
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
            '<script src="' . base_url('assets/js/jquery.validate.js') . '"></script>' . "\n" //validate
        );
        $data['scripts'] = $scripts;
        $this->load->view('karyawan/header', $datah);
        $this->load->view('karyawan/biodata/daftar_biodata', $data);
        $this->load->view('karyawan/footer');
    }

    function simpan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['input'] = array(
            'nama' => $this->input->post('nama'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            "golongan_darah" => $this->input->post('golongan_darah'),
            'agama' => $this->input->post('agama'),
            "no_hp" => $this->input->post('no_hp'),
            'alamat_asal' => $this->input->post('alamat_asal'),
            'alamat_tinggal' => $this->input->post('alamat_tinggal'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'no_ktp' => $this->input->post('no_ktp'),
            'email' => $this->input->post('email')
        );
        print_r($data['input']);
        print_r($this->session->userdata('username'));
        if ($this->biodata_m->update('karyawan', $data['input'], $this->session->userdata('username'))) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

}

