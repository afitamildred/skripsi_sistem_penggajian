<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('user_model');
        $this->load->model('config_model');
    }

    function index() {
        if (isset($this->session->userdata['username'])) {
            if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_ADMIN) {
                redirect('admin/dashboard');
            } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_SUPERADMIN) {
                redirect('superadmin/dashboard');
            } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_KARYAWAN) {
                redirect('karyawan/dashboard');
            }
        }
        /* konstanta */
        $app_config_uniq_kode = APP_CONFIG_UNIQ_KODE;
        $this->session->set_userdata('app_config_uniq_kode', $app_config_uniq_kode);
        $data['action'] = 'login/login_process';
        $data['classnya'] = 'login';
        $data['urlnya'] = $data['classnya'] . '/index';
        $code = $this->generateCode(5);
        $newdata = array(
            'security_code' => $code,
        );

        $this->session->set_userdata($newdata);
        $data['captcha'] = array(
            'name' => 'captcha',
            'id' => 'login-name',
            'placeholder' => 'Masukkan kode',
            'type' => 'text',
            'class' => 'form-control required',
            'minlength' => "5",
            'value' => set_value("captcha"),
        );

        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'loadingSubmit',
            'data-loading-text' => 'Sedang membuka halaman...',
            'autocomplete' => 'off',
            'class' => 'btn btn-primary',
            'type' => 'submit',
            'title' => 'Masuk ke sistem',
            'value' => 'Login'
        );
        $this->load->view('login/login2', $data);
        $this->session->unset_userdata('error_message');
    }

    function login_process() {
        $data['classnya'] = 'login';
        $data['urlnya'] = $data['classnya'] . '/index';
        $data = array();
        $data['classnya'] = 'login';
        $data['urlnya'] = $data['classnya'] . '/index';
        $this->session->unset_userdata('error_message');

//        $cekDahLogin = $this->login_model->cekDahLogin($this->input->post('username'), APP_CONFIG_UNIQ_KODE);
        $cekLogin = $this->login_model->cekLogin('app_user_role', $this->input->post('username'), $this->input->post('password'), '$2a$08$/PMBB5iwXsN5MhFayYpHHe');
            
        if ($cekDahLogin != null) {
            $newdata = array('error_message' => "Sudah ada yang login dengan username ini.");
            $this->session->set_userdata($newdata);
            redirect($data['urlnya']);
        }
        if ($this->input->post('captcha') == $this->session->userdata('security_code')) {
            $this->session->unset_userdata('error_message');
            $password = md5($this->input->post('password'));
            $cekLogin = $this->login_model->cekLogin('app_user_role', $this->input->post('username'), $this->input->post('password'));
//            $cekLogin = $this->login_model->cekLogin('app_user_role', $this->input->post('username'), $this->input->post('password'), '$2a$08$/PMBB5iwXsN5MhFayYpHHe');
            if ($cekLogin == null) {
                $this->session->set_flashdata('failed_message', 'Username atau password salah.');
                redirect($data['urlnya']);
                return;
            } else {
                $this->session->unset_userdata('error_message');
                $this->session->unset_userdata('security_code');
                $dataUserRole = $this->login_model->view('app_user_role', array('user_name' => $cekLogin[0]->user_name));
                $newdata = array(
                    "logged_in" => TRUE,
                    "username" => $dataUserRole[0]->user_name,
                    "role" => $dataUserRole[0]->role
                );
                $this->session->set_userdata($newdata);
                if ($this->session->userdata('role') == 'admin') {
                    redirect('admin/dashboard');
                } else if ($this->session->userdata('role') == 'superadmin') {
                    redirect('superadmin/dashboard');
                } else if ($this->session->userdata('role') == 'karyawan') {
                    redirect('karyawan/dashboard');
                }
            }
        } else {
            $newdata = array('error_message' => "Captcha code salah.");
            $this->session->set_userdata($newdata);
            redirect($data['urlnya']);
        }
    }

    public function generateCode($characters) {
        /* list all possible characters, similar looking characters and vowels have been removed */
        $possible = '23456789bcdfghjkmnpqrstvwxyz';
        $code = '';
        $i = 0;
        while ($i < $characters) {
            $code .= substr($possible, mt_rand(0, strlen($possible) - 1), 1);
            $i++;
        }
        return $code;
    }

    public function checkCaptcha($post) {
        if ($post['captcha'] = $post['ccode']) {
            return 'Success';
        } else {
            return 'Mismatch, try again';
        }
    }

    /**
     * Memproses logout
     */
    function logout() {
        $data['role'] = $this->uri->segment(1);
        $data['classnya'] = 'index';
        $data['urlnya'] = $data['role'];
        $array_items = array('logged_in' => '', 'username' => '', 'app_kode' => '', 'role_uniq_kode' => '');
        $this->session->unset_userdata($array_items);
        $this->session->sess_destroy();
        redirect($data['urlnya']);
    }

    function proses_ubah_password() {
        $nip = $this->session->userdata['username'];
//        $data['keyEnc'] = $this->config_model->getKey('key');
        $keyEncnya = '$2a$08$/PMBB5iwXsN5MhFayYpHHe';
        if ($this->input->post('targetData') == false) {
            $data['array'] = array();
            $this->load->view("login/ubah_password", $data);
        } else {//edit_password
//            $data['keyEnc'] = $this->config_model->getKey('key');
            $keyEncnya = '$2a$08$/PMBB5iwXsN5MhFayYpHHe';
            $pass_baru_enc = crypt($this->input->post('password_baru'), $keyEncnya);
//            echo 'gds' . $pass_baru_enc;
            $data['update_user'] = array(
                "password" => $pass_baru_enc
            );
            $this->login_model->update('app_user_role', $data['update_user'], array('user_name' => $nip));
            if ($this->login_model->update('app_user_role', $data['update_user'], array('user_name' => $nip))) {
//                echo 'berhasil';
            } else {
//                echo 'gagal';
            }
            $ref = $_SERVER["HTTP_REFERER"];
            redirect($ref, 'refresh');
            echo '<script>history.back(1)</script>';
        }
    }

    public function password_lama_check() {
        // allow only Ajax request	
        if ($this->input->is_ajax_request()) {
            $username = $this->session->userdata['username'];
            $keyEncnya = '$2a$08$/PMBB5iwXsN5MhFayYpHHe';
            $password_lama_encrypt = crypt($this->input->post('password_lama'), $keyEncnya);
            $cek = $this->login_model->cek_password_lama($password_lama_encrypt, $username);
            if ($cek == FALSE) {
                echo json_encode(array('message' => false));
            } else {
                echo json_encode(array('message' => true));
            }
        }
    }

    function ubah_password() {
        $data['pegawai_nip'] = $this->session->userdata['username'];
//        echo 'gzdx';
        $this->load->view("login/ubah_password", $data);
    }

}

// END Login Class

/* End of file login.php */
/* Location: ./system/application/controllers/login.php */