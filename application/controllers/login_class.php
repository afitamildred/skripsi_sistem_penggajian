<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_class extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('user_model');
        $this->load->model('config_model');
    }

    function index() {
        if (isset($this->session->userdata['username'])) {
            if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_PPK) {
                redirect('ppk/dashboard');
            } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_PANITIA) {
                redirect('panitia/dashboard');
            } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_PENGAWAS) {
                redirect('pengawas/dashboard');
            } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_PENYEDIA) {
                redirect('penyedia/dashboard');
            } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_SKPD) {
                redirect('skpd/dashboard');
            } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_ADMIN and $this->session->userdata('approle') == APP_CONFIG_UNIQ_KODE) {
                redirect('admin/admin');
            }
        }
        /* konstanta */
        $app_config_uniq_kode = APP_CONFIG_UNIQ_KODE;
        $this->session->set_userdata('app_config_uniq_kode', $app_config_uniq_kode);
        $data['action'] = 'login/login_process';
        // buat captcha 
        $data['classnya'] = 'login';
        $data['urlnya'] = $data['classnya'] . '/index';
        $code = $this->generateCode(5);
        $newdata = array(
            'security_code' => $code,
        );

        $this->session->set_userdata($newdata);
        $data['captcha'] = array(
            'name' => 'captcha',
            'id' => 'login-name',
            'placeholder' => 'Masukkan kode',
            'type' => 'text',
            'class' => 'form-control required',
            'minlength' => "5",
            'value' => set_value("captcha"),
        );

        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'loadingSubmit',
            'data-loading-text' => 'Sedang membuka halaman...',
            'autocomplete' => 'off',
            'class' => 'btn btn btn-primary',
            'type' => 'submit',
            'title' => 'Masuk ke sistem',
            'value' => 'Login'
        );
        //$this->session->unset_userdata('error_message');
        $this->load->view('login/login2', $data);
        $this->session->unset_userdata('error_message');
    }

    function login_process() {
        //$as_penyedia =  $this->input->post('sebagai_penyedia');
        $data['classnya'] = 'login';
        $data['urlnya'] = $data['classnya'] . '/index';

        $data = array();
        if (isset($this->session->userdata['pegawai_nip'])) {
            if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_PPK) {
                redirect('ppk/dashboard');
            } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_PANITIA) {
                redirect('panitia/dashboard');
            } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_PENGAWAS) {
                redirect('pengawas/dashboard');
            } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_PENYEDIA) {
                redirect('penyedia/dashboard');
            } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_SKPD) {
                redirect('skpd/dashboard');
            } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_ADMIN and $this->session->userdata('approle') == APP_CONFIG_UNIQ_KODE) {
                redirect('admin/admin');
            }
        }

        $data['classnya'] = 'login';
        $data['urlnya'] = $data['classnya'] . '/index';
        $this->session->unset_userdata('error_message');

        $cekDahLogin = $this->login_model->cekDahLogin($this->input->post('username'), APP_CONFIG_UNIQ_KODE);
        if ($cekDahLogin != null) {
            $newdata = array('error_message' => "Sudah ada yang login dengan NIP ini.");
            $this->session->set_userdata($newdata);

            redirect($data['urlnya']);
        }

        $data['keyEnc'] = $this->config_model->getKey('key');

        //cek captcha
        if ($this->input->post('captcha') == $this->session->userdata('security_code')) {
            $this->session->unset_userdata('error_message');
            $cekLogin = $this->login_model->cekLogin('user', $this->input->post('username'), $this->input->post('password'), $data['keyEnc'][0]->value);
            if ($cekLogin == null) {
                $this->session->set_flashdata('failed_message', 'Username atau password salah.');
                redirect($data['urlnya']);
                return;
            } else {
                $this->session->unset_userdata('error_message');
                $this->session->unset_userdata('security_code');

                $dataUserRole = $this->login_model->view('app_user_role', array('user_name' => $cekLogin[0]->name));
                $newdata = array(
                    "logged_in" => TRUE,
                    "username" => $dataUserRole[0]->user_name,
                    "app_kode" => $dataUserRole[0]->app_config_uniq_kode,
                    "role_uniq_kode" => $dataUserRole[0]->role_uniq_kode,
                );

                // if ($as_penyedia == 1) { //jika sebagai penyedia.
                //     $dataUserRole = $this->login_model->view('app_user_role', array('user_name' => $cekLogin[0]->name));
                //     $newdata = array(
                //         "logged_in" => TRUE,
                //         "username" => $dataUserRole[0]->user_name,
                //         "app_kode" => $dataUserRole[0]->app_config_uniq_kode,
                //         "role_uniq_kode" => $dataUserRole[0]->role_uniq_kode,
                //     );
                // } else {
                //     $dataUserRole = $this->login_model->view('app_user_role', array('user_name' => $cekLogin[0]->name));
                //     $newdata = array(
                //         "logged_in" => TRUE,
                //         "username" => $dataUserRole[0]->user_name,
                //         "app_kode" => $dataUserRole[0]->app_config_uniq_kode,
                //         "role_uniq_kode" => $dataUserRole[0]->role_uniq_kode,
                //     );
                // }
                //echo '<pre>'; print_r($data['dataUser']); return;
                $this->session->set_userdata($newdata);
                if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_PPK) {
                    redirect('ppk/dashboard');
                } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_PANITIA) {
                    redirect('panitia/dashboard');
                } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_PENGAWAS) {
                    redirect('pengawas/dashboard');
                } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_PENYEDIA) {
                    redirect('penyedia/dashboard');
                } else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_SKPD) {
                    redirect('skpd/dashboard');
                }else if ($this->session->userdata('role_uniq_kode') == ROLE_UNIQ_KODE_ADMIN) { //and $this->session->userdata('approle') == APP_CONFIG_UNIQ_KODE) {
                    redirect('admin/admin');
                }
            }
        } else {
            $newdata = array('error_message' => "Captcha code salah.");
            $this->session->set_userdata($newdata);
            redirect($data['urlnya']);
        }
    }

    public function generateCode($characters) {
        /* list all possible characters, similar looking characters and vowels have been removed */
        $possible = '23456789bcdfghjkmnpqrstvwxyz';
        $code = '';
        $i = 0;
        while ($i < $characters) {
            $code .= substr($possible, mt_rand(0, strlen($possible) - 1), 1);
            $i++;
        }
        return $code;
    }

    public function checkCaptcha($post) {
        if ($post['captcha'] = $post['ccode']) {
            return 'Success';
        } else {
            return 'Mismatch, try again';
        }
    }

    /**
     * Memproses logout
     */
    function logout() {
        $data['role'] = $this->uri->segment(1);
        $data['classnya'] = 'index';
        $data['urlnya'] = $data['role'];
        $array_items = array('logged_in' => '', 'username' => '', 'app_kode' => '', 'role_uniq_kode' => '');
        $this->session->unset_userdata($array_items);
        $this->session->sess_destroy();
        redirect($data['urlnya']);
    }

    function edit_profil() {
        $data['page_title'] = 'Edit Akun';
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];

        $username = $this->session->userdata['username'];
        $role_uniq_kode = $this->session->userdata['role_uniq_kode'];
        if ($role_uniq_kode == 9) {//panitia
            $data['configcontent'] = $this->user_model->get_data_perusahaan($username);
        } else {
            $data['configcontent'] = $this->user_model->get_data_user($username);
        }

        $data['username'] = array(
            'name' => 'username',
            'type' => 'hidden',
            'value' => $username
        );

        $data['email'] = array(
            'name' => 'email',
            'id' => 'email',
            'class' => 'form-control email',
            'placeholder' => 'Masukkan email valid',
            'type' => 'text',
            'data-error-empty' => 'Email harus diisi',
            'value' => set_value('email', $data['configcontent'][0]->email)
        );

        $data['file_avatar'] = array(
            'name' => 'userfile',
            'id' => 'file_avatar',
            'type' => 'file',
            'value' => set_value('avatar', $data['configcontent'][0]->avatar)
        );
        $data['file_avatarDB'] = $data['configcontent'][0]->avatar; //untuk lihat file saat muncul popup edit
        $data['file_avatar_db'] = array(//untuk proses edit
            'name' => 'file_avatar_db',
            'type' => 'hidden',
            'value' => $data['configcontent'][0]->avatar
        );

        $data['password_lama'] = array(
            'name' => 'password_lama',
            'class' => 'form-control',
            'id' => 'password_lama',
            'data-error-empty' => 'Password lama harus diisi',
            'type' => 'text'
        );

        $data['password_baru'] = array(
            'name' => 'password_baru',
            'class' => 'form-control',
            'id' => 'password_baru',
            'data-error-empty' => 'Password baru harus diisi',
            'type' => 'text'
        );

        $data['password_lagi'] = array(
            'name' => 'password_lagi',
            'class' => 'form-control',
            'id' => 'password_lagi',
            'data-error-empty' => 'Ketik ulang password baru',
            'type' => 'text'
        );

        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn-primary pull-right',
            'type' => 'submit',
            'value' => 'Simpan'
        );

        $data['submitin_password'] = array(
            'name' => 'submitin',
            'id' => 'submitin_password',
            'class' => 'btn btn-primary pull-right',
            'type' => 'submit',
            'value' => 'Simpan',
            'disabled' => 'disabled'
        );

        if ($this->input->post('targetData') == false) {
            $data['array'] = array();
            $this->load->view('login/form_edit', $data);
        } else {
            //edit profil
            if ($this->input->post('targetForm') == 'edit_profil') {
                $config['upload_path'] = './assets/uploads/avatar/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['overwrite'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $fileName1 = 'ava' . '_' . $username;
                $config['file_name'] = $fileName1;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload();
                $dataupload = $this->upload->data();
                $data['email'] = $this->input->post('email');
                if ($_FILES['userfile']['name'] != '') {
                    $nama_baru_file = $dataupload['file_name'];
                } else {
                    $nama_baru_file = $data['file_avatarDB'];
                }
                $data['update_profil'] = array(
                    "email" => $data['email'],
                    "avatar" => $nama_baru_file
                );
                if ($role_uniq_kode == 9) {//panitia
                    $this->user_model->update('perusahaan', $data['update_profil'], array('username' => $username));
                } else {
                    $this->user_model->update('pegawai', $data['update_profil'], array('nip' => $username));
                }
                $ref = $_SERVER["HTTP_REFERER"];
                redirect($ref);
                echo '<script>history.back(1)</script>';
            } else { //edit_password
                $data['keyEnc'] = $this->config_model->getKey('key');
                $keyEncnya = $data['keyEnc'][0]->value;
                $pass_baru_enc = crypt($this->input->post('password_baru'), $keyEncnya);
                $data['update_user'] = array(
                    "password" => $pass_baru_enc
                );
                $this->user_model->update('user', $data['update_user'], array('name' => $username));
                $ref = $_SERVER["HTTP_REFERER"];
                redirect($ref, 'refresh');
                echo '<script>history.back(1)</script>';
            }
        }
    }

    public function password_lama_check() {
        // allow only Ajax request	
        if ($this->input->is_ajax_request()) {
            $username = $this->session->userdata['username'];
            $role_uniq_kode = $this->session->userdata['role_uniq_kode'];
            if ($role_uniq_kode == 9) {//panitia
                $data['configcontent'] = $this->user_model->get_data_perusahaan($username);
            } else {
                $data['configcontent'] = $this->user_model->get_data_user($username);
            }
            $data['keyEnc'] = $this->config_model->getKey('key');
            $keyEncnya = $data['keyEnc'][0]->value;
            // grab the password_lama value from the post variable.
            $password_lama_encrypt = crypt($this->input->post('password_lama'), $keyEncnya);
            // check in database - table name : user  , Field name in the table : password
            $cek = $this->login_model->cek_password_lama($password_lama_encrypt, $username);
            if ($cek == FALSE) {
                echo json_encode(array('message' => false));
            } else {
                echo json_encode(array('message' => true));
            }
        }
    }

}

// END Login Class

/* End of file login.php */
/* Location: ./system/application/controllers/login.php */