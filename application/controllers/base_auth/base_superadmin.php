<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Base_superadmin extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('login_model');

        if ($this->session->userdata('username')) {
            $username = $this->session->userdata('username');
        } else {
            $username = '';
        }

        $data['cekRole'] = array(
            'user_name' => $username
        );


        $data['role'] = $this->login_model->view('app_user_role', $data['cekRole']);

        if ($data['role'][0]->role == 'superadmin') {
            //klo berhasil login gak usah redirect
        } else {
            redirect('nonapl');
        }
    }

}

?>
