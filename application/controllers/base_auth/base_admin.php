<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Base_admin extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('login_model');

        if ($this->session->userdata('username')) {
            $username = $this->session->userdata('username');
        } else {
            $username = '';
        }

        $data['cekRole'] = array(
            'user_name' => $username
        );
        $data['role'] = $this->login_model->view('app_user_role', $data['cekRole']);
        if ($data['role'][0]->role == 'admin') {
            //klo berhasil login gak usah redirect
        } else {
            /* if(isset($_SERVER['HTTP_REFERER'])){

              //$this->output->set_header('refresh:3; url='.$_SERVER['HTTP_REFERER']);//redirect
              redirect('nonapl');
              }ELSE{
              redirect('nonapl');
              } */
            redirect('nonapl');
        }
    }

}

?>
