<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class nonapl extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/harjunacomsky.js') . '"></script>' . "\n", //other
        );
        $data['scripts'] = $scripts;
        $this->load->view('nonaplikasi', $data);
//        redirect('login');
    }

}

/* End of file  */
/* Location: ./application/controllers/ */