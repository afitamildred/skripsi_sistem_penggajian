<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_admin.php');

class gaji extends base_admin {

    public function __construct() {
        parent:: __construct();
        $this->load->model('admin/gaji_m');
        session_start();
    }

    function index() {
//        }
        $data['page_title'] = 'Data Gaji';
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];

        $data["per_page"] = DEFAULT_PAGE;
        @$data["data_gaji"] = $this->gaji_m->get_order_by('view_gaji', 'nama_divisi', 'ASC');
        @$data["data_divisi"] = $this->gaji_m->get_order_by('divisi', 'nama_divisi', 'ASC');
        //bualn
        $data['bulan'] = date('n');
        $this->session->set_userdata('bulan', date('n'));
        $data['bul'] = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default            
            '<script src="' . base_url('assets/js/jquery-ui.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/autocomplete_panitia.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/jquery.dataTables.min.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/dataTables.bootstrap.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
            '<script src="' . base_url('assets/js/jquery.validate.js') . '"></script>' . "\n" //validate
        );
        $data['scripts'] = $scripts;
        //isset javascript
        $data['popups_js'] = 'ada';
        $data['dataTable_js'] = 'ada';
        $this->load->view('admin/header', $datah);
        $this->load->view('admin/gaji/daftar_gaji', $data);
        $this->load->view('admin/footer');
    }

    function add_gaji() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        //bulan
        $data['bulan'] = date('n');
        $this->session->set_userdata('bulan', date('n'));
        $data['bul'] = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

//        @$data["username_karyawan"] = $this->gaji_m->get_order_by_username_karyawan('view_karyawan_gaji_blm_dipilih', array('id_karyawan' => null));
        @$data["username_karyawan"] = $this->gaji_m->get_order_by('karyawan', 'nama', 'ASC');

        $data['nama'] = array(
            'name' => 'nama',
            'id' => 'nama',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['div'] = array(
            'name' => 'div',
            'id' => 'div',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['jabatan'] = array(
            'name' => 'jabatan',
            'id' => 'jabatan',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );

        $data['tanggal_gaji'] = array(
            'name' => 'tanggal_gaji',
            'id' => 'tanggal_gaji',
            'placeholder' => 'Masukkan tanggal gaji',
            'class' => "form-control datetimepicker",
            'type' => 'text',
        );
        $data['gaji_pokok'] = array(
            'name' => 'gaji_pokok',
            'id' => 'gaji_pokok',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['pengurangan'] = array(
            'name' => 'pengurangan',
            'id' => 'pengurangan',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['penambahan'] = array(
            'name' => 'penambahan',
            'id' => 'penambahan',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );

        $data['tunjangan_proyek'] = array(
            'name' => 'tunjangan_proyek',
            'id' => 'tunjangan_proyek',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['tunjangan'] = array(
            'name' => 'tunjangan',
            'id' => 'tunjangan',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['potongan_absensi'] = array(
            'name' => 'potongan_absensi',
            'id' => 'potongan_absensi',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['jml_bonus'] = array(
            'name' => 'jml_bonus',
            'id' => 'jml_bonus',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['lembur'] = array(
            'name' => 'lembur',
            'id' => 'lembur',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['pinjaman'] = array(
            'name' => 'pinjaman',
            'id' => 'pinjaman',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['bonus'] = array(
            'name' => 'bonus',
            'id' => 'bonus',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['gaji_bersih'] = array(
            'name' => 'gaji_bersih',
            'id' => 'gaji_bersih',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery.datetimepicker.js') . '"></script>' . "\n", //datepicker
        );
        $data['scripts'] = $scripts;
        $this->load->view('admin/gaji/add_gaji', $data);
    }

    function edit($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        $data['gaji'] = $this->gaji_m->view('view_gaji1', array('id' => $id));
        $data_gapok = $this->gaji_m->view('golongan_gaji', array('id_divisi' => $data['gaji'][0]->id_divisi, 'id_jabatan' => $data['gaji'][0]->id_jabatan));

        $data['bulan'] = date('n');
        $this->session->set_userdata('bulan', date('n'));
        $data['bul'] = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        $data['periode'] = $data['gaji'][0]->periode;
//        print_r($data['gaji'][0]->username);
//        print_r($data['periode']);
        $data['username'] = array(
            'name' => 'username',
            'id' => 'username',
            'type' => 'hidden',
            'value' => $data['gaji'][0]->username
        );
        $data['nama'] = array(
            'name' => 'nama',
            'id' => 'nama',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data['gaji'][0]->nama_karyawan
        );
        $data['divisi'] = array(
            'name' => 'divisi',
            'id' => 'divisi',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data['gaji'][0]->nama_divisi
        );
        $data['jabatan'] = array(
            'name' => 'jabatan',
            'id' => 'jabatan',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data['gaji'][0]->nama_jabatan
        );
        $data['tanggal_gaji'] = array(
            'name' => 'tanggal_gaji',
            'id' => 'tanggal_gaji',
            'placeholder' => 'Masukkan tanggal gaji',
            'class' => "form-control datetimepicker",
            'type' => 'text',
            'value' => $data['gaji'][0]->tanggal_gaji
        );
        $data['gaji_pokok'] = array(
            'name' => 'gaji_pokok',
            'id' => 'gaji_pokok',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data_gapok[0]->gaji_pokok
        );
        $data['potongan_absensi'] = array(
            'name' => 'potongan_absensi',
            'id' => 'potongan_absensi',
//            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data['gaji'][0]->potongan
        );
        $pengurangan = $data['gaji'][0]->potongan + $data['gaji'][0]->angsuran_pinjam;
//        print_r($data['gaji'][0]->angsuran_pinjam);
        $data['pengurangan'] = array(
            'name' => 'pengurangan',
            'id' => 'pengurangan',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $pengurangan
        );
        $data['data_lembur'] = $this->gaji_m->view('lembur', array('id_karyawan' => $data['gaji'][0]->id_karyawan, 'periode' => $data['gaji'][0]->periode));
//        print_r($data['data_lembur'][0]->jumlah_bonus);
        $data['pendapatan'] = $data_gapok[0]->gaji_pokok + $data['gaji'][0]->tunjangan_proyek + $data['data_lembur'][0]->jumlah_bonus + $data['gaji'][0]->jumlah_bonus;
//        print_r($data['pendapatan']);
        $data['penambahan'] = array(
            'name' => 'penambahan',
            'id' => 'penambahan',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data['pendapatan']
        );
        $data['penambahan_unNum'] = array(
            'name' => 'penambahan_unNum',
            'id' => 'penambahan_unNum',
            'class' => 'form-control',
            'type' => 'hidden',
            'value' => $data['pendapatan']
        );
        $data['jumlah_bonus'] = array(
            'name' => 'jumlah_bonus',
            'id' => 'jumlah_bonus',
//            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data['gaji'][0]->jumlah_bonus
        );

        $data['lembur'] = array(
            'name' => 'lembur',
            'id' => 'lembur',
//            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data['data_lembur'][0]->jumlah_bonus
        );
        $data['pinjaman'] = array(
            'name' => 'pinjaman',
            'id' => 'pinjaman',
//            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data['gaji'][0]->jumlah_pinjam
        );
        $data['gaji_bersih'] = array(
            'name' => 'gaji_bersih',
            'id' => 'gaji_bersih',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data['gaji'][0]->gaji_bersih
        );
        $data['tunjangan_proyek'] = array(
            'name' => 'tunjangan_proyek',
            'id' => 'tunjangan_proyek',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data['gaji'][0]->tunjangan_proyek
        );
        $data['id'] = array(
            'name' => 'id',
            'id' => 'id',
            'class' => 'form-control',
            'type' => 'hidden',
            'value' => $data['gaji'][0]->id
        );

        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );

        $scripts = array(
            '<script src="' . base_url('assets/js/jquery.datetimepicker.js') . '"></script>' . "\n", //datepicker
        );
        $data['scripts'] = $scripts;
        $this->load->view('admin/gaji/edit_gaji', $data);
    }

    function simpan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $username = $this->input->post('username');
        $data['id_karyawan'] = $this->gaji_m->view('karyawan', array('username' => $username));
        $data['id_pinjaman'] = $this->gaji_m->view('pinjaman', array('id_karyawan' => $data['id_karyawan'][0]->id));
//        print_r($data['id_pinjaman'][0]->id);
        $data['input'] = array(
            'id_karyawan' => $data['id_karyawan'][0]->id,
            'periode' => $this->input->post('periode2'),
            'tanggal_gaji' => date('Y-m-d'),
            'gaji_bersih' => $this->input->post('gaji_bersih_unNum')
        );
//        print_r($data['input']);
        if ($this->gaji_m->insert('gaji', $data['input'])) {
            echo 'berhasil';

            if ($this->input->post('sisa_pinjam_unNum') == 0) {
                $data['update2'] = array(
                    'angsuran_pinjam' => $data['id_pinjaman'][0]->jumlah_pinjam,
                    'sisa_pinjam' => '0',
                    'status_pinjam' => 'lunas',
//                    'periode_angsuran' => $this->input->post('periode2'),
                );
                $this->gaji_m->update('pinjaman', $data['update2'], $data['id_pinjaman'][0]->id);
            } else {
                $data['update'] = array(
                    'angsuran_pinjam' => $this->input->post('total_angsuran_unNum'),
                    'sisa_pinjam' => $this->input->post('sisa_pinjam_unNum'),
                    'status_pinjam' => 'belum_lunas',
//                    'periode_angsuran' => $this->input->post('periode2'),
                );
//            print_r($data['update']);
                $this->gaji_m->update('pinjaman', $data['update'], $data['id_pinjaman'][0]->id);
            }
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function simpan_edit() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];

        $username = $this->input->post('username');
        $data['id_karyawan'] = $this->gaji_m->view('karyawan', array('username' => $username));
        $data['id_pinjaman'] = $this->gaji_m->view('pinjaman', array('id_karyawan' => $data['id_karyawan'][0]->id));
        $data['input'] = array(
            'gaji_bersih' => $this->input->post('gaji_bersih')
        );
//        print_r($data['input']);
        if ($this->gaji_m->update('gaji', $data['input'], $this->input->post('id'))) {
            if ($this->input->post('sisa_pinjam_unNum') == 0) {
                $data['update2'] = array(
                    'angsuran_pinjam' => $data['id_pinjaman'][0]->jumlah_pinjam,
                    'sisa_pinjam' => '0',
                    'status_pinjam' => 'lunas',
//                    'periode_angsuran' => $this->input->post('periode2'),
                );
                $this->gaji_m->update('pinjaman', $data['update2'], $data['id_pinjaman'][0]->id);
            } else {
                $data['update'] = array(
                    'angsuran_pinjam' => $this->input->post('total_angsuran_unNum'),
                    'sisa_pinjam' => $this->input->post('sisa_pinjam_unNum'),
                    'status_pinjam' => 'belum_lunas',
//                    'periode_angsuran' => $this->input->post('periode2'),
                );
////            print_r($data['update']);
                $this->gaji_m->update('pinjaman', $data['update'], $data['id_pinjaman'][0]->id);
            }
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    public function get_data_karyawan($periode) {
        $username = $this->input->post('username');
        $data_kary = $this->gaji_m->view('karyawan', array('username' => $username));

        $divisi = $data_kary[0]->id_divisi;
        $jabatan = $data_kary[0]->id_jabatan;
        $status = $data_kary[0]->status_karyawan;
        $data_gapok = $this->gaji_m->view('golongan_gaji', array('id_divisi' => $divisi, 'id_jabatan' => $jabatan, 'status_karyawan' => $status));

        $data_karyawan = $this->gaji_m->get_data_karyawan($username);
        $data_karyawan = $this->gaji_m->view('view_hitung_gaji', array('username' => $username));
        $jml_b = $this->gaji_m->view('view_bonus', array('periode' => $periode, 'username' => $username));
        if (empty($jml_b)) {
            $jml_bonus = '0';
        } else {
            $jml_bonus = $jml_b[0]->jumlah_bonus;
        }
//            //
        $pot_absensi = $this->gaji_m->view('view_absensi', array('periode' => $periode, 'username' => $username));
        if (empty($pot_absensi)) {
            $jml_pot_absensi = '0';
        } else {
            $jml_pot_absensi = $pot_absensi[0]->potongan;
        }
        $l = $this->gaji_m->view('lembur', array('id_karyawan' => $data_kary[0]->id, 'periode' => $periode));
        if (empty($l)) {
            $bonus_lembur = '0';
        } else {
//            $bonus_lembur = 'isi';
            $bonus_lembur = $l[0]->jumlah_bonus;
        }
        $data_pinjam1 = $this->gaji_m->view('pinjaman', array('id_karyawan' => $data_kary[0]->id));

        if (!empty($data_pinjam1)) {
            if (($data_pinjam1[0]->status_pinjam == 'belum_lunas') and ($data_pinjam1[0]->angsuran_pinjam > 1)) {
                $pinjaman = $data_pinjam1[0]->sisa_pinjam;
            } elseif (($data_pinjam1[0]->status_pinjam == 'belum_lunas') and ($data_pinjam1[0]->angsuran_pinjam == '')) {
                $pinjaman = $data_pinjam1[0]->jumlah_pinjam;
            } elseif (($data_pinjam1[0]->status_pinjam == 'lunas') and ($data_pinjam1[0]->angsuran_pinjam == $data_pinjam1[0]->jumlah_pinjam)) {
                $pinjaman = '0';
            }
        } else {
            $pinjaman = '0';
        }
        $data = $data_karyawan[0]->nama_karyawan
                . ',' . $data_karyawan[0]->nama_divisi
                . ',' . $data_karyawan[0]->nama_jabatan
                . ',' . $jml_bonus
                . ',' . $pinjaman
                . ',' . $jml_pot_absensi
                . ',' . $bonus_lembur
                . ',' . $data_karyawan[0]->tunjangan_proyek;

        echo $data . ',' . $data_gapok[0]->gaji_pokok;
    }

    function hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        if ($this->gaji_m->delete('gaji', array('id' => $id))) {
            echo "sukses";
        } else {
            echo "gagal";
        }
        redirect($data['urlnya']);
    }

    function cetak_gaji($isi) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
//        $data['ini'] = 'ini';

        $d = explode('_', $isi);
        $id_divisi = $d[0];
        @$periode = $d[1];
        if ($id_divisi == 0) {
            @$data['isi'] = @$isi;
            @$data["data_gaji"] = $this->gaji_m->get_order_by('view_gaji', 'nama_karyawan', 'ASC');
            @$data["periode"] = 'Semua Periode';
        } else {
            @$data['isi'] = @$isi;
            $d = explode('_', $isi);
            $id_divisi = $d[0];
            @$periode = $d[1];
            @$data['periode'] = $periode;
            @$data["data_gaji"] = $this->gaji_m->get_order_by_where('view_gaji', 'nama_divisi', 'ASC', array('id_divisi' => $id_divisi, 'periode' => $periode));
            @$data["periode"] = 'Periode ' . $periode;
        }
        $this->load->view('admin/gaji/cetak_gaji', $data);
    }

    function pdf_cetak_gaji($isi) {

        $d = explode('_', $isi);
        $id_divisi = $d[0];
        @$periode = $d[1];
        if ($id_divisi == 0) {
            @$data['isi'] = @$isi;
            @$data["data_gaji"] = $this->gaji_m->get_order_by('view_gaji', 'nama_karyawan', 'ASC');
            @$data["periode"] = 'Semua Periode';
        } else {
            @$data['isi'] = @$isi;
            $d = explode('_', $isi);
            $id_divisi = $d[0];
            @$periode = $d[1];
            @$data['periode'] = $periode;
            @$data["data_gaji"] = $this->gaji_m->get_order_by_where('view_gaji', 'nama_divisi', 'ASC', array('id_divisi' => $id_divisi, 'periode' => $periode));
            @$data["periode"] = 'Periode ' . $periode;
        }


        $this->load->view('admin/gaji/pdf_cetak_gaji', $data);
        // Get output html
        $html = $this->output->get_output();

        // Load library
        $this->load->library('dompdf_gen');

        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper(array(0, 0, 609.448818898, 935.433070866), 'landscape');
        $this->dompdf->render();
        $this->dompdf->stream("cetak_gaji.pdf");
    }

    function cetak_slip_gaji($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['id'] = $id;
//        $data['ini'] = 'ini';
        @$data["data_gaji"] = $this->gaji_m->view('view_gaji', array('id' => $id));
        $data['periode'] = $data["data_gaji"][0]->periode;
        $this->load->view('admin/gaji/cetak_slip_gaji', $data);
    }

    function pdf_cetak_slip_gaji($id) {

        @$data["data_gaji"] = $this->gaji_m->view('view_gaji', array('id' => $id));
        $data['periode'] = $data["data_gaji"][0]->periode;
        $this->load->view('admin/gaji/pdf_cetak_slip_gaji', $data);
        // Get output html
        $html = $this->output->get_output();
        // Load library
        $this->load->library('dompdf_gen');
        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper(array(0, 0, 609.448818898, 935.433070866), 'potrait');
        $this->dompdf->render();
        $this->dompdf->stream("slip_gaji_" . $id . ".pdf");
    }

    function validasi_hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['ambil_data'] = $this->gaji_m->view('view_gaji', array('id' => $id));
        $data['nama'] = $data['ambil_data'][0]->nama_karyawan;
        $data['id'] = $id;
        $this->load->view('admin/gaji/validasi_hapus', $data);
    }

    function lewat() {
        $data = array('status_data' => true);
        echo json_encode($data);
    }

    function daftar_gaji() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['isi'] = '0_pilih';
        @$data["data_gaji"] = $this->gaji_m->get_order_by('view_gaji', 'nama_karyawan', 'ASC');

        $this->load->view('admin/gaji/data_gaji', $data);
    }

    function daftar_gaji_klik($isi) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $d = explode('_', $isi);
        $id_divisi = $d[0];
        $periode = $d[1];

        @$data['isi'] = @$isi;
        @$data['periode'] = $periode;
        @$data["data_gaji"] = $this->gaji_m->get_order_by_where('view_gaji', 'nama_divisi', 'ASC', array('id_divisi' => $id_divisi, 'periode' => $periode));

        $this->load->view('admin/gaji/data_gaji', $data);
    }

}
