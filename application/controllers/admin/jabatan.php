<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_admin.php');

class jabatan extends base_admin {

    public function __construct() {
        parent:: __construct();
        $this->load->model('admin/jabatan_m');
        session_start();
    }

    function index() {
        $data['page_title'] = 'Data Jabatan';
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];

        $data["per_page"] = DEFAULT_PAGE;
        @$data["data_jabatan"] = $this->jabatan_m->get_order_by('jabatan');
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default            
            '<script src="' . base_url('assets/js/jquery-ui.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/autocomplete_panitia.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/jquery.dataTables.min.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/dataTables.bootstrap.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
            '<script src="' . base_url('assets/js/jquery.validate.js') . '"></script>' . "\n" //validate
        );
        $data['scripts'] = $scripts;
        //isset javascript
        $data['popups_js'] = 'ada';
        $data['dataTable_js'] = 'ada';
        $this->load->view('admin/header', $datah);
        $this->load->view('admin/jabatan/daftar_jabatan', $data);
        $this->load->view('admin/footer');
    }

    function data_per_page($page) {

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data["per_page"] = $page;
        $data["jabatan"] = $this->jabatan_m->get_jabatan('viewjabatan', $data["per_page"]);
        $this->load->view('ppk/jabatan/data_jabatan', $data);
    }

    function add_jabatan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        //added
        //
        $data['nama_jabatan'] = array(
            'name' => 'nama_jabatan',
            'id' => 'nama_jabatan',
            'placeholder' => 'Masukan nama jabatan',
            'class' => 'form-control',
            'type' => 'text',
        );

        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );
        $this->load->view('admin/jabatan/add_jabatan', $data);
    }

    function simpan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['input'] = array(
            'nama_jabatan' => $this->input->post('nama_jabatan')
        );
        print_r($data['input']);
        if ($this->jabatan_m->insert('jabatan', $data['input'])) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function edit($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        $data_jabatan = $this->jabatan_m->view('jabatan', array('id' => $id));
        $data['nama_jabatan'] = array(
            'name' => 'nama_jabatan',
            'id' => 'nama_jabatan',
            'placeholder' => 'Masukan nama jabatan',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data_jabatan[0]->nama_jabatan
        );
        //

        $data['id'] = array(
            'name' => 'id',
            'id' => 'id',
            'class' => 'form-control',
            'type' => 'hidden',
            'value' => $data_jabatan[0]->id
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );

        $this->load->view('admin/jabatan/edit_jabatan', $data);
    }

    function simpan_edit() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['input'] = array(
            'nama_jabatan' => $this->input->post('nama_jabatan')
        );
//        print_r($data['input']);
        if ($this->jabatan_m->update('jabatan', $data['input'], $this->input->post('id'))) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        if ($this->jabatan_m->delete('jabatan', array('id' => $id))) {
            echo "sukses";
        } else {
            echo "gagal";
        }
        redirect($data['urlnya']);
    }

    function validasi_hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['ambil_data'] = $this->jabatan_m->view('jabatan', array('id' => $id));
        $data['nama'] = $data['ambil_data'][0]->nama_jabatan;
        $data['id'] = $id;
        $this->load->view('admin/jabatan/validasi_hapus', $data);
    }

    function cetak_data() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        @$data["data_order"] = $this->jabatan_m->view_order('jabatan', 'nama_jabatan');
        $this->load->view('admin/jabatan/cetak_data', $data);
    }

    function pdf_cetak() {

        @$data["data_order"] = $this->jabatan_m->view_order('jabatan', 'nama_jabatan');
        $this->load->view('admin/jabatan/pdf_cetak', $data);
        // Get output html
        $html = $this->output->get_output();

        // Load library
        $this->load->library('dompdf_gen');

        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper(array(0, 0, 609.448818898, 935.433070866), 'potrait');
        $this->dompdf->render();
        $this->dompdf->stream("cetak_jabatan.pdf");
    }

}
