<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_admin.php');

class pinjaman extends base_admin {

    public function __construct() {
        parent:: __construct();
        $this->load->model('admin/pinjaman_m');
        session_start();
    }

    function index() {
        $data['page_title'] = 'Data Pinjaman';
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];

        $data["per_page"] = DEFAULT_PAGE;
        @$data["data_pinjaman"] = $this->pinjaman_m->get_order_by('view_pinjaman','nama_karyawan','ASC');
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default            
            '<script src="' . base_url('assets/js/jquery-ui.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/autocomplete_panitia.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/jquery.dataTables.min.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/dataTables.bootstrap.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
            '<script src="' . base_url('assets/js/jquery.validate.js') . '"></script>' . "\n" //validate
        );
        $data['scripts'] = $scripts;
        $data['popups_js'] = 'ada';
        $data['dataTable_js'] = 'ada';
        $this->load->view('admin/header', $datah);
        $this->load->view('admin/pinjaman/daftar_pinjaman', $data);
        $this->load->view('admin/footer');
    }

    function add_pinjaman() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
//        @$data["username_karyawan"] = $this->pinjaman_m->get_order_by_username_karyawan('view_karyawan_pinjaman_blm_dipilih', array('id_karyawan' => null));
        @$data["username_karyawan"] = $this->pinjaman_m->get_order_by('karyawan','nama','ASC');
        $data['nama'] = array(
            'name' => 'nama',
            'id' => 'nama',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['divisi'] = array(
            'name' => 'divisi',
            'id' => 'divisi',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['jabatan'] = array(
            'name' => 'jabatan',
            'id' => 'jabatan',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['tanggal_pinjam'] = array(
            'name' => 'tanggal_pinjam',
            'id' => 'tanggal_pinjam',
            'placeholder' => 'Masukkan tanggal pinjam',
            'class' => "form-control datetimepicker",
            'type' => 'text'
        );
        $data['jumlah_pinjam'] = array(
            'name' => 'jumlah_pinjam',
            'id' => 'jumlah_pinjam',
            'placeholder' => 'Masukkan jumlah pinjam',
            'data-error-empty' => 'Jumlah pinjam harus diisi',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['alasan_pinjam'] = array(
            'name' => 'alasan_pinjam',
            'id' => 'alasan_pinjam',
            'placeholder' => 'Masukkan alasan pinjam',
            'data-error-empty' => 'Alasan pinjam harus diisi',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery.datetimepicker.js') . '"></script>' . "\n", //datepicker
        );
        $data['scripts'] = $scripts;
        $this->load->view('admin/pinjaman/add_pinjaman', $data);
    }

    function edit($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        $data_pinjaman = $this->pinjaman_m->view('view_pinjaman', array('id' => $id));
        $data['username'] = array(
            'name' => 'username',
            'id' => 'autocomplete',
            'placeholder' => 'Masuakan username karyawan',
            'class' => 'form-control',
            'type' => 'text',
            'disabled' => 'disabled',
            'value' => $data_pinjaman[0]->username
        );
        //
        $data['nama'] = array(
            'name' => 'nama',
            'id' => 'nama',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data_pinjaman[0]->nama_karyawan
        );
        $data['divisi'] = array(
            'name' => 'divisi',
            'id' => 'divisi',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data_pinjaman[0]->nama_divisi
        );
        $data['jabatan'] = array(
            'name' => 'jabatan',
            'id' => 'jabatan',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data_pinjaman[0]->nama_jabatan
        );
        $data['tanggal_pinjam'] = array(
            'name' => 'tanggal_pinjam',
            'id' => 'tanggal_pinjam',
            'placeholder' => 'Masukkan tanggal pinjam',
            'class' => "form-control datetimepicker",
            'type' => 'text',
            'value' => $data_pinjaman[0]->tanggal_pinjam
        );
        $data['jumlah_pinjam'] = array(
            'name' => 'jumlah_pinjam',
            'id' => 'jumlah_pinjam',
            'placeholder' => 'Masukkan jumlah pinjam',
            'data-error-empty' => 'Jumlah pinjam harus diisi',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data_pinjaman[0]->jumlah_pinjam
        );
        $data['alasan_pinjam'] = array(
            'name' => 'alasan_pinjam',
            'id' => 'alasan_pinjam',
            'placeholder' => 'Masukkan alasan pinjam',
            'data-error-empty' => 'Alasan pinjam harus diisi',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data_pinjaman[0]->alasan_pinjam
        );

        $data['id'] = array(
            'name' => 'id',
            'id' => 'id',
            'class' => 'form-control',
            'type' => 'hidden',
            'value' => $data_pinjaman[0]->id
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery.datetimepicker.js') . '"></script>' . "\n", //datepicker
        );
        $data['scripts'] = $scripts;
        $this->load->view('admin/pinjaman/edit_pinjaman', $data);
    }

    function simpan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $username = $this->input->post('username');
        print_r($username);
        $data['id_karyawan'] = $this->pinjaman_m->view('karyawan', array('username' => $username));

        $data['input'] = array(
            'id_karyawan' => $data['id_karyawan'][0]->id,
            'tanggal_pinjam' => $this->input->post('tanggal_pinjam'),
            'jumlah_pinjam' => $this->input->post('jumlah_pinjam_unNum'),
            'alasan_pinjam' => $this->input->post('alasan_pinjam'),
            'status_pinjam' => 'belum_lunas',
            'tanggal' => date('Y-m-d')
        );
        print_r($data['input']);
        if ($this->pinjaman_m->insert('pinjaman', $data['input'])) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function simpan_edit() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['input'] = array(
            'tanggal_pinjam' => $this->input->post('tanggal_pinjam'),
            'jumlah_pinjam' => $this->input->post('jumlah_pinjam_unNum'),
            'alasan_pinjam' => $this->input->post('alasan_pinjam'),
            'status_pinjam' => 'belum_lunas'
        );
        print_r($data['input']);
        if ($this->pinjaman_m->update('pinjaman', $data['input'], $this->input->post('id'))) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        if ($this->pinjaman_m->delete('pinjaman', array('id' => $id))) {
            echo "sukses";
        } else {
            echo "gagal";
        }
        redirect($data['urlnya']);
    }

    public function get_data_karyawan() {
        $username = $this->input->post('username');
        $data_karyawan = $this->pinjaman_m->get_data_karyawan($username);
        foreach ($data_karyawan as $u) {
            $data = "$u[nama_karyawan],$u[nama_divisi],$u[nama_jabatan]";
        }
        echo $data;
    }

    function validasi_hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['ambil_data'] = $this->pinjaman_m->view('view_pinjaman', array('id' => $id));
        $data['nama'] = $data['ambil_data'][0]->nama_karyawan;
        $data['id'] = $id;
        $this->load->view('admin/pinjaman/validasi_hapus', $data);
    }

    function cetak_data() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        @$data["data_order"] = $this->pinjaman_m->view_order('view_pinjaman', 'nama_karyawan');
        $this->load->view('admin/pinjaman/cetak_data', $data);
    }

    function pdf_cetak() {

        @$data["data_order"] = $this->pinjaman_m->view_order('view_pinjaman', 'nama_karyawan');
        $this->load->view('admin/pinjaman/pdf_cetak', $data);
        // Get output html
        $html = $this->output->get_output();

        // Load library
        $this->load->library('dompdf_gen');

        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper(array(0, 0, 609.448818898, 935.433070866), 'potrait');
        $this->dompdf->render();
        $this->dompdf->stream("cetak_pinjaman.pdf");
    }

}
