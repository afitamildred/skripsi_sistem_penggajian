<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_admin.php');

class karyawan extends base_admin {

    public function __construct() {
        parent:: __construct();
        $this->load->model('admin/karyawan_m');
        session_start();
    }

    function index() {
        $data['page_title'] = 'Data Karyawan';
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];

        @$data["data_karyawan"] = $this->karyawan_m->get_order_by('view_karyawan','nama','ASC');
//        print_r($data["data_karyawan"]);
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default            
            '<script src="' . base_url('assets/js/jquery-ui.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/autocomplete_panitia.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/jquery.dataTables.min.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/dataTables.bootstrap.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
            '<script src="' . base_url('assets/js/jquery.validate.js') . '"></script>' . "\n", //validate
//            '<script src="' . base_url('assets/js/jquery.datetimepicker.js') . '"></script>' . "\n", //datepicker
        );
        $data['scripts'] = $scripts;
        $data['popups_js'] = 'ada';
        $data['dataTable_js'] = 'ada';
        $this->load->view('admin/header', $datah);
        $this->load->view('admin/karyawan/daftar_karyawan', $data);
        $this->load->view('admin/footer');
    }


    function add_karyawan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
//        $data['nip_pegawai'] = $this->karyawan_m->get_pegawai();
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        $datauser = $this->karyawan_m->view('karyawan', array('username' => $datah['nip']));
        @$data["data_jabatan"] = $this->karyawan_m->get_order_by_name('jabatan');
        @$data["data_proyek"] = $this->karyawan_m->get_order_by_name_proyek('tunjangan_proyek');
        @$data["data_divisi"] = $this->karyawan_m->get_order_by_name_divisi('divisi');
        $data['id_terakhir'] = $this->karyawan_m->last_insert('app_user_role', 'uniq_kode');
        if ($data['id_terakhir'] < 9) {
            $data['id_terakhir'] = '0' . $data['id_terakhir'];
        }
        //
        $data['nama'] = array(
            'name' => 'nama',
            'id' => 'nama',
            'placeholder' => 'Masukkan nama karyawan',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['divisi'] = array(
            'name' => 'divisi',
            'id' => 'divisi',
            'placeholder' => 'Masukkan divisi dulu',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['jabatan'] = array(
            'name' => 'jabatan',
            'id' => 'jabatan',
            'placeholder' => 'Masukkan jabatan dulu',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['status_pekerjaan'] = array(
            'name' => 'status_pekerjaan',
            'id' => 'status_pekerjaan',
            'placeholder' => 'Masukkan No. SK',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['tanggal_masuk'] = array(
            'name' => 'tanggal_masuk',
            'id' => 'tanggal_masuk',
            'placeholder' => 'Masukkan tanggal masuk kerja',
            'class' => 'form-control datetimepicker',
            'type' => 'text',
        );
        $data['tanggal_akhir_kontrak'] = array(
            'name' => 'tanggal_akhir_kontrak',
            'id' => 'tanggal_akhir_kontrak',
            'placeholder' => 'Masukkan selesai kerja',
            'class' => 'form-control datetimepicker',
            'type' => 'text',
        );
        $data['status_karyawan'] = array(
            'name' => 'status_karyawan',
            'id' => 'status_karyawan',
            'placeholder' => 'Masukkan Status Karyawan',
            'data-error-empty' => 'No SK harus diisi',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['tunjangan_proyek'] = array(
            'name' => 'tunjangan_proyek',
            'id' => 'tunjangan_proyek',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'type' => 'text',
            'disabled' => 'disabled'
        );
        $data['no_ktp'] = array(
            'name' => 'no_ktp',
            'id' => 'no_ktp',
            'placeholder' => 'Masukkan No. KTP',
            'data-error-empty' => 'No SK harus diisi',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['gaji_pokok'] = array(
            'name' => 'gaji_pokok',
            'id' => 'gaji_pokok',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'type' => 'text',
            'disabled' => 'disabled'
        );

        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery.datetimepicker.js') . '"></script>' . "\n", //datepicker
        );
        $data['scripts'] = $scripts;
        $this->load->view('admin/karyawan/add_karyawan', $data);
    }

    function simpan() {

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $divisi = $this->input->post('divisi');
        if ($divisi < 10) {
            $div = '0' . $divisi;
        } else {
            $div = $divisi;
        }
        $jabatan = $this->input->post('jabatan');

        if ($jabatan < 10) {
            $jab = '0' . $jabatan;
        } else {
            $jab = $jabatan;
        }
        $data['id_terakhir'] = $this->karyawan_m->last_insert('app_user_role', 'uniq_kode');
        if ($data['id_terakhir'] < 9) {
            $data['id_terakhir'] = '.0' . $data['id_terakhir'];
        } else {
            $data['id_terakhir'] = '.' . $data['id_terakhir'];
        }
        $username = $div .'.'. $jab . $data['id_terakhir'];
        print_r($username);
        $data['input'] = array(
            'nama' => $this->input->post('nama'),
            'username' => $username,
            'id_divisi' => $this->input->post('divisi'),
            "id_jabatan" => $this->input->post('jabatan'),
            'no_ktp' => $this->input->post('no_ktp'),
            'tanggal_masuk' => $this->input->post('tanggal_masuk'),
            'tanggal_akhir_kontrak' => $this->input->post('tanggal_akhir_kontrak'),
            "status_karyawan" => $this->input->post('status_karyawan'),
            'id_proyek' => $this->input->post('id_proyek'),
            'tanggal' => date('Y-m-d')
        );
        print_r($data['input']);
        if ($this->karyawan_m->insert('karyawan', $data['input'])) {
            echo 'berhasil input karyawan';

            $data['input3'] = array(
                'role' => 'karyawan',
                'user_name' => $username,
                'password' => '$2a$08$/PMBB5iwXsN5MhFayYpHHeJSJBNZ5k3EuslBZKV7M8JJDmwBGQhZ6'
            );
            $this->karyawan_m->insert('app_user_role', $data['input3']);
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function edit($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        @$data["data_jabatan"] = $this->karyawan_m->get_order_by_name('jabatan');
        @$data["data_divisi"] = $this->karyawan_m->get_order_by_name_divisi('divisi');
        @$data["data_proyek"] = $this->karyawan_m->get_order_by_name_proyek('tunjangan_proyek');
        $data_karyawan = $this->karyawan_m->view('karyawan', array('id' => $id));
        $data['data_karyawan'] = $this->karyawan_m->view('karyawan', array('id' => $id));
        //added
        $data['status_karyawan'] = $data['data_karyawan'][0]->status_karyawan;


        $divisi = $data['data_karyawan'][0]->id_divisi;
        $jabatan = $data['data_karyawan'][0]->id_jabatan;
        $status_karyawan = $data['data_karyawan'][0]->status_karyawan;
//        $data_gapok = $this->karyawan_m->get_gapok($username);
        $data_gapok = $this->karyawan_m->view('golongan_gaji', array('id_divisi' => $divisi,
            'id_jabatan' => $jabatan,
            'status_karyawan' => $status_karyawan)
        );
        $tunj_proyek = $this->karyawan_m->view('tunjangan_proyek', array('id' => $data['data_karyawan'][0]->id_proyek)
        );
//        print_r($tunj_proyek[0]->tunjangan_proyek);
        $data['gaji_pokok'] = array(
            'name' => 'gaji_pokok',
            'id' => 'gaji_pokok',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'type' => 'text',
            'disabled' => 'disabled',
            'value' => @$data_gapok[0]->gaji_pokok
        );
        $data['tunjangan_proyek'] = array(
            'name' => 'tunjangan_proyek',
            'id' => 'tunjangan_proyek',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'type' => 'text',
            'disabled' => 'disabled',
            'value' => @$tunj_proyek[0]->tunjangan_proyek
        );
        $data['username'] = array(
            'name' => 'username',
            'id' => 'username',
            'placeholder' => 'Masukan username karyawan',
            'data-error-empty' => 'Username harus diisi',
            'class' => 'form-control',
            'type' => 'text',
            'readonly' => 'readonly',
            'value' => @$data_karyawan[0]->username
        );
        //
        $data['nama'] = array(
            'name' => 'nama',
            'id' => 'nama',
            'placeholder' => 'Masukkan username dulu',
            'data-error-empty' => 'Nama harus diisi atau tidak ada data',
            'class' => 'form-control',
            'type' => 'text',
            'value' => @$data_karyawan[0]->nama
        );
        $data['jenis_kelamin'] = array(
            'name' => 'jenis_kelamin',
            'id' => 'jenis_kelamin',
            'placeholder' => 'Masukkan jumlah_harus_hadir dulu',
            'data-error-empty' => 'jumlah_harus_hadir harus diisi atau tidak ada data',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['golongan_darah'] = array(
            'name' => 'golongan_darah',
            'id' => 'golongan_darah',
            'placeholder' => 'Masukkan jumlah_hadir dulu',
            'data-error-empty' => 'jumlah_hadir harus diisi atau tidak ada data',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['tanggal_masuk'] = array(
            'name' => 'tanggal_masuk',
            'id' => 'tanggal_masuk',
            'placeholder' => 'Masukkan No. SK',
            'data-error-empty' => 'No SK harus diisi',
            'class' => 'form-control datetimepicker',
            'type' => 'text',
            'value' => @$data_karyawan[0]->tanggal_masuk
        );
        $data['lama_kontrak'] = array(
            'name' => 'lama_kontrak',
            'id' => 'lama_kontrak',
            'placeholder' => 'Masukkan No. SK',
            'data-error-empty' => 'No SK harus diisi',
            'class' => 'form-control',
            'type' => 'text',
//            'value' => $data_karyawan[0]->id
        );
        $data['tanggal_akhir_kontrak'] = array(
            'name' => 'tanggal_akhir_kontrak',
            'id' => 'tanggal_akhir_kontrak',
            'placeholder' => 'Masukkan No. SK',
            'data-error-empty' => 'No SK harus diisi',
            'class' => 'form-control datetimepicker',
            'type' => 'text',
            'value' => @$data_karyawan[0]->tanggal_akhir_kontrak
        );

        $data['no_ktp'] = array(
            'name' => 'no_ktp',
            'id' => 'no_ktp',
            'placeholder' => 'Masukkan No. SK',
            'data-error-empty' => 'No SK harus diisi',
            'class' => 'form-control',
            'type' => 'text',
            'value' => @$data_karyawan[0]->no_ktp
        );

        $data['id'] = array(
            'name' => 'id',
            'id' => 'id',
            'class' => 'form-control',
            'type' => 'hidden',
            'value' => @$data_karyawan[0]->id
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );

        if ($this->input->post('targetData') == false) {
            $scripts = array(
                '<script src="' . base_url('assets/js/jquery.datetimepicker.js') . '"></script>' . "\n", //datepicker
            );
            $data['scripts'] = $scripts;
            $this->load->view('admin/karyawan/edit_karyawan', $data);
        } else {
            $nama = $this->input->post('nama');

            $data['input'] = array(
                'id_karyawan' => $this->input->post('username'),
                'jumlah_harus_hadir' => $this->input->post('jumlah_harus_hadir'),
                'jumlah_hadir' => $this->input->post('jumlah_hadir')
            );

            if (empty($nomer_sk) || $nama == "Panitia mungkin telah terpilih atau tidak ada data.") {
                return false;
            } else {
                $this->karyawan_m->insert('karyawan', $data['input']);
                echo "sukses";
            }
        }
    }

    function simpan_edit() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $divisi = $this->input->post('divisi');
        if ($divisi < 10) {
            $div = '0' . $divisi;
        } else {
            $div = $divisi;
        }
//        echo 'dfs' . $div;
        $jabatan = $this->input->post('jabatan');

        if ($jabatan < 10) {
            $jab = '0' . $jabatan;
        } else {
            $jab = $jabatan;
        }
//        print_r($jab);
        $explod = explode('.', $this->input->post('username'));
        $id_user = $explod[2];
//        print_r($id_user);
        $username = $div . '.' . $jab . '.' . $id_user;
        echo 'un' . $username;
        $data['input'] = array(
            'nama' => $this->input->post('nama'),
            'username' => $username,
            'id_divisi' => $this->input->post('divisi'),
            "id_jabatan" => $this->input->post('jabatan'),
            'no_ktp' => $this->input->post('no_ktp'),
            'tanggal_masuk' => $this->input->post('tanggal_masuk'),
            'tanggal_akhir_kontrak' => $this->input->post('tanggal_akhir_kontrak'),
            "status_karyawan" => $this->input->post('status_karyawan'),
            'id_proyek' => $this->input->post('id_proyek')
        );
//        print_r($data['input']);
        if ($this->karyawan_m->update('karyawan', $data['input'], $this->input->post('id'))) {
            $data['input3'] = array(
                'user_name' => $username
            );
            $this->karyawan_m->update('app_user_role', $data['input3'], $this->input->post('id'));
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['data_karyawan'] = $this->karyawan_m->view('karyawan', array('id' => $id));
//        print_r($data['data_karyawan'][0]->username);
        if ($this->karyawan_m->delete('karyawan', array('id' => $id))) {
            echo "sukses";
            $this->karyawan_m->delete('app_user_role', array('user_name' => $data['data_karyawan'][0]->username));
        } else {
            echo "gagal";
        }
        redirect($data['urlnya']);
    }

    public function get_gapok() {
        $sijab = $this->input->post('sijab');
        $ex = explode('_', $sijab);
        $divisi = $ex[0];
        $jabatan = $ex[1];
        $status_karyawan = $ex[2];
//        $data_gapok = $this->karyawan_m->get_gapok($username);
        $data_gapok = $this->karyawan_m->view('golongan_gaji', array('id_divisi' => $divisi,
            'id_jabatan' => $jabatan,
            'status_karyawan' => $status_karyawan)
        );
        if ($data_gapok != NULL) {
            $data = $data_gapok[0]->gaji_pokok;
        } else {
            $data = '0';
        }
        echo $data;
    }

    public function get_tunjangan_proyek() {
        $id_proyek = $this->input->post('id_proyek');
        $data_tunjangan = $this->karyawan_m->view('tunjangan_proyek', array('id' => $id_proyek));
        if ($data_tunjangan != NULL) {
            $data = $data_tunjangan[0]->tunjangan_proyek;
        } else {
            $data = '0';
        }
        echo $data;
    }

    function cetak_data_karyawan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        @$data["data_karyawan"] = $this->karyawan_m->get_order_by_nama_karyawan('view_karyawan');
        $this->load->view('admin/karyawan/cetak_data_karyawan', $data);
    }

    function pdf_cetak_data_karyawan() {

        @$data["data_karyawan"] = $this->karyawan_m->get_order_by_nama_karyawan('view_karyawan');

        $this->load->view('admin/karyawan/pdf_cetak_data_karyawan', $data);
        // Get output html
        $html = $this->output->get_output();

        // Load library
        $this->load->library('dompdf_gen');

        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper(array(0, 0, 609.448818898, 935.433070866), 'landscape');
        $this->dompdf->render();
        $this->dompdf->stream("cetak_data_karyawan.pdf");
    }

    function validasi_hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['ambil_data'] = $this->karyawan_m->view('karyawan', array('id' => $id));
        $data['nama'] = $data['ambil_data'][0]->nama;
        $data['id'] = $id;
        $this->load->view('admin/karyawan/validasi_hapus', $data);
    }

}

