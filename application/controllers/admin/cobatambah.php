<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_admin.php');

class cobatambah extends base_admin {

    public function __construct() {
        parent:: __construct();
        $this->load->model('admin/cobatambah_m');
        session_start();
    }

    function index() {
        $data['page_title'] = 'Data Golongan Cobatambah';
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];

        
        $data["per_page"] = DEFAULT_PAGE;
        @$data["data_cobatambah"] = $this->cobatambah_m->get_order_by('view_cobatambah');
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default            
            '<script src="' . base_url('assets/js/jquery-ui.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/autocomplete_panitia.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/jquery.dataTables.min.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/dataTables.bootstrap.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
            '<script src="' . base_url('assets/js/jquery.validate.js') . '"></script>' . "\n" //validate
        );
        $data['scripts'] = $scripts;
        $data['popups_js'] = 'ada';
        $data['dataTable_js'] = 'ada';
//        echo 'ii';
        $this->load->view('admin/header', $datah);
        $this->load->view('admin/cobatambah/daftar_cobatambah', $data);
        $this->load->view('admin/footer');
    }

    function data_per_page($page) {

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data["per_page"] = $page;
        $data["panitia"] = $this->panitia_m->get_panitia('viewpanitia', $data["per_page"]);
        $this->load->view('ppk/panitia/data_panitia', $data);
    }

    function add_cobatambah() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        $datauser = $this->cobatambah_m->view('karyawan', array('username' => $datah['nip']));
        @$data["data_jabatan"] = $this->cobatambah_m->get_order_by_name('jabatan');
        @$data["data_divisi"] = $this->cobatambah_m->get_order_by_name_divisi('divisi');
        //added
        $data['gaji_pokok'] = array(
            'name' => 'gaji_pokok',
            'id' => 'gaji_pokok',
            'placeholder' => 'Masukkan gaji pokok dulu',
            'data-error-empty' => 'gaji pokok harus diisi',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['lembur_perjam'] = array(
            'name' => 'lembur_perjam',
            'id' => 'lembur_perjam',
            'placeholder' => 'Masukkan lembur perjam dulu',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );
        $this->load->view('admin/cobatambah/add_cobatambah', $data);
    }

    function edit($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        $data['cobatambah'] = $this->cobatambah_m->view('cobatambah', array('id' => $id));
//        print_r($data['cobatambah'][0]->status_karyawan);
        $data['status_karyawan'] = $data['cobatambah'][0]->status_karyawan;
        @$data["data_jabatan"] = $this->cobatambah_m->get_order_by_name('jabatan');
        @$data["data_divisi"] = $this->cobatambah_m->get_order_by_name_divisi('divisi');
        $data['gaji_pokok'] = array(
            'name' => 'gaji_pokok',
            'id' => 'gaji_pokok',
            'placeholder' => 'Masukkan gaji pokok dulu',
            'data-error-empty' => 'gaji pokok harus diisi',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data['cobatambah'][0]->gaji_pokok
        );
        $data['lembur_perjam'] = array(
            'name' => 'lembur_perjam',
            'id' => 'lembur_perjam',
            'placeholder' => 'Masukkan lembur perjam dulu',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data['cobatambah'][0]->lembur_perjam
        );
        $data['id'] = array(
            'name' => 'id',
            'id' => 'id',
            'class' => 'form-control',
            'type' => 'hidden',
            'value' => $data['cobatambah'][0]->id
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );

        $this->load->view('admin/cobatambah/edit_cobatambah', $data);
    }

    function simpan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['input'] = array(
            'status_karyawan' => $this->input->post('status_karyawan'),
            'id_jabatan' => $this->input->post('jabatan'),
            'id_divisi' => $this->input->post('divisi'),
            'gaji_pokok' => $this->input->post('gaji_pokok_unNum'),
            'lembur_perjam' => $this->input->post('lembur_perjam_unNum')
        );
        print_r($data['input']);
        if ($this->cobatambah_m->insert('cobatambah', $data['input'])) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function simpan_edit() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['input'] = array(
            'status_karyawan' => $this->input->post('status_karyawan'),
            'id_jabatan' => $this->input->post('jabatan'),
            'id_divisi' => $this->input->post('divisi'),
            'gaji_pokok' => $this->input->post('gaji_pokok_unNum'),
            'lembur_perjam' => $this->input->post('lembur_perjam_unNum')
        );
        print_r($data['input']);
        if ($this->cobatambah_m->update('cobatambah', $data['input'], $this->input->post('id'))) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        if ($this->cobatambah_m->delete('cobatambah', array('id' => $id))) {
            echo "sukses";
        } else {
            echo "gagal";
        }
        redirect($data['urlnya']);
    }

    function validasi_hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['id'] = $id;
        $this->load->view('admin/cobatambah/validasi_hapus', $data);
    }

    function cetak_data() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        @$data["data_order"] = $this->cobatambah_m->view_order('view_cobatambah', 'status_karyawan');
        $this->load->view('admin/cobatambah/cetak_data', $data);
    }

    function pdf_cetak() {

        @$data["data_order"] = $this->cobatambah_m->view_order('view_cobatambah', 'status_karyawan');
        $this->load->view('admin/cobatambah/pdf_cetak', $data);
        // Get output html
        $html = $this->output->get_output();

        // Load library
        $this->load->library('dompdf_gen');

        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper(array(0, 0, 609.448818898, 935.433070866), 'potrait');
        $this->dompdf->render();
        $this->dompdf->stream("cetak_cobatambah.pdf");
    }

}

