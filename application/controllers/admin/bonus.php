<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_admin.php');

class bonus extends base_admin {

    public function __construct() {
        parent:: __construct();
        $this->load->model('admin/bonus_m');
        session_start();
    }

    function index() {
        $data['page_title'] = 'Data Bonus';
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data["per_page"] = DEFAULT_PAGE;
        @$data["data_bonus"] = $this->bonus_m->get_order_by('view_bonus','nama_karyawan','ASC');
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default            
            '<script src="' . base_url('assets/js/jquery-ui.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/autocomplete_panitia.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/jquery.dataTables.min.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/dataTables.bootstrap.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
            '<script src="' . base_url('assets/js/jquery.validate.js') . '"></script>' . "\n" //validate
        );
        $data['scripts'] = $scripts;
        //isset javascript
        $data['popups_js'] = 'ada';
        $data['dataTable_js'] = 'ada';
        $this->load->view('admin/header', $datah);
        $this->load->view('admin/bonus/daftar_bonus', $data);
        $this->load->view('admin/footer');
    }

    function add_bonus() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        @$data["username_karyawan"] = $this->bonus_m->get_order_by('karyawan','nama','ASC');
        //added
        //bulan
        $data['bulan'] = date('n');
        $this->session->set_userdata('bulan', date('n'));
        $data['bul'] = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

        $data['nama'] = array(
            'name' => 'nama',
            'id' => 'nama',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['divisi'] = array(
            'name' => 'divisi',
            'id' => 'divisi',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['jabatan'] = array(
            'name' => 'jabatan',
            'id' => 'jabatan',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['nama_bonus'] = array(
            'name' => 'nama_bonus',
            'id' => 'nama_bonus',
            'placeholder' => 'Masukkan nama bonus',
            'data-error-empty' => 'Nama potonagn harus diisi',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['jumlah_bonus'] = array(
            'name' => 'jumlah_bonus',
            'id' => 'jumlah_bonus',
            'placeholder' => 'Masukkan jumlah bonus',
            'data-error-empty' => 'Jumlah bonus harus diisi',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );
        $this->load->view('admin/bonus/add_bonus', $data);
    }

    function simpan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $username = $this->input->post('username');
        $data['id_karyawan'] = $this->bonus_m->view('karyawan', array('username' => $username));

        $data['input'] = array(
            'id_karyawan' => $data['id_karyawan'][0]->id,
            'nama_bonus' => $this->input->post('nama_bonus'),
            'jumlah_bonus' => $this->input->post('jumlah_bonus_unNum'),
            'periode' => $this->input->post('periode'),
            'tanggal' => date('Y-m-d')
        );
        print_r($data['input']);
        if ($this->bonus_m->insert('bonus', $data['input'])) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function edit($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
//        $datauser = $this->bonus_m->view('pegawai', array('nip' => $datah['nip']));
        $data_bonus = $this->bonus_m->view('view_bonus', array('id' => $id));
        //bulan
        $data['bulan'] = date('n');
        $this->session->set_userdata('bulan', date('n'));
        $data['bul'] = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        $data['periode'] = $data_bonus[0]->periode;
        //added
        $data['username'] = array(
            'name' => 'username',
            'id' => 'autocomplete',
            'placeholder' => 'Masuakan username karyawan',
            'class' => 'form-control',
            'type' => 'text',
            'disabled' => 'disabled',
            'value' => $data_bonus[0]->username
        );
        //
        $data['nama'] = array(
            'name' => 'nama',
            'id' => 'nama',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data_bonus[0]->nama_karyawan
        );
        $data['divisi'] = array(
            'name' => 'divisi',
            'id' => 'divisi',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data_bonus[0]->nama_divisi
        );
        $data['jabatan'] = array(
            'name' => 'jabatan',
            'id' => 'jabatan',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data_bonus[0]->nama_jabatan
        );
        $data['nama_bonus'] = array(
            'name' => 'nama_bonus',
            'id' => 'nama_bonus',
            'placeholder' => 'Masukkan nama bonus',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data_bonus[0]->nama_bonus
        );
        $data['jumlah_bonus'] = array(
            'name' => 'jumlah_bonus',
            'id' => 'jumlah_bonus',
            'placeholder' => 'Masukkan jumlah bonus',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data_bonus[0]->jumlah_bonus
        );
        $data['jumlah_bonus_unNum'] = array(
            'name' => 'jumlah_bonus_unNum',
            'id' => 'jumlah_bonus_unNum',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data_bonus[0]->jumlah_bonus
        );

        $data['id'] = array(
            'name' => 'id',
            'id' => 'id',
            'class' => 'form-control',
            'type' => 'hidden',
            'value' => $data_bonus[0]->id
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );

        $this->load->view('admin/bonus/edit_bonus', $data);
    }

    function simpan_edit() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['input'] = array(
            'nama_bonus' => $this->input->post('nama_bonus'),
            'jumlah_bonus' => $this->input->post('jumlah_bonus_unNum'),
            'periode' => $this->input->post('periode'),
        );
        print_r($data['input']);
        if ($this->bonus_m->update('bonus', $data['input'], $this->input->post('id'))) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        if ($this->bonus_m->delete('bonus', array('id' => $id))) {
            echo "sukses";
        } else {
            echo "gagal";
        }
        redirect($data['urlnya']);
    }

    public function get_data_karyawan() {
        $username = $this->input->post('username');
        $data_karyawan = $this->bonus_m->get_data_karyawan($username);
        foreach ($data_karyawan as $u) {
            $data = "$u[nama_karyawan],$u[nama_divisi],$u[nama_jabatan]";
//            $data = "$u";
        }
        echo $data;
    }

    function validasi_hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['ambil_data'] = $this->bonus_m->view('view_bonus', array('id' => $id));
        $data['nama'] = $data['ambil_data'][0]->nama_karyawan;
        $data['id'] = $id;
        $this->load->view('admin/bonus/validasi_hapus', $data);
    }

    function cetak_data() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        @$data["data_order"] = $this->bonus_m->view_order('view_bonus', 'nama_karyawan');
        $this->load->view('admin/bonus/cetak_data', $data);
    }

    function pdf_cetak() {

        @$data["data_order"] = $this->bonus_m->view_order('view_bonus', 'nama_karyawan');
        $this->load->view('admin/bonus/pdf_cetak', $data);
        // Get output html
        $html = $this->output->get_output();

        // Load library
        $this->load->library('dompdf_gen');

        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper(array(0, 0, 609.448818898, 935.433070866), 'potrait');
        $this->dompdf->render();
        $this->dompdf->stream("cetak_bonus.pdf");
    }

}
