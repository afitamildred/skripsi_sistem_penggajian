<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_admin.php');

class divisi extends base_admin {

    public function __construct() {
        parent:: __construct();
        $this->load->model('admin/divisi_m');
        session_start();
    }

    function index() {
        $data['page_title'] = 'Data Divisi';
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];

        $data["per_page"] = DEFAULT_PAGE;
        @$data["data_divisi"] = $this->divisi_m->get_order_by('divisi','nama_divisi');
//        echo 'dsf';
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default            
            '<script src="' . base_url('assets/js/jquery-ui.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/autocomplete_panitia.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/jquery.dataTables.min.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/dataTables.bootstrap.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
            '<script src="' . base_url('assets/js/jquery.validate.js') . '"></script>' . "\n" //validate
        );
        $data['scripts'] = $scripts;
        $data['popups_js'] = 'ada';
        $data['dataTable_js'] = 'ada';
        $this->load->view('admin/header', $datah);
        $this->load->view('admin/divisi/daftar_divisi', $data);
        $this->load->view('admin/footer');
    }

    function add_divisi() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        $data['nama_divisi'] = array(
            'name' => 'nama_divisi',
            'id' => 'nama_divisi',
            'placeholder' => 'Masukan nama divisi',
            'class' => 'form-control',
            'type' => 'text',
        );

        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );
        if ($this->input->post('targetData') == false) {
            $this->load->view('admin/divisi/add_divisi', $data);
        } else {
            $nama = $this->input->post('nama');

            $data['input'] = array(
                'id_karyawan' => $this->input->post('id_karyawan'),
                'jumlah_hadir' => $this->input->post('jumlah_hadir'),
                'total_harus_masuk' => $this->input->post('total_harus_masuk'),
                'potongan' => $this->input->post('potongan')
            );

            if (empty($nomer_sk) || $nama == "Panitia mungkin telah terpilih atau tidak ada data.") {
                return false;
            } else {
                $this->divisi_m->insert('divisi', $data['input']);
            }
        }
    }

    function simpan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['input'] = array(
            'nama_divisi' => $this->input->post('nama_divisi')
        );
        print_r($data['input']);
        if ($this->divisi_m->insert('divisi', $data['input'])) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function edit($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        $data_divisi = $this->divisi_m->view('divisi', array('id' => $id));
        //added
        $data['nama_divisi'] = array(
            'name' => 'nama_divisi',
            'id' => 'nama_divisi',
            'placeholder' => 'Masukan nama divisi',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data_divisi[0]->nama_divisi
        );
        //

        $data['id'] = array(
            'name' => 'id',
            'id' => 'id',
            'class' => 'form-control',
            'type' => 'hidden',
            'value' => $data_divisi[0]->id
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );

        $this->load->view('admin/divisi/edit_divisi', $data);
    }

    function simpan_edit() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['input'] = array(
            'nama_divisi' => $this->input->post('nama_divisi')
        );
//        print_r($data['input']);
        if ($this->divisi_m->update('divisi', $data['input'], $this->input->post('id'))) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        if ($this->divisi_m->delete('divisi', array('id' => $id))) {
            echo "sukses";
        } else {
            echo "gagal";
        }
        redirect($data['urlnya']);
    }

    function validasi_hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['ambil_data'] = $this->divisi_m->view('divisi', array('id' => $id));
        $data['nama'] = $data['ambil_data'][0]->nama_divisi;
        $data['id'] = $id;
        $this->load->view('admin/divisi/validasi_hapus', $data);
    }

    function cetak_data() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        @$data["data_order"] = $this->divisi_m->view_order('divisi', 'nama_divisi');
        $this->load->view('admin/divisi/cetak_data', $data);
    }

    function pdf_cetak() {

        @$data["data_order"] = $this->divisi_m->view_order('divisi', 'nama_divisi');
        $this->load->view('admin/divisi/pdf_cetak', $data);
        // Get output html
        $html = $this->output->get_output();

        // Load library
        $this->load->library('dompdf_gen');

        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper(array(0, 0, 609.448818898, 935.433070866), 'potrait');
        $this->dompdf->render();
        $this->dompdf->stream("cetak_divisi.pdf");
    }

}
