<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_admin.php');

class lembur extends base_admin {

    public function __construct() {
        parent:: __construct();

        $this->load->model('admin/lembur_m');
        session_start();
    }

    function index() {
        $data['page_title'] = 'Data Lembur';

        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];

        $data["per_page"] = DEFAULT_PAGE;
        @$data["data_lembur"] = $this->lembur_m->get_order_by('view_lembur', 'nama_karyawan', 'ASC');
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default            
            '<script src="' . base_url('assets/js/jquery-ui.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/autocomplete_panitia.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/jquery.dataTables.min.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/dataTables.bootstrap.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
            '<script src="' . base_url('assets/js/jquery.validate.js') . '"></script>' . "\n" //validate
        );
        $data['scripts'] = $scripts;
        $data['popups_js'] = 'ada';
        $data['dataTable_js'] = 'ada';
        $this->load->view('admin/header', $datah);
        $this->load->view('admin/lembur/daftar_lembur', $data);
        $this->load->view('admin/footer');
    }

    function data_per_page($page) {

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data["per_page"] = $page;
        $data["panitia"] = $this->panitia_m->get_panitia('viewpanitia', $data["per_page"]);
        $this->load->view('ppk/panitia/data_panitia', $data);
    }

    function add_lembur() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];

//        @$data["username_karyawan"] = $this->lembur_m->get_order_by_username_karyawan('view_karyawan_lembur_blm_dipilih', array('id_karyawan' => null));
        @$data["username_karyawan"] = $this->lembur_m->get_order_by('karyawan', 'nama', 'ASC');
        //bulan
        $data['bulan'] = date('n');
        $this->session->set_userdata('bulan', date('n'));
        $data['bul'] = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        $datauser = $this->
                lembur_m->view('karyawan', array('username' => $datah['nip']));

        $data['nama'] = array(
            'name' => 'nama',
            'id' => 'nama',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'label',
        );
        $data['divisi'] = array(
            'name' => 'divisi',
            'id' => 'divisi',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['jabatan'] = array(
            'name' => 'jabatan',
            'id' => 'jabatan',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['tanggal_lembur'] = array(
            'name' => 'tanggal_lembur',
            'id' => 'tanggal_lembur',
            'placeholder' => 'Masukkan tanggal lembur dulu',
            'class' => 'form-control datetimepicker',
            'type' => 'text'
        );
        $data['jumlah_jam'] = array(
            'name' => 'jumlah_jam',
            'id' => 'jumlah_jam',
            'placeholder' => 'Masukkan jumlah jam lembur',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['jumlah_bonus'] = array(
            'name' => 'jumlah_bonus',
            'id' => 'jumlah_bonus',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'type' => 'text',
            'disabled' => 'disabled'
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery.datetimepicker.js') . '"></script>' . "\n", //datepicker
        );
        $data['scripts'] = $scripts;
        $this->load->view('admin/lembur/add_lembur', $data);
    }

    function edit($id) {
        $data['role'] = $this->uri->segment(1); //
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $id_kary = $this->lembur_m->view('lembur', array('id' => $id));
        //bulan
        $data['bulan'] = date('n');
        $this->session->set_userdata('bulan', date('n'));
        $data['bul'] = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");


        $gol_gaji = $this->lembur_m->view('karyawan', array('id' => $id_kary[0]->id_karyawan));
        $divisi = $gol_gaji[0]->id_divisi;
        $jabatan = $gol_gaji[0]->id_jabatan;
        $status_kary = $gol_gaji[0]->status_karyawan;


        $data_lemburd = $this->lembur_m->view('golongan_gaji', array(
            'id_divisi' => $divisi,
            'id_jabatan' => $jabatan,
            'status_karyawan' => $status_kary
                )
        );
        $data_lembur = $this->lembur_m->view('view_lembur', array('id' => $id));
        $data['lembur_perjam'] = $data_lemburd[0]->lembur_perjam;
        $data['jml_bonus'] = $data_lembur[0]->jumlah_bonus;
        $data['data_lembur'] = $this->lembur_m->view('view_lembur', array('id' => $id));
        $data['periode'] = $data_lembur[0]->periode;
//        print_r($data['periode']);
        $data['usernamenya'] = $data_lembur[0]->username;

        $data['username'] = array(
            'name' => 'username',
            'id' => 'autocomplete',
            'placeholder' => 'Masuakan username karyawan', 'class' => 'form-control',
            'type' => 'text',
            'disabled' => 'disabled',
            'value' => $data_lembur[0]->username
        );
        //
        $data['nama'] = array(
            'name' => 'nama',
            'id' => 'nama',
            'placeholder' => 'Data terisi otomatis', 'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data_lembur[0]->nama_karyawan
        );
        $data['divisi'] = array(
            'name' => 'divisi',
            'id' => 'divisi',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data_lembur[0]->nama_divisi
        );
        $data['jabatan'] = array(
            'name' => 'jabatan',
            'id' => 'jabatan',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data_lembur[0]->nama_jabatan
        );
        $data['tanggal_lembur'] = array(
            'name' => 'tanggal_lembur',
            'id' => 'tanggal_lembur',
            'placeholder' => 'Masukkan tanggal lembur dulu',
            'class' => 'form-control datetimepicker',
            'type' => 'text',
            'value' => $data_lembur[0]->tanggal_lembur
        );
        $data['jumlah_jam'] = array(
            'name' => 'jumlah_jam',
            'id' => 'jumlah_jam',
            'placeholder' => 'Masukkan jumlah jam lembur',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data_lembur[0]->jumlah_jam
        );
        $data['jml'] = array(
            'name' => 'jml',
            'id' => 'jml',
            'class' => 'form-control',
            'type' => 'text',
            'readonly' => 'readonly',
            'value' => $data_lemburd[0]->lembur_perjam
        );
        $data['jumlah_bonus'] = array(
            'name' => 'jumlah_bonus',
            'id' => 'jumlah_bonus',
            'class' => 'form-control',
            'type' => 'text',
            'readonly' => 'readonly',
            'value' => $data_lembur[0]->jumlah_bonus
        );

        $data['id'] = array(
            'name' => 'id',
            'id' => 'id',
            'class' => 'form-control',
            'type' => 'hidden',
            'value' => $data_lembur[0]->id
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );

        $scripts = array(
            '<script src="' . base_url('assets/js/jquery.datetimepicker.js') . '"></script>' . "\n", //datepicker
        );
        $data['scripts'] = $scripts;
        $this->load->view('admin/lembur/edit_lembur', $data);
    }

    function simpan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $username = $this->input->post('username');
        $data['id_karyawan'] = $this->lembur_m->view('karyawan', array('username' => $username));


        $data_kary = $this->lembur_m->view('view_data_karyawan', array('username' => $username));
        $divisi = $data_kary[0]->id_divisi;
        $jabatan = $data_kary[0]->id_jabatan;
        $status_kary = $data_kary[0]->status_karyawan;
        $data['jml_bonus'] = $this->lembur_m->view('golongan_gaji', array(
            'id_divisi' => $divisi,
            'id_jabatan' => $jabatan,
            'status_karyawan' => $status_kary
                )
        );
        $jml_bonus = $data['jml_bonus'][0]->lembur_perjam * $this->input->post('jumlah_jam');

        $data['input'] = array(
            'id_karyawan' => $data['id_karyawan'][0]->id,
            'tanggal_lembur' => $this->input->post('tanggal_lembur'),
            'jumlah_bonus' => $jml_bonus,
            'jumlah_jam' => $this->input->post('jumlah_jam'),
            'periode' => $this->input->post('periode'),
            'tanggal' => date('Y-m-d')
//            'id_golongan_lembur' => $this->input->post('id_golongan_lembur')
        );
        print_r($data['input']);
        if ($this->lembur_m->insert('lembur', $data['input'])) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function simpan_edit($un) {
//        print_r($un);
        $data['role'] = $this->uri->segment(1); //
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data_kary = $this->lembur_m->view('karyawan', array('username' => $un));

        $divisi = $data_kary[0]->id_divisi;
        $jabatan = $data_kary[0]->id_jabatan;
        $status_kary = $data_kary[0]->status_karyawan;
        $data['jml_bonus'] = $this->lembur_m->view('golongan_gaji', array(
            'id_divisi' => $divisi,
            'id_jabatan' => $jabatan,
            'status_karyawan' => $status_kary
                )
        );
        $jml_bonus = $data['jml_bonus'][0]->lembur_perjam * $this->input->post('jumlah_jam');

        $data['input'] = array(
            'tanggal_lembur' => $this->input->post('tanggal_lembur'),
            'jumlah_bonus' => $jml_bonus,
            'jumlah_jam' => $this->input->post('jumlah_jam'),
            'periode' => $this->input->post('periode'),
        );
        if ($this->lembur_m->update('lembur', $data['input'], $this->input->post('id'))) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        if ($this->lembur_m->delete('lembur', array('id' => $id))) {
            echo "sukses";
        } else {
            echo "gagal";
        }
        redirect($data['urlnya']);
    }

    public function get_data_karyawan() {
        $username = $this->input->post('username');
        $data_kary = $this->lembur_m->view('view_data_karyawan', array('username' => $username));

        $divisi = $data_kary[0]->id_divisi;
        $jabatan = $data_kary[0]->id_jabatan;
        $status_kary = $data_kary[0]->status_karyawan;
        $data_gapok = $this->lembur_m->view('golongan_gaji', array(
            'id_divisi' => $divisi,
            'id_jabatan' => $jabatan,
            'status_karyawan' => $status_kary
                )
        );

        $data_karyawan = $this->lembur_m->get_data_karyawan($username);
        foreach ($data_karyawan as $u) {
            $data = "$u[nama_karyawan],$u[nama_divisi] ,$u[nama_jabatan] ";
        }
        echo $data . ',' . $data_gapok[0]->lembur_perjam;
    }

    function validasi_hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['ambil_data'] = $this->lembur_m->view('view_lembur', array('id' => $id));
        $data['nama'] = $data['ambil_data'][0]->nama_karyawan;
        $data['id'] = $id;
        $this->load->view('admin/lembur/validasi_hapus', $data);
    }

    function cetak_data() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        @$data["data_order"] = $this->lembur_m->view_order('view_lembur', 'nama_karyawan');
        $this->load->view('admin/lembur/cetak_data', $data);
    }

    function pdf_cetak() {

        @$data["data_order"] = $this->lembur_m->view_order('view_lembur', 'nama_karyawan');
        $this->load->view('admin/lembur/pdf_cetak', $data);
        // Get output html
        $html = $this->output->get_output();

        // Load library
        $this->load->library('dompdf_gen');

        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper(array(0, 0, 609.448818898, 935.433070866), 'potrait');
        $this->dompdf->render();
        $this->dompdf->stream("cetak_lembur.pdf");
    }

}

