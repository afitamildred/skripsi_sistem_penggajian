<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_admin.php');

class dashboard extends base_admin {

    public function __construct() {
        parent:: __construct();
        $this->load->model('admin_m');
        session_start();
    }

    function index() {
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
        );
        $data['scripts'] = $scripts;
        //isset javascript
        $data['popups_js'] = 'ada';
        $this->load->view('admin/header', $datah);
        $this->load->view('admin/dashboard', $data);
        $this->load->view('admin/footer');
    }

}
