<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_admin.php');

class tunjangan_proyek extends base_admin {

    public function __construct() {
        parent:: __construct();
        $this->load->model('admin/tunjangan_proyek_m');
        session_start();
    }

    function index() {
//        echo 'fds';
        $data['page_title'] = 'Data Tunjangan Proyek';
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data["per_page"] = DEFAULT_PAGE;
        @$data["data_tunjangan_proyek"] = $this->tunjangan_proyek_m->get_order_by('tunjangan_proyek');
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default            
            '<script src="' . base_url('assets/js/jquery-ui.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/autocomplete_panitia.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/jquery.dataTables.min.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/dataTables.bootstrap.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
            '<script src="' . base_url('assets/js/jquery.validate.js') . '"></script>' . "\n" //validate
        );
        $data['scripts'] = $scripts;
        //isset javascript
        $data['popups_js'] = 'ada';
        $data['dataTable_js'] = 'ada';
        $this->load->view('admin/header', $datah);
        $this->load->view('admin/tunjangan_proyek/daftar_tunjangan_proyek', $data);
        $this->load->view('admin/footer');
    }

    function data_per_page($page) {

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data["per_page"] = $page;
        $data["tunjangan_proyek"] = $this->tunjangan_proyek_m->get_tunjangan_proyek('viewtunjangan_proyek', $data["per_page"]);
        $this->load->view('ppk/tunjangan_proyek/data_tunjangan_proyek', $data);
    }

    function add_tunjangan_proyek() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['nama_proyek'] = array(
            'name' => 'nama_proyek',
            'id' => 'nama_proyek',
            'placeholder' => 'Masukan nama proyek',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['tunjangan_proyek'] = array(
            'name' => 'tunjangan_proyek',
            'id' => 'tunjangan_proyek',
            'placeholder' => 'Masukan jumlah tunjangan',
            'class' => 'form-control',
            'type' => 'text',
        );

        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );
        $this->load->view('admin/tunjangan_proyek/add_tunjangan_proyek', $data);
    }

    function simpan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];

        $data['input'] = array(
            'nama_proyek' => $this->input->post('nama_proyek'),
            'tunjangan_proyek' => $this->input->post('tunjangan_proyek_unNum')
        );
        print_r($data['input']);
        if ($this->tunjangan_proyek_m->insert('tunjangan_proyek', $data['input'])) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function edit($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        $data_tunjangan_proyek = $this->tunjangan_proyek_m->view('tunjangan_proyek', array('id' => $id));
        //added

        $data['nama_proyek'] = array(
            'name' => 'nama_proyek',
            'id' => 'nama_proyek',
            'placeholder' => 'Masukan nama proyek',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data_tunjangan_proyek[0]->nama_proyek
        );
        $data['tunjangan_proyek'] = array(
            'name' => 'tunjangan_proyek',
            'id' => 'tunjangan_proyek',
            'placeholder' => 'Masukan jumlah tunjangan',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data_tunjangan_proyek[0]->tunjangan_proyek
        );

        $data['id'] = array(
            'name' => 'id',
            'id' => 'id',
            'class' => 'form-control',
            'type' => 'hidden',
            'value' => $data_tunjangan_proyek[0]->id
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );

        $this->load->view('admin/tunjangan_proyek/edit_tunjangan_proyek', $data);
    }

    function simpan_edit() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['input'] = array(
            'nama_proyek' => $this->input->post('nama_proyek'),
            'tunjangan_proyek' => $this->input->post('tunjangan_proyek_unNum')
        );
        if ($this->tunjangan_proyek_m->update('tunjangan_proyek', $data['input'], $this->input->post('id'))) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        if ($this->tunjangan_proyek_m->delete('tunjangan_proyek', array('id' => $id))) {
            echo "sukses";
        } else {
            echo "gagal";
        }
        redirect($data['urlnya']);
    }

    public function get_data_karyawan() {
        $username = $this->input->post('username');
        $data_karyawan = $this->tunjangan_proyek_m->get_data_karyawan($username);
        foreach ($data_karyawan as $u) {
            $data = "$u[nama_karyawan],$u[nama_divisi],$u[nama_jabatan]";
        }
        echo $data;
    }

    function validasi_hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['ambil_data'] = $this->tunjangan_proyek_m->view('tunjangan_proyek', array('id' => $id));
        $data['nama'] = $data['ambil_data'][0]->nama_proyek;
        $data['id'] = $id;
        $this->load->view('admin/tunjangan_proyek/validasi_hapus', $data);
    }

    function cetak_data() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        @$data["data_order"] = $this->tunjangan_proyek_m->view_order('tunjangan_proyek', 'nama_proyek');
        $this->load->view('admin/tunjangan_proyek/cetak_data', $data);
    }

    function pdf_cetak() {

        @$data["data_order"] = $this->tunjangan_proyek_m->view_order('tunjangan_proyek', 'nama_proyek');
        $this->load->view('admin/tunjangan_proyek/pdf_cetak', $data);
        // Get output html
        $html = $this->output->get_output();

        // Load library
        $this->load->library('dompdf_gen');

        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper(array(0, 0, 609.448818898, 935.433070866), 'potrait');
        $this->dompdf->render();
        $this->dompdf->stream("cetak_tunjangan_proyek.pdf");
    }

}
