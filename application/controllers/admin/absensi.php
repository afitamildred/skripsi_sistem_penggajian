<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_admin.php');

class absensi extends base_admin {

    public function __construct() {
        parent:: __construct();
        $this->load->model('admin/absensi_m');
        session_start();
    }

    function index() {
        $data['page_title'] = 'Data Absensi';
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];

        $data["per_page"] = DEFAULT_PAGE;
        @$data["data_absensi"] = $this->absensi_m->get_order_by('view_absensi', 'nama_karyawan', 'ASC');
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default            
            '<script src="' . base_url('assets/js/jquery-ui.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/autocomplete_panitia.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/jquery.dataTables.min.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/dataTables.bootstrap.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
            '<script src="' . base_url('assets/js/jquery.validate.js') . '"></script>' . "\n" //validate
        );
        $data['scripts'] = $scripts;
        $data['popups_js'] = 'ada';
        $data['dataTable_js'] = 'ada';
        $this->load->view('admin/header', $datah);
        $this->load->view('admin/absensi/daftar_absensi', $data);
        $this->load->view('admin/footer');
    }

    function add_absensi() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
//        @$data["username_karyawan"] = $this->absensi_m->get_order_by_username_karyawan('view_karyawan_absensi_blm_dipilih', array('id_karyawan' => null));
        @$data["username_karyawan"] = $this->absensi_m->get_order_by('karyawan','nama','ASC');
        
//added
        //bulan
        $data['bulan'] = date('n');
        $this->session->set_userdata('bulan', date('n'));
        $data['bul'] = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

        $data['nama'] = array(
            'name' => 'nama',
            'id' => 'nama',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['divisi'] = array(
            'name' => 'divisi',
            'id' => 'divisi',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['jabatan'] = array(
            'name' => 'jabatan',
            'id' => 'jabatan',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['gapok_tunj'] = array(
            'name' => 'gapok_tunj',
            'id' => 'gapok_tunj',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['potongan'] = array(
            'name' => 'potongan',
            'id' => 'potongan',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
        );
        $data['val'] = 'aktif';
        $data['jumlah_harus_hadir'] = array(
            'name' => 'jumlah_harus_hadir',
            'id' => 'jumlah_harus_hadir',
            'placeholder' => 'Masukkan jumlah harus hadir',
            'data-error-empty' => 'Jumlah harus hadir harus diisi',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['jumlah_hadir'] = array(
            'name' => 'jumlah_hadir',
            'id' => 'jumlah_hadir',
            'placeholder' => 'Masukkan jumlah hadir',
            'data-error-empty' => 'Jumlah hadir harus diisi',
            'class' => 'form-control',
            'type' => 'text',
        );
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => ' height: 37px;
        margin-top: 12px;
        ',
            'value' => 'Simpan'
        );
        $this->load->view('admin/absensi/add_absensi', $data);
    }

    function simpan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $username = $this->input->post('username');
        $data['id_karyawan'] = $this->absensi_m->view('karyawan', array('username' => $username));

        $data['input'] = array(
            'id_karyawan' => $data['id_karyawan'][0]->id,
            'jumlah_harus_hadir' => $this->input->post('jumlah_harus_hadir'),
            'jumlah_hadir' => $this->input->post('jumlah_hadir'),
            'potongan' => $this->input->post('potongan_unNum'),
            'periode' => $this->input->post('periode'),
            'tanggal' => date('Y-m-d')
        );
        print_r($data['input']);
        if ($this->absensi_m->insert('absensi', $data['input'])) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function edit($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        $data_absensi = $this->absensi_m->view('view_absensi', array('id' => $id));

        //bulan
        $data['bulan'] = date('n');
        $this->session->set_userdata('bulan', date('n'));
        $data['bul'] = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        $data['periode'] = $data_absensi[0]->periode;
        //CARI GAPOK
        $data['data_karyawan'] = $this->absensi_m->view('karyawan', array('id' => $data_absensi[0]->id_karyawan));

        $divisi = $data['data_karyawan'][0]->id_divisi;
        $jabatan = $data['data_karyawan'][0]->id_jabatan;
        $status_karyawan = $data['data_karyawan'][0]->status_karyawan;
//        $data_gapok = $this->karyawan_m->get_gapok($username);
        $data_gapok = $this->absensi_m->view('golongan_gaji', array('id_divisi' => $divisi,
            'id_jabatan' => $jabatan,
            'status_karyawan' => $status_karyawan)
        );

        $tunj_proyek = $this->absensi_m->view('tunjangan_proyek', array('id' => $data['data_karyawan'][0]->id_proyek)
        );
//        print_r($tunj_proyek);
        //added
        $data['gapok_tunj'] = array(
            'name' => 'gapok_tunj',
            'id' => 'gapok_tunj',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $tunj_proyek[0]->tunjangan_proyek
        );
        $data['username'] = array(
            'name' => 'username',
            'id' => 'autocomplete',
            'placeholder' => 'Masuakan username karyawan',
//            'data-error-empty' => 'Username harus diisi',
            'class' => 'form-control',
            'type' => 'text',
            'disabled' => 'disabled',
            'value' => $data_absensi[0]->username
        );
        //
        $data['nama'] = array(
            'name' => 'nama',
            'id' => 'nama',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data_absensi[0]->nama_karyawan
        );
        $data['divisi'] = array(
            'name' => 'divisi',
            'id' => 'divisi',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data_absensi[0]->nama_divisi
        );
        $data['jabatan'] = array(
            'name' => 'jabatan',
            'id' => 'jabatan',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data_absensi[0]->nama_jabatan
        );
        $data['potongan'] = array(
            'name' => 'potongan',
            'id' => 'potongan',
            'placeholder' => 'Data terisi otomatis',
            'class' => 'form-control',
            'readonly' => 'readonly',
            'type' => 'text',
            'value' => $data_absensi[0]->potongan
        );
        $data['val'] = 'aktif';
        $data['jumlah_harus_hadir'] = array(
            'name' => 'jumlah_harus_hadir',
            'id' => 'jumlah_harus_hadir',
            'placeholder' => 'Masukkan jumlah harus hadir',
            'data-error-empty' => 'Jumlah harus hadir harus diisi',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data_absensi[0]->jumlah_harus_hadir
        );
        $data['jumlah_hadir'] = array(
            'name' => 'jumlah_hadir',
            'id' => 'jumlah_hadir',
            'placeholder' => 'Masukkan jumlah hadir',
            'data-error-empty' => 'Jumlah hadir harus diisi',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data_absensi[0]->jumlah_hadir
        );

        $data['id'] = array(
            'name' => 'id',
            'id' => 'id',
            'class' => 'form-control',
            'type' => 'hidden',
            'value' => $data_absensi[0]->id
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => ' height: 37px;
        margin-top: 12px;
        ',
            'value' => 'Simpan'
        );

        $this->load->view('admin/absensi/edit_absensi', $data);
    }

    function simpan_edit() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['input'] = array(
            'jumlah_harus_hadir' => $this->input->post('jumlah_harus_hadir'),
            'jumlah_hadir' => $this->input->post('jumlah_hadir'),
            'periode' => $this->input->post('periode'),
            'potongan' => $this->input->post('potongan_unNum')
        );
        print_r($data['input']);
        if ($this->absensi_m->update('absensi', $data['input'], $this->input->post('id'))) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        if ($this->absensi_m->delete('absensi', array('id' => $id))) {
            echo "sukses";
        } else {
            echo "gagal";
        }
        redirect($data['urlnya']);
    }

    public function get_data_karyawan() {
        $username = $this->input->post('username');
        $data_kary = $this->absensi_m->view('karyawan', array('username' => $username));

        $divisi = $data_kary[0]->id_divisi;
        $jabatan = $data_kary[0]->id_jabatan;
        $data_gapok = $this->absensi_m->view('golongan_gaji', array('id_divisi' => $divisi, 'id_jabatan' => $jabatan));

        $data_karyawan = $this->absensi_m->get_data_karyawan($username);
        foreach ($data_karyawan as $u) {
            $data = "$u[nama_karyawan],$u[nama_divisi],$u[nama_jabatan],$u[tunjangan_proyek]";
//            $data = "$u";
        }
        echo $data . ', ' . $data_gapok[0]->gaji_pokok;
    }

    function validasi_hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['ambil_data'] = $this->absensi_m->view('view_absensi', array('id' => $id));
        $data['nama'] = $data['ambil_data'][0]->nama_karyawan;
        $data['id'] = $id;
        $this->load->view('admin/absensi/validasi_hapus', $data);
    }

    function cetak_data() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        @$data["data_order"] = $this->absensi_m->view_order('view_absensi', 'nama_karyawan');
        $this->load->view('admin/absensi/cetak_data', $data);
    }

    function pdf_cetak() {

        @$data["data_order"] = $this->absensi_m->view_order('view_absensi', 'nama_karyawan');
        $this->load->view('admin/absensi/pdf_cetak', $data);
        // Get output html
        $html = $this->output->get_output();

        // Load library
        $this->load->library('dompdf_gen');

        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper(array(0, 0, 609.448818898, 935.433070866), 'potrait');
        $this->dompdf->render();
        $this->dompdf->stream("cetak_absensi.pdf");
    }

}
