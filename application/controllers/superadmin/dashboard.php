<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_superadmin.php');

class dashboard extends Base_superadmin {

    public function __construct() {
        parent:: __construct();
        $this->load->model('superadmin_m');
        session_start();
    }

    function index() {
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
        );
        $data['scripts'] = $scripts;
        //isset javascript
        $data['popups_js'] = 'ada';
        $this->load->view('superadmin/header', $datah);
        $this->load->view('superadmin/dashboard', $data);
        $this->load->view('superadmin/footer');
    }

    function data_admin() {
        $data['page_title'] = 'Data Admin';
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
//        $datauser = $this->gaji_m->view('pegawai', array('nip' => $datah['nip']));
//        $datah['user'] = $datauser[0]->nama; //session user logged
//        $datah['avatar'] = $datauser[0]->avatar; //session user logged

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];

//        $datappk = $this->gaji_m->view('ppk', array('username' => $datah['nip']));
        $data["per_page"] = DEFAULT_PAGE;
//        @$data["data_admin"] = $this->superadmin_m->get_order_by('admin');
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default            
            '<script src="' . base_url('assets/js/jquery-ui.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/autocomplete_panitia.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/jquery.dataTables.min.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/dataTables.bootstrap.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
            '<script src="' . base_url('assets/js/jquery.validate.js') . '"></script>' . "\n" //validate
        );
        $data['scripts'] = $scripts;
        //isset javascript
        $data['popups_js'] = 'ada';
        $data['dataTable_js'] = 'ada';
        if ($this->input->get('data') == 'reload') { // untuk reload di ajax. jdi gk perlu refresh halaman. automatis. jng di hapus
            $this->load->view('superadmin/admin/data_admin', $data);
        } else {
            $this->load->view('superadmin/header', $datah);
            $this->load->view('superadmin/admin/daftar_admin', $data);
            $this->load->view('superadmin/footer');
        }
    }

    function tambah_admin() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
//        $data['nip_pegawai'] = $this->gaji_m->get_pegawai();
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
//        echo 's'.$datah['nip'];
//        $datauser = $this->superadmin_->view('pegawai', array('nip' => $datah['nip']));
        //added
        //
        $data['nama_admin'] = array(
            'name' => 'nama_admin',
            'id' => 'nama_admin',
            'placeholder' => 'Masukan nama admin',
            'class' => 'form-control',
            'type' => 'text',
        );

        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );
        if ($this->input->post('targetData') == false) {
            $this->load->view('superadmin/header');
            $this->load->view('superadmin/admin/add_admin', $data);
            $this->load->view('superadmin/footer');
        } else {
//            $nama = $this->input->post('nama');
//
//            $data['input'] = array(
//                'id_karyawan' => $this->input->post('id_karyawan'),
//                'jumlah_hadir' => $this->input->post('jumlah_hadir'),
//                'total_harus_masuk' => $this->input->post('total_harus_masuk'),
//                'potongan' => $this->input->post('potongan')
//            );
//
//            if (empty($nomer_sk) || $nama == "Panitia mungkin telah terpilih atau tidak ada data.") {
//                return false;
//            } else {
//                $this->superadmin_m->insert('admin', $data['input']);
//            }
        }
    }

    function simpan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['input'] = array(
            'name' => $this->input->post('nama_admin'),
            'password' => '$2a$08$/PMBB5iwXsN5MhFayYpHHeJSJBNZ5k3EuslBZKV7M8JJDmwBGQhZ6'
        );
        print_r($data['input']);
        if ($this->superadmin_->insert('user', $data['input'])) {
            echo 'berhasil';
            $data['input2'] = array(
                'role_uniq_kode' => '4',
                'user_name' => $this->input->post('nama_admin'),
                'app_config_uniq_kode' => '3'
            );
            $this->superadmin_->insert('app_user_role', $data['input2']);
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function edit($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
//        $data['nip_pegawai'] = $this->panitia_m->get_pegawai();
//        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        $datauser = $this->superadmin_m->view('pegawai', array('nip' => $datah['nip']));
        $data_admin = $this->superadmin_m->view('admin', array('id' => $id));
        //added
        $data['nama_admin'] = array(
            'name' => 'nama_admin',
            'id' => 'nama_admin',
            'placeholder' => 'Masukan username karyawan',
//            'data-error-empty' => 'Username harus diisi',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data_admin[0]->nama_admin
        );
        //

        $data['id'] = array(
            'name' => 'id',
            'id' => 'id',
            'class' => 'form-control',
            'type' => 'hidden',
            'value' => $data_admin[0]->id
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );

        if ($this->input->post('targetData') == false) {
//            $this->load->view('admin/header', $datah);
            $this->load->view('admin/admin/edit_admin', $data);
//            $this->load->view('admin/footer');
        } else {
            $nama = $this->input->post('nama');

            $data['input'] = array(
                'id_karyawan' => $this->input->post('username'),
                'jumlah_harus_hadir' => $this->input->post('jumlah_harus_hadir'),
                'jumlah_hadir' => $this->input->post('jumlah_hadir')
            );

            if (empty($nomer_sk) || $nama == "Panitia mungkin telah terpilih atau tidak ada data.") {
                return false;
            } else {
                $this->superadmin_m->insert('pinjaman', $data['input']);
                echo "sukses";
            }
        }
    }

    function simpan_edit() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['input'] = array(
            'nama_admin' => $this->input->post('nama_admin')
        );
//        print_r($data['input']);
        if ($this->superadmin_m->update('admin', $data['input'], $this->input->post('id'))) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        if ($this->superadmin_m->delete('admin', array('id' => $id))) {
            echo "sukses";
        } else {
            echo "gagal";
        }
        redirect($data['urlnya']);
    }

}
