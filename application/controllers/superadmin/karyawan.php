<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_superadmin.php');

class karyawan extends base_superadmin {

    public function __construct() {
        parent:: __construct();
        $this->load->model('admin/karyawan_m');
        session_start();
    }

    function index() {
        $data['page_title'] = 'Data Karyawan';
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        @$data["data_karyawan"] = $this->karyawan_m->get_order_by('view_karyawan', 'nama', 'ASC');

        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default            
            '<script src="' . base_url('assets/js/jquery-ui.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/autocomplete_panitia.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/jquery.dataTables.min.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/dataTables.bootstrap.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
            '<script src="' . base_url('assets/js/jquery.validate.js') . '"></script>' . "\n", //validate
//            '<script src="' . base_url('assets/js/jquery.datetimepicker.js') . '"></script>' . "\n", //datepicker
        );
        $data['scripts'] = $scripts;
        $data['popups_js'] = 'ada';
        $data['dataTable_js'] = 'ada';
        $this->load->view('superadmin/header', $datah);
        $this->load->view('superadmin/karyawan/daftar_karyawan', $data);
        $this->load->view('superadmin/footer');
    }

    function cetak_data_karyawan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        @$data["data_karyawan"] = $this->karyawan_m->get_order_by_nama_karyawan('view_karyawan');
        $this->load->view('superadmin/karyawan/cetak_data_karyawan', $data);
    }

    function pdf_cetak_data_karyawan() {
        @$data["data_karyawan"] = $this->karyawan_m->get_order_by_nama_karyawan('view_karyawan');
        $this->load->view('superadmin/karyawan/pdf_cetak_data_karyawan', $data);
        // Get output html
        $html = $this->output->get_output();
        // Load library
        $this->load->library('dompdf_gen');
        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper(array(0, 0, 609.448818898, 935.433070866), 'landscape');
        $this->dompdf->render();
        $this->dompdf->stream("cetak_data_karyawan.pdf");
    }

}

