<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_superadmin.php');

class data_admin extends base_superadmin {

    public function __construct() {
        parent:: __construct();
        $this->load->model('superadmin/data_admin_m');
        session_start();
    }

    function index() {
        $data['page_title'] = 'Data Admin';
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];

        $data["per_page"] = DEFAULT_PAGE;
        @$data["data_jabatan"] = $this->data_admin_m->view('app_user_role', array('role' => 'admin'));
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default            
            '<script src="' . base_url('assets/js/jquery-ui.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/autocomplete_panitia.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/jquery.dataTables.min.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/dataTables.bootstrap.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
            '<script src="' . base_url('assets/js/jquery.validate.js') . '"></script>' . "\n" //validate
        );
        $data['scripts'] = $scripts;        
        $data['popups_js'] = 'ada';
        $data['dataTable_js'] = 'ada';
            $this->load->view('superadmin/header', $datah);
            $this->load->view('superadmin/data_admin/daftar_admin', $data);
            $this->load->view('superadmin/footer');
    }

    function add_admin() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $datah['nip'] = $this->session->userdata('username'); //ambil data user
        
        $data['nama_admin'] = array(
            'name' => 'nama_admin',
            'id' => 'nama_admin',
            'placeholder' => 'Masukan nama admin',
            'class' => 'form-control',
            'type' => 'text',
        );

        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );
        $this->load->view('superadmin/data_admin/add_admin', $data);
    }

    function simpan() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
//        $password =  md5('default0123');
        $data['input'] = array(
            'user_name' => $this->input->post('nama_admin'),
            'password' => '$2a$08$/PMBB5iwXsN5MhFayYpHHeJSJBNZ5k3EuslBZKV7M8JJDmwBGQhZ6',
            'role' => 'admin'
        );
//        print_r($data['input']);
        if ($this->data_admin_m->insert('app_user_role', $data['input'])) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function edit($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data_admin = $this->data_admin_m->view('app_user_role', array('user_name' => $id));
        //added
        $data['nama_admin'] = array(
            'name' => 'nama_admin',
            'id' => 'nama_admin',
            'placeholder' => 'Masukan nama jabatan',
            'class' => 'form-control',
            'type' => 'text',
            'value' => $data_admin[0]->user_name
        );

        $data['id'] = array(
            'name' => 'id',
            'id' => 'id',
            'class' => 'form-control',
            'type' => 'hidden',
            'value' => $data_admin[0]->user_name
        );
        $data['val'] = 'aktif';
        $data['submitin'] = array(
            'name' => 'submitin',
            'id' => 'submitin',
            'class' => 'btn btn btn-primary pull-right col-sm-4',
            'type' => 'submit',
            'style' => '  height: 37px;margin-top: 12px;',
            'value' => 'Simpan'
        );

        $this->load->view('superadmin/data_admin/edit_admin', $data);
    }

    function simpan_edit() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['input'] = array(
            'user_name' => $this->input->post('nama_admin')
        );
        print_r($data['input']);
        print_r($this->input->post('id'));
        if ($this->data_admin_m->update('app_user_role', $data['input'], $this->input->post('id'))) {
            echo 'berhasil';
        } else {
            echo 'gagal';
        }
        redirect($data['urlnya']);
    }

    function hapus($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        if ($this->data_admin_m->delete('app_user_role', array('uniq_kode' => $id))) {
            echo "sukses";
        } else {
            echo "gagal";
        }
        redirect($data['urlnya']);
    }

}
