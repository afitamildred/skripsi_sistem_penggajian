<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'controllers/base_auth/base_superadmin.php');

class gaji extends base_superadmin {

    public function __construct() {
        parent:: __construct();
        $this->load->model('admin/gaji_m');
        session_start();
    }

    function index() {
        $data['page_title'] = 'Data Gaji';
        $datah['nip'] = $this->session->userdata('username'); //ambil data user

        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data["per_page"] = DEFAULT_PAGE;
        @$data["data_gaji"] = $this->gaji_m->get_order_by('view_gaji', 'nama_divisi', 'ASC');
        $scripts = array(
            '<script src="' . base_url('assets/js/jquery-1.11.2.min.js') . '"></script>' . "\n", //default            
            '<script src="' . base_url('assets/js/jquery-ui.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/bootstrap.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/metisMenu.min.js') . '"></script>' . "\n", //default
            '<script src="' . base_url('assets/js/autocomplete_panitia.js') . '"></script>' . "\n", //autocomplete
            '<script src="' . base_url('assets/js/jquery.dataTables.min.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/dataTables.bootstrap.js') . '"></script>' . "\n", //dataTables
            '<script src="' . base_url('assets/js/fancybox2/jquery.fancybox.js?v=2.1.5') . '"></script>' . "\n", //fancybox popups
            '<script src="' . base_url('assets/js/jquery.validate.js') . '"></script>' . "\n" //validate
        );
        $data['scripts'] = $scripts;
        $data['popups_js'] = 'ada';
        $data['dataTable_js'] = 'ada';
        $this->load->view('superadmin/header', $datah);
        $this->load->view('superadmin/gaji/daftar_gaji', $data);
        $this->load->view('superadmin/footer');
    }

    function cetak_gaji($isi) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
//        $data['ini'] = 'ini';

        $d = explode('_', $isi);
        $id_divisi = $d[0];
        @$periode = $d[1];
        if ($id_divisi == 0) {
            @$data['isi'] = @$isi;
            @$data["data_gaji"] = $this->gaji_m->get_order_by('view_gaji', 'nama_karyawan', 'ASC');
            @$data["periode"] = 'Semua Periode';
        } else {
            @$data['isi'] = @$isi;
            $d = explode('_', $isi);
            $id_divisi = $d[0];
            @$periode = $d[1];
            @$data['periode'] = $periode;
            @$data["data_gaji"] = $this->gaji_m->get_order_by_where('view_gaji', 'nama_divisi', 'ASC', array('id_divisi' => $id_divisi, 'periode' => $periode));
            @$data["periode"] = 'Periode ' . $periode;
        }
        $this->load->view('superadmin/gaji/cetak_gaji', $data);
    }

    function pdf_cetak_gaji($isi) {

        $d = explode('_', $isi);
        $id_divisi = $d[0];
        @$periode = $d[1];
        if ($id_divisi == 0) {
            @$data['isi'] = @$isi;
            @$data["data_gaji"] = $this->gaji_m->get_order_by('view_gaji', 'nama_karyawan', 'ASC');
            @$data["periode"] = 'Semua Periode';
        } else {
            @$data['isi'] = @$isi;
            $d = explode('_', $isi);
            $id_divisi = $d[0];
            @$periode = $d[1];
            @$data['periode'] = $periode;
            @$data["data_gaji"] = $this->gaji_m->get_order_by_where('view_gaji', 'nama_divisi', 'ASC', array('id_divisi' => $id_divisi, 'periode' => $periode));
            @$data["periode"] = 'Periode ' . $periode;
        }


        $this->load->view('superadmin/gaji/pdf_cetak_gaji', $data);
        // Get output html
        $html = $this->output->get_output();

        // Load library
        $this->load->library('dompdf_gen');

        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper(array(0, 0, 609.448818898, 935.433070866), 'landscape');
        $this->dompdf->render();
        $this->dompdf->stream("cetak_gaji.pdf");
    }

    function cetak_slip_gaji($id) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['functionnya'] = $this->uri->segment(3); //nama method
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['id'] = $id;
//        $data['ini'] = 'ini';
        @$data["data_gaji"] = $this->gaji_m->view('view_gaji', array('id' => $id));
        $data['periode'] = $data["data_gaji"][0]->periode;
        $this->load->view('superadmin/gaji/cetak_slip_gaji', $data);
    }

    function pdf_cetak_slip_gaji($id) {

        @$data["data_gaji"] = $this->gaji_m->view('view_gaji', array('id' => $id));
        $data['periode'] = $data["data_gaji"][0]->periode;
        $this->load->view('superadmin/gaji/pdf_cetak_slip_gaji', $data);
        // Get output html
        $html = $this->output->get_output();
        // Load library
        $this->load->library('dompdf_gen');
        // Convert to PDF
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper(array(0, 0, 609.448818898, 935.433070866), 'potrait');
        $this->dompdf->render();
        $this->dompdf->stream("slip_gaji_" . $id . ".pdf");
    }

    function lewat() {
        $data = array('status_data' => true);
        echo json_encode($data);
    }

    function daftar_gaji() {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $data['isi'] = '0_pilih';
        @$data["data_gaji"] = $this->gaji_m->get_order_by('view_gaji', 'nama_karyawan', 'ASC');

        $this->load->view('superadmin/gaji/data_gaji', $data);
    }

    function daftar_gaji_klik($isi) {
        $data['role'] = $this->uri->segment(1); //nama folder
        $data['classnya'] = $this->uri->segment(2); //nama class
        $data['urlnya'] = $data['role'] . '/' . $data['classnya'];
        $d = explode('_', $isi);
        $id_divisi = $d[0];
        $periode = $d[1];

        @$data['isi'] = @$isi;
        @$data['periode'] = $periode;
        @$data["data_gaji"] = $this->gaji_m->get_order_by_where('view_gaji', 'nama_divisi', 'ASC', array('id_divisi' => $id_divisi, 'periode' => $periode));

        $this->load->view('superadmin/gaji/data_gaji', $data);
    }

}
