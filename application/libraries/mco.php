<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_mco extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function logged(){
		if(!isset($this->session->userdata['logged_in'])){
			redirect('login');
		}
	}
}