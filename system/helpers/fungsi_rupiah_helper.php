<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('format_rupiah')) {

    function format_rupiah($angka) {
        $rupiah = number_format(empty($angka) ? 0 : $angka, 0, ',', '.');
        return $rupiah;
    }

}

