//autocomplete cari instansi http://localhost/edelivery/ppk/pemb_secara_elektronik/pemb_elektronik
$(document).ready(function () {
    $('#autocomplete').on('keyup', function() {
        afterShow: function () {
            var urlbaru = $('#url').data('npwp');
            $("#autocomplete").autocomplete({
                minLength: 1,
                source:
                        function (req, add) {
                            $.ajax({
                                url: urlbaru,
                                dataType: 'json',
                                type: 'POST',
                                data: req,
                                success: function (data) {
                                    if (data.response === "true") {
                                        add(data.message);
                                    } else {
                                        add(data.value);
                                    }
                                }
                            });
                        }, select: function (event, ui) {
                    // Pencarian NPWP Perusahaan
                    $("#autocomplete").val(ui.item.value);
                    $("#alamat").val(ui.item.alamat);
                    $("#email").val(ui.item.email);
                    $("#direktur").val(ui.item.direktur);
                    $("#id_perusahaan").val(ui.item.id_perusahaan);
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li>")
                        .append("<a><strong>" + item.value + "</strong><br />" + "\
                                    <span style='margin-left:20px'>" + item.alamat + "</span>" + "\
                                    <span style='margin-left:20px'>" + item.email + "</span>" + "\
                                    <span style='margin-left:20px'>" + item.direktur + "</span>" + "\
                                    </a>")
                        .appendTo(ul);
            };
        }       
    });
});
//