//begin: harjuna comsky added
//
//autocomplete
$(document).ready(function () {
    $(".conbtn_autocomplete").fancybox({
        afterShow: function () {
            var urlbaru = $('#url').data('alamat');
            $("#autocomplete").autocomplete({
                minLength: 1,
                source:
                        function (req, add) {
                            $.ajax({
                                url: urlbaru,
                                dataType: 'json',
                                type: 'POST',
                                data: req,
                                success: function (data) {
                                    if (data.response === "true") {
                                        add(data.message);
                                    } else {
                                        add(data.value);
                                    }
                                }
                            });
                        }, select: function (event, ui) {
                    // Pencarian NIP Pegawai
                    $("#nama").val(ui.item.nama);
                    $("#autocomplete").val(ui.item.value);
                    if(!$(this).val()){
                        $('#autocomplete').removeAttr('readonly'); 
                        
                    }else{
                        $('#autocomplete').prop('readonly', true);                       
                    }
                    $("#pangkat").val(ui.item.pangkat);
                    $("#jabatan").val(ui.item.jabatan);
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li>")
                        .append("<a><strong>" + item.nama + "</strong><br>" + "<span style='margin-left:20px'>" + item.value + "</span>" + "<br>" + "<span style='margin-left: 40px'>" + item.jabatan + "</span>" + "</a>")
                        .appendTo(ul);
            };
        }
    });
});
//
//end: harjuna comsky added