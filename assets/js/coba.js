    $(document).ready(function() {
        var jml_termin = $('#jml_termin').val();

        var maksimal = 100;
        var sum_progress_fisik = 0;

        $('.progress_fisik').each(function() {
            sum_progress_fisik += parseInt(this.value);
        });
        var parse_mak = parseInt(sum_progress_fisik);
        $('#total_progress_fisik').val(parse_mak);

        sum_persen = 0;
        for (i = 1; i <= jml_termin; i++) {
            var nil1 = $('#persen_pembayaran_cb_awal_' + i).val();
            if (!isNaN(nil1) && nil1.length != 0) {
                sum_persen += parseInt(nil1);
            }
        }
        var parse_mak2 = parseInt(sum_persen);
        $('#total_persen_pembayaran').val(parse_mak2);

        $('.progress_fisik').keyup(function() {
            var persen_ps = $(this).val();
            var idnya = $(this).attr('id');
            var pecah = idnya.split('_');
            var idne = pecah[2];
            var maksimal = 100;
            var sum_progress_fisik = 0;

            $('.progress_fisik').each(function() {
                sum_progress_fisik += parseInt(this.value);
            });

            $('#total_progress_fisik').val(sum_progress_fisik);

            var sisa_persennya = parseInt(maksimal) - sum_progress_fisik;
            if (sisa_persennya > 0 && sum_progress_fisik <= maksimal) {
                $('#data_um_' + idne).html("Masih ada sisa " + sisa_persennya + " %").fadeIn(200).delay(2000).fadeOut(200);
                this.value = persen_ps;
            } else if (sisa_persennya == 0 && sum_progress_fisik == maksimal) {
                bootbox.alert("Alokasi progress fisik sudah habis !", function() {
                    console.log("Alert Callback");
                });
            } else if (sisa_persennya < 0 && sum_progress_fisik > maksimal) {
                bootbox.alert("Nilai maksimal progress fisik semua termin 100%!", function() {
                    console.log("Alert Callback");
                });
                var sisa_nilai = parseInt(maksimal) - sum_progress_fisik + parseInt(persen_ps);
                this.value = sisa_nilai;
            }

        });
        var sum_all = 0;
        $('.total_terminke').each(function() {
            sum_all += parseInt(this.value);
        });

        $("#total_termin").val(("Rp " + convertToRupiah(sum_all) + ',00'));
        $('#total_termin_unNum').val(sum_all);

        $('.targetDiv').hide();
        $('.show').click(function() {
            $('.targetDiv').hide();
            if ($(this).is('[disabled]')) {
                $('#div' + $(this).attr('target')).hide();
            } else {
                $(this).css("cursor", "pointer");
                $('#div' + $(this).attr('target')).show();
            }
        });
        $('.hide').click(function() {
            $('.targetDiv').hide();

        });
    });
    $('#myCheck').on('change', function() {
        if (this.checked === true) {
            $(".persen_pembayaran").prop('disabled', true);
            $(".persen_um_pembayaran").prop('disabled', true);

            var jml_temin = $('#jml_termin').val();
            for (i = 1; i <= jml_temin; i++) {
                $('#btntermin' + i).removeAttr('disabled');
            }
            $('#total_persen_pembayaran').val(100);
            $('.persen_pembayaran').each(function() {
                var jml_termin = $('#jml_termin').val();
                var idpersen = $(this).attr('id');
                if (jml_termin == 1) {
                    $('#persen_pembayaran_cb' + 1).val(parseInt(100));
                    $('#persen_pembayaran_cb_awal_' + 1).val(parseInt(100));
                    $('#' + 1).val(parseInt(100));
                } else if (jml_termin == 2) {
                    $('#persen_pembayaran_cb' + 1).val(parseInt(50));
                    $('#persen_pembayaran_cb' + 2).val(parseInt(50));
                    $('#persen_pembayaran_cb_awal_' + 1).val(parseInt(50));
                    $('#persen_pembayaran_cb_awal_' + 2).val(parseInt(50));
                } else if (jml_termin == 3) {
                    $('#persen_pembayaran_cb' + 1).val(parseInt(35));
                    $('#persen_pembayaran_cb' + 2).val(parseInt(35));
                    $('#persen_pembayaran_cb' + 3).val(parseInt(30));
                    $('#persen_pembayaran_cb_awal_' + 1).val(parseInt(35));
                    $('#persen_pembayaran_cb_awal_' + 2).val(parseInt(35));
                    $('#persen_pembayaran_cb_awal_' + 3).val(parseInt(30));
                }
            });
            $('.alokasi2_unNum').each(function() {
                var valueField = $(this).attr('id');
                var explod = valueField.split('_');
                var id_persen_pemb = explod[1];
                var persen_um = $('#persen_um').val();
                var nilai_akun = parseInt($('#' + valueField).val());
                var persen_um_kali_akun = (persen_um / 100) * nilai_akun;

                var persen = parseInt($('#persen_pembayaran_cb' + id_persen_pemb).val());
                var hasile = (nilai_akun * persen) / 100;
//                var hasil_kurang = nilai_akun - hasile;
                var hasil_kurang2 = nilai_akun - persen_um_kali_akun;
                var nilai_persene = $('#persen_pembayaran_cb_awal_' + id_persen_pemb).val();
                var hasilpersenpembayaran = (hasil_kurang2 * nilai_persene) / 100;
                if (persen == 0) {
                    $('#kurang' + valueField).val("Rp " + convertToRupiah(hasil_kurang2) + ',00');
                    $('#kurang_unNum' + valueField).val(hasil_kurang2);
                } else {
                    $('#kurang' + valueField).val("Rp " + convertToRupiah(hasil_kurang2) + ',00');
                    $('#kurang_unNum' + valueField).val(hasil_kurang2);

                    $('#akun_kotor' + valueField).val(hasile);
                    $('#nominal_akun_kurang_um' + valueField).val("Rp " + convertToRupiah(Math.round(hasilpersenpembayaran)) + ',00');
                    $('#nominal_akun_kurang_um_unNum' + valueField).val(Math.round(hasilpersenpembayaran));

                    var sum = 0;
                    $('.nominal_akun_kurang_um_' + id_persen_pemb).each(function() {
                        sum += parseInt(this.value);
                    });

                    var pembulatan1 = sum / 1000;
                    var pembulatan2 = Math.floor(pembulatan1);
                    var pembulatan3 = pembulatan2 * 1000;
                    $('#total_terminke_' + id_persen_pemb).val(("Rp " + convertToRupiah(pembulatan3) + ',00'));
                    $('#totalterminunNum_' + id_persen_pemb).val(pembulatan3);

                    var sum_all = 0;
                    $('.total_terminke').each(function() {
                        sum_all += parseInt(this.value);
                    });

                    $("#total_termin").val(("Rp " + convertToRupiah(sum_all) + ',00'));
                    $('#total_termin_unNum').val(sum_all);
                }
            });
            $('#persen_um').keyup(function() {
                var persen = $(this).val();
                var jml_termin = $('#jml_termin').val();
                var idpersen = $(this).attr('id');
                var bagi_rata = 100 / jml_termin;
                $('.persen_pembayaran').attr('value', Math.round(bagi_rata)); // pembulatan ni fit.. :D

                $('.persen_pembayaran').each(function() {
                    var jml_termin = $('#jml_termin').val();
                    var idpersen = $(this).attr('id');
                    if (jml_termin == 1) {
                        $('#persen_pembayaran_cb' + 1).val(parseInt(100));
                        $('#persen_pembayaran_cb_awal_' + 1).val(parseInt(100));

                        $('#' + 1).val(parseInt(100));
                    } else if (jml_termin == 2) {
                        $('#persen_pembayaran_cb' + 1).val(parseInt(50));
                        $('#persen_pembayaran_cb' + 2).val(parseInt(50));
                        $('#persen_pembayaran_cb_awal_' + 1).val(parseInt(50));
                        $('#persen_pembayaran_cb_awal_' + 2).val(parseInt(50));
                    } else if (jml_termin == 3) {
                        $('#persen_pembayaran_cb' + 1).val(parseInt(35));
                        $('#persen_pembayaran_cb' + 2).val(parseInt(35));
                        $('#persen_pembayaran_cb' + 3).val(parseInt(30));
                        $('#persen_pembayaran_cb_awal_' + 1).val(parseInt(35));
                        $('#persen_pembayaran_cb_awal_' + 2).val(parseInt(35));
                        $('#persen_pembayaran_cb_awal_' + 3).val(parseInt(30));
                    }
                });

                $('.alokasi2_unNum').each(function() {
                    var valueField = $(this).attr('id');
                    var explod = valueField.split('_');
                    var id_persen_pemb = explod[1];
                    var nilai_akun = parseInt($('#' + valueField).val());
                    var hasile = (nilai_akun * persen) / 100;
                    var hasil_kurang = nilai_akun - hasile;
                    var nilai_persene = $('#persen_pembayaran_cb_awal_' + id_persen_pemb).val();
                    var hasilpersenpembayaran = (hasil_kurang * nilai_persene) / 100;
                    $('#akun_kotor' + valueField).val(hasile);
                    $('#kurang' + valueField).val("Rp " + convertToRupiah(hasil_kurang) + ',00');
                    $('#kurang_unNum' + valueField).val(hasil_kurang);

                    $('#nominal_akun_kurang_um' + valueField).val("Rp " + convertToRupiah(Math.round(hasilpersenpembayaran)) + ',00');
                    $('#nominal_akun_kurang_um_unNum' + valueField).val(Math.round(hasilpersenpembayaran));

                    var sum = 0;
                    $('.nominal_akun_kurang_um_' + id_persen_pemb).each(function() {
                        sum += parseInt(this.value);
                    });

                    var pembulatan1 = sum / 1000;
                    var pembulatan2 = Math.floor(pembulatan1);
                    var pembulatan3 = pembulatan2 * 1000;
                    $('#total_terminke_' + id_persen_pemb).val(("Rp " + convertToRupiah(pembulatan3) + ',00'));
                    $('#totalterminunNum_' + id_persen_pemb).val(pembulatan3);

                    var sum_all = 0;
                    $('.total_terminke').each(function() {
                        sum_all += parseInt(this.value);
                    });

                    $("#total_termin").val(("Rp " + convertToRupiah(sum_all) + ',00'));
                    $('#total_termin_unNum').val(sum_all);
                });
            });
            $('.progress_fisik').keyup(function() {
                var persen_ps = $(this).val();
                var maksimal_ps = 100;
                if (persen_ps > maksimal_ps) {
                    bootbox.alert("Nilai maksimal 100!", function() {
                        console.log("Alert Callback");
                    });
                    this.value = maksimal_ps;
                    persen_ps = maksimal_ps;
                } else {
                    this.value = persen_ps;
                }
            });
        }
        else {
            $(".persen_pembayaran").prop('disabled', false);
            $(".persen_um_pembayaran").prop('disabled', false);
            //belum benar
            var jml_temin = $('#jml_termin').val();
            for (i = 1; i <= jml_temin; i++) {
                if (i > 0) {
                    var idbutton_termin = 'btntermin' + i;
                    document.getElementById(idbutton_termin).disabled = true;
                }
            }
        }
    });
    jQuery(function($) {
        var anc = {};
        $('.datetimepicker').datetimepicker({
            lang: 'id',
            timepicker: false,
            format: 'Y-m-d'
        });
    });
    $(document).ready(function() {
        $('#persen_um').keyup(function() {
            var persen_um = $(this).val();
            var sumalokasi = 0;
            $('.alokasi_unNum').each(function() {
                var valueField = $(this).attr('id');
                var rawVal = parseInt($('#' + valueField).val());
                var nominale = (rawVal * persen_um) / 100;

                $('#nominal_persen2' + valueField).val("Rp " + convertToRupiah(nominale) + ',00');
                $('#nominal_persen2_unNum' + valueField).val(nominale);

                var hasil_kurang_akun = $('#nominal_persen2_unNum' + valueField).val();
                var pengurangan = rawVal - hasil_kurang_akun;

                $('#alokasi_akun_termin_' + valueField).val("Rp " + convertToRupiah(pengurangan) + ',00');
                $('#alokasi_akun_termin_unNum_' + valueField).val(pengurangan);

                $('#alokasi2_' + valueField).val("Rp " + convertToRupiah(pengurangan) + ',00');
                $('#_' + valueField).val(pengurangan);

                if (!isNaN($(this).val()) && $(this).val().length != 0) {
                    sumalokasi += parseInt($(this).val());
                }
            });
            //penambahan untuk keyup persen um dan persen pembayaran
            $('.alokasi2_unNum').each(function() {
                var valueField = $(this).attr('id');
                var explod = valueField.split('_');
                var id_persen_pemb = explod[1];
                var nilai_akun = parseInt($('#' + valueField).val());
                var persen_um_kali_akun = (persen_um / 100) * nilai_akun;

                var persen = parseInt($('#persen_pembayaran_cb' + id_persen_pemb).val());
                var hasile = (nilai_akun * persen) / 100;
                var hasil_kurang = nilai_akun - hasile;
                var hasil_kurang2 = nilai_akun - persen_um_kali_akun;
//                alert(hasil_kurang2);
                var nilai_persene = $('#persen_pembayaran_cb_awal_' + id_persen_pemb).val();
                var hasilpersenpembayaran = (hasil_kurang2 * nilai_persene) / 100;
                if (persen == 0) {
                    $('#kurang' + valueField).val("Rp " + convertToRupiah(hasil_kurang2) + ',00');
                    $('#kurang_unNum' + valueField).val(hasil_kurang2);
                } else {
                    $('#kurang' + valueField).val("Rp " + convertToRupiah(hasil_kurang2) + ',00');
                    $('#kurang_unNum' + valueField).val(hasil_kurang2);

                    $('#akun_kotor' + valueField).val(hasile);
                    $('#nominal_akun_kurang_um' + valueField).val("Rp " + convertToRupiah(Math.round(hasilpersenpembayaran)) + ',00');
                    $('#nominal_akun_kurang_um_unNum' + valueField).val(Math.round(hasilpersenpembayaran));

                    var sum = 0;
                    $('.nominal_akun_kurang_um_' + id_persen_pemb).each(function() {
                        sum += parseInt(this.value);
                    });

                    var pembulatan1 = sum / 1000;
                    var pembulatan2 = Math.floor(pembulatan1);
                    var pembulatan3 = pembulatan2 * 1000;
                    $('#total_terminke_' + id_persen_pemb).val(("Rp " + convertToRupiah(pembulatan3) + ',00'));
                    $('#totalterminunNum_' + id_persen_pemb).val(pembulatan3);

                    var sum_all = 0;
                    $('.total_terminke').each(function() {
                        sum_all += parseInt(this.value);
                    });

                    $("#total_termin").val(("Rp " + convertToRupiah(sum_all) + ',00'));
                    $('#total_termin_unNum').val(sum_all);
                }
            });

            $("#total_alokasi").val(("Rp " + convertToRupiah(sumalokasi) + ',00'));
            $('#total_alokasi_unNum').val(sumalokasi);

            var sum = 0;
            $(".nominal_unNum").each(function() {
                if (!isNaN($(this).val()) && $(this).val().length != 0) {
                    sum += parseInt($(this).val());
                }
            });
            var pembulatan1 = sum / 1000;
            var pembulatan2 = Math.floor(pembulatan1);
            var pembulatan3 = pembulatan2 * 1000;
            $("#nominal_persen").val(("Rp " + convertToRupiah(pembulatan3) + ',00'));
            $('#nominal_persen_unNum').val(pembulatan3);
        });

        $('.persen_pembayaran').keyup(function() {
            var id_persen1 = $(this).attr('id');
            var e = id_persen1.split('_');
            var id_persen = e[4];
            var nilai_persen_pembayaran = $(this).val();

            var persen_umnya = $('#persen_um').val();
            var jml_termin = $('#jml_termin').val();
            var maksimal_persen = 100;

            sum_persen = 0;
            for (i = 1; i <= jml_termin; i++) {
                var nil1 = $('#persen_pembayaran_cb_awal_' + i).val();
                if (!isNaN(nil1) && nil1.length != 0) {
                    sum_persen += parseInt(nil1);
                }
            }

            $('#total_persen_pembayaran').val(sum_persen);
            if (sum_persen > maksimal_persen) {
                bootbox.alert("Jumlah semua termin tidak boleh melebihi 100%, mohon cek kembali isian anda?", function() {
                    console.log("Alert Callback");
                });
                var sisa_nilai = parseInt(maksimal_persen) - sum_persen;
                var keyup_persennya = $(this).val();
                this.value = sisa_nilai + parseInt(keyup_persennya);
                nilai_persen_pembayaran = sisa_nilai + parseInt(keyup_persennya);
                $('#persen_pembayaran_cb' + id_persen).val(nilai_persen_pembayaran);
            } else {
                var sisa_persennya = parseInt(maksimal_persen) - sum_persen;
                if (sisa_persennya > 0) {
                    //alert('Masih ada sisa ' + sisa_persennya + ' %');
//                    bootbox.alert("Masih ada sisa " + sisa_persennya + " %", function() {
//                        console.log("Alert Callback");
//                    });
                    $('#data_pp_' + id_persen).html("Masih ada sisa " + sisa_persennya + " %").fadeIn(200).delay(2000).fadeOut(200);
                    this.value = nilai_persen_pembayaran;
                } else {
                    bootbox.alert("Alokasi persen pembayaran sudah habis!", function() {
                        console.log("Alert Callback");
                    });
                }
            }

            $('#persen_pembayaran_cb' + id_persen).val(nilai_persen_pembayaran);

            var idter = parseInt(id_persen);
            var termin_next = idter + 1;
            $('#btntermin' + termin_next).removeAttr('disabled');
            var persen_um = $('#persen_um').val();

            $('.alokasi2_unNum').each(function() {
                var valueField = $(this).attr('id');
                var nilai_akun = parseInt($('#' + valueField).val());
                var hasile = (nilai_akun * persen_um) / 100;

                var hasil_kurang = nilai_akun - hasile;
                var hasilpersenpembayaran = (hasil_kurang * nilai_persen_pembayaran) / 100;
                $('#kurang' + valueField).val("Rp " + convertToRupiah(hasil_kurang) + ',00');
                $('#kurang_unNum' + valueField).val(hasil_kurang);
                $('#akun_kotor' + valueField).val(hasile);
            });

            $('.kurang_unNum' + id_persen).each(function() {
                var id_pe_um = $(this).attr('id');
                var e = id_pe_um.split('_');
                var idnya = e[2] + '_' + e[3];
                var nilai_akun = parseInt($('#kurang_unNum_' + idnya).val());
                var persen = parseInt($('#persen_pembayaran_cb' + id_persen).val());
                var nominale = (nilai_akun * persen) / 100;

                if (persen_umnya == 0) {
                    $('#nominal_akun_kurang_um_' + idnya).val("Rp " + convertToRupiah(Math.round(nominale)) + ',00');
                    $('#nominal_akun_kurang_um_unNum_' + idnya).val(Math.round(nominale));
                } else {
                    $('#kurang' + idnya).val("Rp " + convertToRupiah(nilai_akun) + ',00');
                    $('#kurang_unNum' + idnya).val(nilai_akun);
                    $('#nominal_akun_kurang_um_' + idnya).val("Rp " + convertToRupiah(Math.round(nominale)) + ',00');
                    $('#nominal_akun_kurang_um_unNum_' + idnya).val(Math.round(nominale));
                }
            });

            var sum = 0;
            $('.nominal_akun_kurang_um_' + id_persen).each(function() {
                sum += parseInt(this.value);
            });

            var pembulatan1 = sum / 1000;
            var pembulatan2 = Math.floor(pembulatan1);
            var pembulatan3 = pembulatan2 * 1000;

            $('#total_terminke_' + id_persen).val(("Rp " + convertToRupiah(pembulatan3) + ',00'));
            $('#totalterminunNum_' + id_persen).val(pembulatan3);

            var sum_all = 0;
            $('.total_terminke').each(function() {
                sum_all += parseInt(this.value);
            });
            $("#total_termin").val(("Rp " + convertToRupiah(sum_all) + ',00'));
            $('#total_termin_unNum').val(sum_all);
        });
    });