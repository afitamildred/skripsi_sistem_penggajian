$(document).ready(function () {
        $(".conbtn").fancybox({
            "showLoading":false,
            afterShow: function () {
                var urlbaru = $('#url').data('alamat');
                $("#autocomplete").autocomplete({
                    minLength: 1,
                    source:
                            function (req, add) {
                                $.ajax({
                                    url: urlbaru,
                                    dataType: 'json',
                                    type: 'POST',
                                    data: req,
                                    success: function (data) {
                                        if (data.response === "true") {
                                            add(data.message);
                                        } else {
                                            add(data.value);
                                        }
                                    }
                                });
                            }, select: function (event, ui) {
                        // Pencarian NIP Pegawai
                        $("#nama").val(ui.item.label);
                        $("#nip").val(ui.item.value);
                        // Pencarian Level 3 HSB
                        $("#parent").val(ui.item.kode);
                        $("#level1").val(ui.item.label);
                        $("#level2").val(ui.item.desc);
                        $("#carinip").val(ui.item.value);
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    return $("<li>")
                            .append("<a><strong>" + item.label + "</strong><br>" + "<span style='margin-left:20px'>" + item.desc + "</span>" + "<br>" + "<span style='margin-left: 40px'>" + item.title + "</span>" + "</a>")
                            .appendTo(ul);
                };
            }
        });
    });